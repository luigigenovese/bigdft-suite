! hasn't been maintained, and in any case too expensive to be useful - would need to be re-written
!! we want to compare every tmb to every other tmb, calculating rotations using environment info
!subroutine tmb_overlap_onsite_rotate(iproc, nproc, input, at, tmb, rxyz, ref_frags)
!  use module_base
!  use module_types
!  use module_fragments
!  use io, only: find_neighbours
!  use locregs, only: allocate_wfd, deallocate_wfd
!  use rototranslations
!  use reformatting
!  implicit none
!
!  ! Calling arguments
!  integer,intent(in) :: iproc, nproc
!  type(input_variables),intent(in) :: input
!  type(atoms_data), intent(in) :: at
!  type(DFT_wavefunction),intent(inout):: tmb
!  real(kind=gp),dimension(3,at%astruct%nat),intent(in) :: rxyz
!  type(system_fragment), dimension(input%frag%nfrag_ref), intent(in) :: ref_frags
!
!  !local arguments
!  integer :: num_neighbours, iat, jat, ntmb_frag_and_env, iiorb, iforb, iiat, iatf, ityp
!  integer :: iiat_tmp, ilr_tmp, norb_tmp, jproc, iroot, ncount, j, ndim, ndim_tmp, iorb, ilr
!  integer :: jstart, jstart_tmp, ndim_tmp1, istart_tmp, jorb, jlr, jjorb, jjat, iorba, jorba
!  integer :: ifr, ifr_ref, ntmb_frag_and_env_dfrag, num_env, i, k
!  integer, dimension(3) :: ns, ns_tmp, n, n_tmp, nglr
!  integer, dimension(0:7) :: reformat_reason
!  integer, dimension(:), allocatable :: workarray, ifrag_ref
!  integer, dimension(:,:), allocatable :: map_frag_and_env, frag_map
!
!  real(kind=gp) :: tol, ddot
!  real(kind=gp), dimension(3) :: centre_old_box, centre_new_box, da
!  real(kind=wp), dimension(:), pointer :: psi_tmp
!  real(kind=wp), dimension(:), allocatable :: psi_tmp_i, psi_tmp_j
!  real(kind=gp), dimension(:,:), allocatable :: overlap
!  real(kind=wp), dimension(:,:,:,:,:,:), allocatable :: phigold
!
!  type(system_fragment), dimension(:), allocatable :: ref_frags_atomic, ref_frags_atomic_dfrag
!  type(rototranslation), dimension(:,:), pointer :: frag_trans
!  type(f_enumerator) :: strategy
!  type(dictionary), pointer :: dict_info
!  logical :: reformat, wrap_around
!  !!real(wp) :: dnrm2
!
!  ! check which fragment we're on - if this isn't a fragment calculation then all atoms are in the same fragment, which could be very expensive!
!
!  call reformatting_init_info(dict_info)
!  
!  ifrag_ref=f_malloc(at%astruct%nat,id='ifrag_ref')
!  if (input%frag%nfrag>1) then
!     iat=0
!     do ifr=1,input%frag%nfrag
!        ifr_ref=input%frag%frag_index(ifr)
!        do iatf=1,ref_frags(ifr_ref)%astruct_frg%nat
!           iat=iat+1
!           ifrag_ref(iat)=ifr_ref
!           !print*,'frag',iat,ifr_ref,input%frag%nfrag
!        end do
!     end do
!  else
!     ifrag_ref=1
!  end if
!
!  ! allocate and set up ref_frags_atomic, treating each atom as if it is a fragment
!  allocate(ref_frags_atomic(at%astruct%nat))
!  allocate(ref_frags_atomic_dfrag(at%astruct%nat))
!
!  do iat=1,at%astruct%nat
!     call setup_frags_from_astruct(ref_frags_atomic(iat))
!     call setup_frags_from_astruct(ref_frags_atomic_dfrag(iat))
!  end do
!
!
!  ! take a default value of 4 if this hasn't been specified in the input
!  if (input%lin%frag_num_neighbours==0) then
!     num_neighbours=4
!  else
!     num_neighbours=input%lin%frag_num_neighbours
!  end if
!
!
!  allocate(frag_trans(at%astruct%nat,at%astruct%nat))
!
!  ! pre-fill with identity transformation
!  do iat=1,at%astruct%nat
!     do jat=1,at%astruct%nat
!        frag_trans(iat,jat)=rototranslation_identity()
!        call set_translation(frag_trans(iat,jat),src=[0.0_gp,0.0_gp,0.0_gp],&
!             dest=frag_center(1,rxyz(:,jat)))
!!!$        frag_trans(iat,jat)%rot_center_new=frag_center(1,rxyz(:,jat))
!        frag_trans(iat,jat)%Werror=-1.0d0
!     end do
!     ! diagonal terms have zero error
!     frag_trans(iat,jat)%Werror=0.0d0
!  end do
!
!  ! get all fragment transformations first, then reformat
!  do iat=1,at%astruct%nat
!     frag_map=f_malloc0((/ref_frags_atomic(iat)%fbasis%forbs%norb,3/),id='frag_map')
!
!     ! tmbs on the same atom should be consecutive, so we just need to find the first tmb for this atom
!     do iiorb=1,tmb%orbs%norb
!        iiat=tmb%orbs%onwhichatom(iiorb)
!        if (iiat==iat) exit
!     end do
!
!     do iforb=1,ref_frags_atomic(iat)%fbasis%forbs%norb
!        !tmb frag -> tmb full
!        frag_map(iforb,1)=iiorb+iforb-1
!        !tmb frag -> atom frag
!        frag_map(iforb,2)=1
!     end do
!     !atom frag -> atom full
!     frag_map(1,3)=iat
!
!     ! find environment atoms
!     ! don't think we care about keeping track of which atoms they were so we can immediately destroy the mapping array
!     map_frag_and_env = f_malloc((/tmb%orbs%norb,3/),id='map_frag_and_env')
!     
!     ! NB not checking for ghost atoms here - don't think it's necessary
! 
!     ! version with most extensive matching
!     call find_neighbours(num_neighbours,at,rxyz,tmb%orbs,ref_frags_atomic(iat),frag_map,&
!          ntmb_frag_and_env,map_frag_and_env,.false.,input%lin%frag_neighbour_cutoff,.false.)
!     ! with only closest shell
!     call find_neighbours(num_neighbours,at,rxyz,tmb%orbs,ref_frags_atomic_dfrag(iat),frag_map,&
!          ntmb_frag_and_env_dfrag,map_frag_and_env,.true.,input%lin%frag_neighbour_cutoff,.false.)
!     call f_free(map_frag_and_env)
!
!     ! we also need nbasis_env
!     ref_frags_atomic(iat)%nbasis_env=0
!     do iatf=1,ref_frags_atomic(iat)%astruct_env%nat
!        ityp=ref_frags_atomic(iat)%astruct_env%iatype(iatf)
!        ref_frags_atomic(iat)%nbasis_env=ref_frags_atomic(iat)%nbasis_env+input%lin%norbsPerType(ityp)
!     end do
!
!     ! we also need nbasis_env
!     ref_frags_atomic_dfrag(iat)%nbasis_env=0
!     do iatf=1,ref_frags_atomic_dfrag(iat)%astruct_env%nat
!        ityp=ref_frags_atomic_dfrag(iat)%astruct_env%iatype(iatf)
!        ref_frags_atomic_dfrag(iat)%nbasis_env=ref_frags_atomic_dfrag(iat)%nbasis_env+input%lin%norbsPerType(ityp)
!     end do
!
!     do jat=1,at%astruct%nat
!        ! now we treat ref_frags_atomic and environment data therein as if it came from a file
!        ! and find transformation between that and current atom
!        ! in case where Wahba gives a huge error just do no transformation?****************** (actually maybe want to ignore Si atom i.e. those further away?)
!
!        ! for different atom types don't bother checking for transformation (avoids problems if ntmb/atom doesn't match)
!        if (at%astruct%iatype(iat)==at%astruct%iatype(jat)) then
!           ! as above, we don't need to keep track of mapping, we just want the transformation
!           map_frag_and_env = f_malloc((/ref_frags_atomic(iat)%nbasis_env,3/),id='map_frag_and_env')
!
!           ! if we are an identical fragment then do full matching
!           if (ifrag_ref(iat)==ifrag_ref(jat)) then
!              call match_environment_atoms(jat-1,at,rxyz,tmb%orbs,ref_frags_atomic(iat),&
!                   ref_frags_atomic(iat)%nbasis_env,map_frag_and_env,frag_trans(jat,iat),.false.)
!           ! otherwise just look at nearest neighbours, i.e. n closest, not n closest of each type
!           else
!              call match_environment_atoms(jat-1,at,rxyz,tmb%orbs,ref_frags_atomic_dfrag(iat),&
!                   ref_frags_atomic_dfrag(iat)%nbasis_env,map_frag_and_env,frag_trans(jat,iat),.true.)
!           end if
!
!           call f_free(map_frag_and_env)
!
!           !if (frag_trans(jat,iat)%Werror > W_tol) call f_increment(itoo_big)
!        end if
!
!     end do
!
!     call f_free(frag_map)
!  end do
!
!  !debug - check calculated transformations
!  if (iproc==0) then
!     open(99)
!     do iat=1,at%astruct%nat
!        do jat=1,at%astruct%nat
!           if (at%astruct%iatype(iat)/=at%astruct%iatype(jat)) then
!              num_env=-1
!           else if (ifrag_ref(iat)==ifrag_ref(jat)) then
!              num_env=ref_frags_atomic(iat)%astruct_env%nat-ref_frags_atomic(iat)%astruct_frg%nat
!           else
!              num_env=ref_frags_atomic_dfrag(iat)%astruct_env%nat-ref_frags_atomic_dfrag(iat)%astruct_frg%nat
!           end if
!           write(99,'(2(a,1x,I5,1x),F12.6,2x,3(F12.6,1x),6(1x,F18.6),2x,F6.2,3(2x,I5))') &
!                trim(at%astruct%atomnames(at%astruct%iatype(iat))),iat,&
!                trim(at%astruct%atomnames(at%astruct%iatype(jat))),jat,&
!                frag_trans(iat,jat)%theta/(4.0_gp*atan(1.d0)/180.0_gp),frag_trans(iat,jat)%rot_axis,&
!                frag_trans(iat,jat)%rot_center_src,frag_trans(iat,jat)%rot_center_dest,&
!                frag_trans(iat,jat)%Werror,num_env,ifrag_ref(iat),ifrag_ref(jat)
!        end do
!     end do
!     close(99)
!  end if
!
!
!  ! NOW WE ACTUALLY NEED TO REFORMAT
!
!  ! move all psi into psi_tmp all centred in the same place and calculate overlap matrix
!  tol=1.d-3
!  reformat_reason=0
!
!  !arbitrarily pick the middle one as assuming it'll be near the centre of structure
!  !and therefore have large fine grid
!  !might be more efficient to reformat each jorb to iorb's lzd, but that involves communicating lots of keys...
!  norb_tmp=tmb%orbs%norb/2
!  ilr_tmp=tmb%orbs%inwhichlocreg(norb_tmp)
!  iiat_tmp=tmb%orbs%onwhichatom(norb_tmp)
!
!  ! Find out which process handles TMB norb_tmp and get the keys from that process
!  do jproc=0,nproc-1
!      if (tmb%orbs%isorb_par(jproc)<norb_tmp .and. norb_tmp<=tmb%orbs%isorb_par(jproc)+tmb%orbs%norb_par(jproc,0)) then
!          iroot=jproc
!          exit
!      end if
!  end do
!  if (iproc/=iroot) then
!      ! some processes might already have it allocated
!      call deallocate_wfd(tmb%lzd%llr(ilr_tmp)%wfd)
!      call allocate_wfd(tmb%lzd%llr(ilr_tmp)%wfd)
!  end if
!  if (nproc>1) then
!      ncount = tmb%lzd%llr(ilr_tmp)%wfd%nseg_c + tmb%lzd%llr(ilr_tmp)%wfd%nseg_f
!      workarray = f_malloc(6*ncount,id='workarray')
!      if (iproc==iroot) then
!          call vcopy(2*ncount, tmb%lzd%llr(ilr_tmp)%wfd%keygloc(1,1), 1, workarray(1), 1)
!          call vcopy(2*ncount, tmb%lzd%llr(ilr_tmp)%wfd%keyglob(1,1), 1, workarray(2*ncount+1), 1)
!          call vcopy(ncount, tmb%lzd%llr(ilr_tmp)%wfd%keyvloc(1), 1, workarray(4*ncount+1), 1)
!          call vcopy(ncount, tmb%lzd%llr(ilr_tmp)%wfd%keyvglob(1), 1, workarray(5*ncount+1), 1)
!      end if
!      call mpibcast(workarray, root=iroot, comm=bigdft_mpi%mpi_comm)
!      if (iproc/=iroot) then
!          call vcopy(2*ncount, workarray(1), 1, tmb%lzd%llr(ilr_tmp)%wfd%keygloc(1,1), 1)
!          call vcopy(2*ncount, workarray(2*ncount+1), 1, tmb%lzd%llr(ilr_tmp)%wfd%keyglob(1,1), 1)
!          call vcopy(ncount, workarray(4*ncount+1), 1, tmb%lzd%llr(ilr_tmp)%wfd%keyvloc(1), 1)
!          call vcopy(ncount, workarray(5*ncount+1), 1, tmb%lzd%llr(ilr_tmp)%wfd%keyvglob(1), 1)
!      end if
!      call f_free(workarray)
!  end if
!
!
!  ndim_tmp1=tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c+7*tmb%lzd%llr(ilr_tmp)%wfd%nvctr_f
!
!  ! Determine size of phi_old and phi
!  ndim_tmp=0
!  ndim=0
!  do iorb=1,tmb%orbs%norbp
!      iiorb=tmb%orbs%isorb+iorb
!      ilr=tmb%orbs%inwhichlocreg(iiorb)
!      ndim=ndim+tmb%lzd%llr(ilr)%wfd%nvctr_c+7*tmb%lzd%llr(ilr)%wfd%nvctr_f
!      ndim_tmp=ndim_tmp+ndim_tmp1
!  end do
!
!  ! should integrate bettwer with existing reformat routines, but restart needs tidying anyway
!  psi_tmp = f_malloc_ptr(ndim_tmp,id='psi_tmp')
!
!  ! since we have a lot of different rotations, store all (reformatted) psi_i but only 1 psi_j at a time
!  ! bypass calculate_overlap_transposed and do directly in dense format
!  ! think about how to do this better...
!
!  ! get all psi_i - i.e. identity transformation, but reformatting to a single lr
!  n_tmp(1)=tmb%lzd%Llr(ilr_tmp)%d%n1
!  n_tmp(2)=tmb%lzd%Llr(ilr_tmp)%d%n2
!  n_tmp(3)=tmb%lzd%Llr(ilr_tmp)%d%n3
!  ns_tmp(1)=tmb%lzd%Llr(ilr_tmp)%ns1
!  ns_tmp(2)=tmb%lzd%Llr(ilr_tmp)%ns2
!  ns_tmp(3)=tmb%lzd%Llr(ilr_tmp)%ns3
!  nglr(1)=tmb%lzd%glr%d%n1
!  nglr(2)=tmb%lzd%glr%d%n2
!  nglr(3)=tmb%lzd%glr%d%n3
!
!  jstart=1
!  jstart_tmp=1
!  do iorb=1,tmb%orbs%norbp
!      iiorb=tmb%orbs%isorb+iorb
!      ilr=tmb%orbs%inwhichlocreg(iiorb)
!      iiat=tmb%orbs%onwhichatom(iiorb)
!
!      n(1)=tmb%lzd%Llr(ilr)%d%n1
!      n(2)=tmb%lzd%Llr(ilr)%d%n2
!      n(3)=tmb%lzd%Llr(ilr)%d%n3
!      ns(1)=tmb%lzd%Llr(ilr)%ns1
!      ns(2)=tmb%lzd%Llr(ilr)%ns2
!      ns(3)=tmb%lzd%Llr(ilr)%ns3
!
!      ! override centres
!      call set_translation(frag_trans(iiat,iiat),src=rxyz(:,iiat),dest=rxyz(:,iiat_tmp))
!!!$      frag_trans(iiat,iiat)%rot_center(:)=rxyz(:,iiat)
!!!$      frag_trans(iiat,iiat)%rot_center_new(:)=rxyz(:,iiat_tmp)
!
!      strategy=inspect_rototranslation(frag_trans(iiat,iiat),tol,tmb%lzd%llr(ilr_tmp),tmb%lzd%llr(ilr),&
!           tmb%lzd%glr%mesh_coarse,tmb%lzd%glr%mesh_coarse,dict_info)
!     
!!!$      call reformat_check(reformat,reformat_reason,tol,at,tmb%lzd%hgrids,tmb%lzd%hgrids,&
!!!$           tmb%lzd%llr(ilr)%wfd%nvctr_c,tmb%lzd%llr(ilr)%wfd%nvctr_f,&
!!!$           tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c,tmb%lzd%llr(ilr_tmp)%wfd%nvctr_f,&
!!!$           n,n_tmp,ns,ns_tmp,nglr,nglr,at%astruct%geocode,& !,tmb%lzd%llr(ilr_tmp)%geocode,&
!!!$           frag_trans(iiat,iiat),centre_old_box,centre_new_box,da,wrap_around)  
!
!      !if ((.not. reformat) .and. (.not. wrap_around)) then ! copy psi into psi_tmp
!      if (strategy == REFORMAT_COPY) then ! copy psi into psi_tmp
!          do j=1,tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c
!              psi_tmp(jstart_tmp)=tmb%psi(jstart)
!              jstart=jstart+1
!              jstart_tmp=jstart_tmp+1
!          end do
!          do j=1,7*tmb%lzd%llr(ilr_tmp)%wfd%nvctr_f-6,7
!              psi_tmp(jstart_tmp+0)=tmb%psi(jstart+0)
!              psi_tmp(jstart_tmp+1)=tmb%psi(jstart+1)
!              psi_tmp(jstart_tmp+2)=tmb%psi(jstart+2)
!              psi_tmp(jstart_tmp+3)=tmb%psi(jstart+3)
!              psi_tmp(jstart_tmp+4)=tmb%psi(jstart+4)
!              psi_tmp(jstart_tmp+5)=tmb%psi(jstart+5)
!              psi_tmp(jstart_tmp+6)=tmb%psi(jstart+6)
!              jstart=jstart+7
!              jstart_tmp=jstart_tmp+7
!          end do
!
!      else
!          phigold = f_malloc((/ 0.to.n(1), 1.to.2, 0.to.n(2), 1.to.2, 0.to.n(3), 1.to.2 /),id='phigold')
!
!          call psi_to_psig(n,tmb%lzd%llr(ilr)%wfd%nseg_c,tmb%lzd%llr(ilr)%wfd%nvctr_c,&
!               tmb%lzd%llr(ilr)%wfd%keygloc,tmb%lzd%llr(ilr)%wfd%keyvloc,&
!               tmb%lzd%llr(ilr)%wfd%nseg_f,tmb%lzd%llr(ilr)%wfd%nvctr_f,&
!               tmb%lzd%llr(ilr)%wfd%keygloc(1:,tmb%lzd%Llr(ilr)%wfd%nseg_c+1:), &
!               tmb%lzd%llr(ilr)%wfd%keyvloc(tmb%lzd%Llr(ilr)%wfd%nseg_c+1:), &
!               phigold,tmb%psi(jstart),tmb%psi(jstart+tmb%lzd%llr(ilr)%wfd%nvctr_c))
!
!          if (strategy == REFORMAT_FULL) then
!
!             call reformat_one_supportfunction(tmb%lzd%llr(ilr_tmp),tmb%lzd%llr(ilr),&
!                  tmb%lzd%glr%mesh_coarse,tmb%lzd%glr%mesh_coarse,&
!                  !at%astruct%geocode,& !,tmb%lzd%llr(ilr_tmp)%geocode,&
!                  !& tmb%lzd%hgrids,
!                  n,phigold,&
!                  !tmb%lzd%hgrids,&
!                  n_tmp,&
!                  !centre_old_box,centre_new_box,da,&
!                  frag_trans(iiat,iiat),psi_tmp(jstart_tmp:))
!
!          else
!
!             ! in this case we don't need to reformat, just re-wrap the tmb, so ilr and ilr_tmp should contain same info
!             call compress_plain(n_tmp(1),n_tmp(2),0,n_tmp(1),0,n_tmp(2),0,n_tmp(3),  &
!                  tmb%lzd%llr(ilr_tmp)%wfd%nseg_c,tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c,&
!                  tmb%lzd%llr(ilr_tmp)%wfd%keygloc(1,1),tmb%lzd%llr(ilr_tmp)%wfd%keyvloc(1),   &
!                  tmb%lzd%llr(ilr_tmp)%wfd%nseg_f,tmb%lzd%llr(ilr_tmp)%wfd%nvctr_f,&
!                  tmb%lzd%llr(ilr_tmp)%wfd%keygloc(1,tmb%lzd%llr(ilr_tmp)%wfd%nseg_c+min(1,tmb%lzd%llr(ilr_tmp)%wfd%nseg_f)),&
!                  tmb%lzd%llr(ilr_tmp)%wfd%keyvloc(tmb%lzd%llr(ilr_tmp)%wfd%nseg_c+min(1,tmb%lzd%llr(ilr_tmp)%wfd%nseg_f)), &
!                  phigold,psi_tmp(jstart_tmp),psi_tmp(jstart_tmp+tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c))
! 
!          end if
!
!          jstart=jstart+tmb%lzd%llr(ilr)%wfd%nvctr_c+7*tmb%lzd%llr(ilr)%wfd%nvctr_f
!          jstart_tmp=jstart_tmp+ndim_tmp1
!
!          call f_free(phigold)
!
!      end if
!
!      !!!debug
!      !!write(*,'(a,5(1x,I3),(1x,F8.4),5(2x,3(1x,F6.2)),5(2x,3(1x,I4)),2(1x,L2),4(1x,I5),2(1x,a))')'DEBUGr:',&
!      !!     iproc,iiat,iiorb,ilr,ilr_tmp,ddot(ndim_tmp1, psi_tmp(jstart_tmp-ndim_tmp1), 1, psi_tmp(jstart_tmp-ndim_tmp1), 1), &
!      !!     da, frag_trans(iiat,iiat)%rot_center, frag_trans(iiat,iiat)%rot_center_new, &
!      !!     centre_old_box, centre_new_box, ns, ns_tmp, n, n_tmp, nglr, &
!      !!     reformat, wrap_around, tmb%lzd%llr(ilr)%wfd%nvctr_c, tmb%lzd%llr(ilr)%wfd%nvctr_f, &
!      !!     tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c, tmb%lzd%llr(ilr_tmp)%wfd%nvctr_f, tmb%lzd%llr(ilr)%geocode, tmb%lzd%glr%geocode
!
!  end do
!
!  !not sure whether to print this - maybe actual reformatting only?
!  !call print_reformat_summary(iproc,nproc,reformat_reason)
!
!  psi_tmp_i=f_malloc(ndim_tmp1,id='psi_tmp_i')
!  psi_tmp_j=f_malloc(ndim_tmp1,id='psi_tmp_j')
!
!  !maybe make this an argument?
!  overlap=f_malloc0((/tmb%orbs%norb,tmb%orbs%norb/),id='overlap')
!
!  istart_tmp=1
!  do iiorb=1,tmb%orbs%norb
!      !iiorb=tmb%orbs%isorb+iorb
!      ilr=tmb%orbs%inwhichlocreg(iiorb)
!      iiat=tmb%orbs%onwhichatom(iiorb)
!
!      !these have already been reformatted, so need to communicate each reformatted psi_tmp
!      !first check which mpi has this tmb
!      do jproc=0,nproc-1
!          if (tmb%orbs%isorb_par(jproc)<iiorb .and. iiorb<=tmb%orbs%isorb_par(jproc)+tmb%orbs%norb_par(jproc,0)) then
!              iroot=jproc
!              exit
!          end if
!      end do
!
!      if (nproc>1) then
!          !workarray = f_malloc(ndim_tmp1,id='workarray')
!          if (iproc==iroot) then
!              !call vcopy(2*ncount, tmb%lzd%llr(ilr_tmp)%wfd%keygloc(1,1), 1, workarray(1), 1)
!              call vcopy(ndim_tmp1, psi_tmp(istart_tmp), 1, psi_tmp_i(1), 1)
!          end if
!          call mpibcast(psi_tmp_i, root=iroot, comm=bigdft_mpi%mpi_comm)
!          if (iproc==iroot) then
!              istart_tmp = istart_tmp + ndim_tmp1
!          !else
!          !    call vcopy(2*ncount, workarray(1), 1, tmb%lzd%llr(ilr_tmp)%wfd%keygloc(1,1), 1)
!          !    call vcopy(2*ncount, workarray(2*ncount+1), 1, tmb%lzd%llr(ilr_tmp)%wfd%keyglob(1,1), 1)
!          !    call vcopy(ncount, workarray(4*ncount+1), 1, tmb%lzd%llr(ilr_tmp)%wfd%keyvloc(1), 1)
!          !    call vcopy(ncount, workarray(5*ncount+1), 1, tmb%lzd%llr(ilr_tmp)%wfd%keyvglob(1), 1)
!          end if
!          !call f_free(workarray)
!      end if
!
!      ! reformat jorb for iorb then calculate overlap
!      jstart=1
!      do jorb=1,tmb%orbs%norbp
!         jjorb=tmb%orbs%isorb+jorb
!         jlr=tmb%orbs%inwhichlocreg(jjorb)
!         jjat=tmb%orbs%onwhichatom(jjorb)
!
!         ! not sure if this really saves us much, or just makes for poor load balancing
!         if (jjat>iiat) then !  .or. (jjat/=7 .and. jjat/=8) .or. (iiat/=7 .and. iiat/=8)   ) then
!            jstart=jstart+tmb%lzd%llr(jlr)%wfd%nvctr_c+7*tmb%lzd%llr(jlr)%wfd%nvctr_f
!            !overlap(jjorb,iiorb) = 0.0
!            cycle
!         end if
!
!         n(1)=tmb%lzd%Llr(jlr)%d%n1
!         n(2)=tmb%lzd%Llr(jlr)%d%n2
!         n(3)=tmb%lzd%Llr(jlr)%d%n3
!         ns(1)=tmb%lzd%Llr(jlr)%ns1
!         ns(2)=tmb%lzd%Llr(jlr)%ns2
!         ns(3)=tmb%lzd%Llr(jlr)%ns3
!
!         ! override centres
!         call set_translation(frag_trans(iiat,jjat),&
!              src=rxyz(:,jjat),dest=rxyz(:,iiat_tmp))
!         !frag_trans(iiat,jjat)%rot_center(:)=rxyz(:,jjat)
!         !frag_trans(iiat,jjat)%rot_center_new(:)=rxyz(:,iiat_tmp)
!
!         strategy=inspect_rototranslation(frag_trans(iiat,jjat),tol,&
!              tmb%lzd%llr(ilr_tmp),tmb%lzd%llr(jlr),&
!              tmb%lzd%glr%mesh_coarse,tmb%lzd%glr%mesh_coarse,dict_info)
!
!         reformat=strategy==REFORMAT_FULL
!         wrap_around=strategy==REFORMAT_WRAP
!!!$         call reformat_check(reformat,reformat_reason,tol,at,tmb%lzd%hgrids,tmb%lzd%hgrids,&
!!!$              tmb%lzd%llr(jlr)%wfd%nvctr_c,tmb%lzd%llr(jlr)%wfd%nvctr_f,&
!!!$              tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c,tmb%lzd%llr(ilr_tmp)%wfd%nvctr_f,&
!!!$              n,n_tmp,ns,ns_tmp,nglr,nglr,at%astruct%geocode,& !,tmb%lzd%llr(ilr_tmp)%geocode,&
!!!$              frag_trans(iiat,jjat),centre_old_box,centre_new_box,da,wrap_around)
!
!         if ((.not. reformat) .and. (.not. wrap_around)) then ! copy psi into psi_tmp
!             jstart_tmp=1
!             do j=1,tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c
!                 psi_tmp_j(jstart_tmp)=tmb%psi(jstart)
!                 jstart=jstart+1
!                 jstart_tmp=jstart_tmp+1
!             end do
!             do j=1,7*tmb%lzd%llr(ilr_tmp)%wfd%nvctr_f-6,7
!                 psi_tmp_j(jstart_tmp+0)=tmb%psi(jstart+0)
!                 psi_tmp_j(jstart_tmp+1)=tmb%psi(jstart+1)
!                 psi_tmp_j(jstart_tmp+2)=tmb%psi(jstart+2)
!                 psi_tmp_j(jstart_tmp+3)=tmb%psi(jstart+3)
!                 psi_tmp_j(jstart_tmp+4)=tmb%psi(jstart+4)
!                 psi_tmp_j(jstart_tmp+5)=tmb%psi(jstart+5)
!                 psi_tmp_j(jstart_tmp+6)=tmb%psi(jstart+6)
!                 jstart=jstart+7
!                 jstart_tmp=jstart_tmp+7
!             end do
!
!         else
!             phigold = f_malloc((/ 0.to.n(1), 1.to.2, 0.to.n(2), 1.to.2, 0.to.n(3), 1.to.2 /),id='phigold')
!
!             call psi_to_psig(n,tmb%lzd%llr(jlr)%wfd%nseg_c,tmb%lzd%llr(jlr)%wfd%nvctr_c,&
!                  tmb%lzd%llr(jlr)%wfd%keygloc,tmb%lzd%llr(jlr)%wfd%keyvloc,&
!                  tmb%lzd%llr(jlr)%wfd%nseg_f,tmb%lzd%llr(jlr)%wfd%nvctr_f,&
!                  tmb%lzd%llr(jlr)%wfd%keygloc(1:,tmb%lzd%Llr(jlr)%wfd%nseg_c+1:), &
!                  tmb%lzd%llr(jlr)%wfd%keyvloc(tmb%lzd%Llr(jlr)%wfd%nseg_c+1:), &
!                  phigold,tmb%psi(jstart),tmb%psi(jstart+tmb%lzd%llr(jlr)%wfd%nvctr_c))
!
!         !!!debug
!         !!!if ((iiat==7 .or. iiat==8) .and. (jjat==7 .or. jjat==8)) then
!         !!   open(20000+iiorb*100+jjorb)
!         !!   do i=1,n(1)
!         !!   do j=1,n(2)
!         !!   do k=1,n(3)
!         !!      write(20000+iiorb*100+jjorb,'(3(I6,1x),1x,2(F12.6,1x))') i,j,k,phigold(i,1,j,1,k,1),&
!         !!           dnrm2(8*(n(1)+1)*(n(2)+1)*(n(3)+1),phigold,1)
!         !!   end do
!         !!   end do
!         !!   end do
!         !!   close(20000+iiorb*100+jjorb)
!         !!!end if
!
!             if (reformat) then
!
!                call reformat_one_supportfunction(tmb%lzd%llr(ilr_tmp),tmb%lzd%llr(jlr),&
!                     tmb%lzd%glr%mesh_coarse,tmb%lzd%glr%mesh_coarse,&
!                     !at%astruct%geocode,&  !tmb%lzd%llr(ilr_tmp)%geocode,&
!                     !& tmb%lzd%hgrids,
!                     n,phigold,&
!                     !tmb%lzd%hgrids,
!                     n_tmp,&
!                     !centre_old_box,centre_new_box,da,&
!                     frag_trans(iiat,jjat),psi_tmp_j) !,tag=30000+iiorb*100+jjorb)
!
!             else
!                ! in this case we don't need to reformat, just re-wrap the tmb, so ilr and ilr_tmp should contain same info
!                call compress_plain(n_tmp(1),n_tmp(2),0,n_tmp(1),0,n_tmp(2),0,n_tmp(3),  &
!                     tmb%lzd%llr(ilr_tmp)%wfd%nseg_c,tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c,&
!                     tmb%lzd%llr(ilr_tmp)%wfd%keygloc(1,1),tmb%lzd%llr(ilr_tmp)%wfd%keyvloc(1),   &
!                     tmb%lzd%llr(ilr_tmp)%wfd%nseg_f,tmb%lzd%llr(ilr_tmp)%wfd%nvctr_f,&
!                     tmb%lzd%llr(ilr_tmp)%wfd%keygloc(1,tmb%lzd%llr(ilr_tmp)%wfd%nseg_c+min(1,tmb%lzd%llr(ilr_tmp)%wfd%nseg_f)),&
!                     tmb%lzd%llr(ilr_tmp)%wfd%keyvloc(tmb%lzd%llr(ilr_tmp)%wfd%nseg_c+min(1,tmb%lzd%llr(ilr_tmp)%wfd%nseg_f)), &
!                     phigold,psi_tmp_j(1),psi_tmp_j(tmb%lzd%llr(ilr_tmp)%wfd%nvctr_c+1))
!             end if
!
!             jstart=jstart+tmb%lzd%llr(jlr)%wfd%nvctr_c+7*tmb%lzd%llr(jlr)%wfd%nvctr_f   
!
!             call f_free(phigold)
!         end if
!
!         overlap(iiorb,jjorb) = ddot(ndim_tmp1, psi_tmp_i(1), 1, psi_tmp_j(1), 1)
!         overlap(jjorb,iiorb) = overlap(iiorb,jjorb)
!
!         !!!debug
!         !!write(*,'(a,5(1x,I3),1x,2(1x,F8.4),1x,2(1x,L2),1x,2(1x,F7.2))')'DEBUGr2:',&
!         !!     iproc,iiat,jjat,iiorb,jjorb,ddot(ndim_tmp1, psi_tmp_j(1), 1, psi_tmp_j(1), 1), &
!         !!     ddot(ndim_tmp1, psi_tmp_i(1), 1, psi_tmp_j(1), 1), &
!         !!     reformat, wrap_around, frag_trans(iiat,jjat)%Werror, frag_trans(iiat,jjat)%theta/(4.0_gp*atan(1.d0)/180.0_gp)
!
!         !!!debug
!         !!write(*,'(a,5(1x,I4),2(2x,F12.6))')'iproc,iat,jat,iorb,jorb,ovrlp',iproc,iiat,jjat,iiorb,jjorb,overlap(jjorb,iiorb),&
!         !!      tmb%linmat%ovrlp_%matrix(iiorb,jjorb,1)
!
!         !!!debug
!         !!!if ((iiat==7 .or. iiat==8) .and. (jjat==7 .or. jjat==8)) then
!         !!   open(10000+iiorb*100+jjorb)
!         !!   do i=1,ndim_tmp1
!         !!      write(10000+iiorb*100+jjorb,'(I6,1x,2(F12.6,1x),1x,3(F12.6,1x))') i,psi_tmp_i(i),psi_tmp_j(i),&
!         !!           ddot(ndim_tmp1, psi_tmp_i(1), 1, psi_tmp_i(1), 1),&
!         !!           ddot(ndim_tmp1, psi_tmp_j(1), 1, psi_tmp_j(1), 1),&
!         !!           ddot(ndim_tmp1, psi_tmp_i(1), 1, psi_tmp_j(1), 1)
!         !!   end do
!         !!   close(10000+iiorb*100+jjorb)
!         !!!end if
!
!      end do
!
!      if (iproc==0) write(*,'(F6.2,a)') 100.0d0*real(iiorb,dp)/real(tmb%orbs%norb,dp),'%'
!  end do
!
!  call fmpi_allreduce(overlap, FMPI_SUM, comm=bigdft_mpi%mpi_comm)
!
!  ! print also various files useful for more direct analysis - eventually tidy this into better formatted outputs
!  if (iproc==0) then
!     open(98)
!     open(97)
!     open(96)
!     open(95)
!     do iat=1,at%astruct%nat
!        iorba=0
!        do iorb=1,tmb%orbs%norb
!           iiat=tmb%orbs%onwhichatom(iorb)
!           if (iat/=iiat) cycle
!           iorba=iorba+1
!
!           do jat=1,at%astruct%nat
!              jorba=0
!              do jorb=1,tmb%orbs%norb
!                 jjat=tmb%orbs%onwhichatom(jorb)
!                 if (jat/=jjat) cycle
!                 jorba=jorba+1
!
!                 ! full matrix
!                 write(98,'(2(a,1x,I5,1x),4(I5,1x),2x,5(F12.6,1x))') &
!                      trim(at%astruct%atomnames(at%astruct%iatype(iat))),iat,&
!                      trim(at%astruct%atomnames(at%astruct%iatype(jat))),jat,&
!                      iorb,jorb,iorba,jorba,overlap(iorb,jorb),&
!                      tmb%linmat%ovrlp_%matrix(iorb,jorb,1),&
!                      overlap(iorb,jorb)-tmb%linmat%ovrlp_%matrix(iorb,jorb,1),&
!                      frag_trans(iat,jat)%Werror,frag_trans(iat,jat)%theta/(4.0_gp*atan(1.d0)/180.0_gp)
!
!                 ! same 'number' tmb, no rotation
!                 if ((iat==jat .or. frag_trans(iat,jat)%theta==0.0) .and. iorba==jorba) then
!                    write(97,'(2(a,1x,I5,1x),4(I5,1x),2x,5(F12.6,1x))') &
!                         trim(at%astruct%atomnames(at%astruct%iatype(iat))),iat,&
!                         trim(at%astruct%atomnames(at%astruct%iatype(jat))),jat,&
!                         iorb,jorb,iorba,jorba,overlap(iorb,jorb),&
!                         tmb%linmat%ovrlp_%matrix(iorb,jorb,1),&
!                         overlap(iorb,jorb)-tmb%linmat%ovrlp_%matrix(iorb,jorb,1),&
!                         frag_trans(iat,jat)%Werror,frag_trans(iat,jat)%theta/(4.0_gp*atan(1.d0)/180.0_gp)
!                 end if
!
!                 ! same 'number' tmb
!                 if (iorba==jorba) then
!                    write(96,'(2(a,1x,I5,1x),4(I5,1x),2x,5(F12.6,1x))') &
!                         trim(at%astruct%atomnames(at%astruct%iatype(iat))),iat,&
!                         trim(at%astruct%atomnames(at%astruct%iatype(jat))),jat,&
!                         iorb,jorb,iorba,jorba,overlap(iorb,jorb),&
!                         tmb%linmat%ovrlp_%matrix(iorb,jorb,1),&
!                         overlap(iorb,jorb)-tmb%linmat%ovrlp_%matrix(iorb,jorb,1),&
!                         frag_trans(iat,jat)%Werror,frag_trans(iat,jat)%theta/(4.0_gp*atan(1.d0)/180.0_gp)
!                 end if
!
!                 ! first tmb of each atom
!                 if (iorba==jorba .and. iorba==1) then
!                    write(95,'(2(a,1x,I5,1x),4(I5,1x),2x,5(F12.6,1x))') &
!                         trim(at%astruct%atomnames(at%astruct%iatype(iat))),iat,&
!                         trim(at%astruct%atomnames(at%astruct%iatype(jat))),jat,&
!                         iorb,jorb,iorba,jorba,overlap(iorb,jorb),&
!                         tmb%linmat%ovrlp_%matrix(iorb,jorb,1),&
!                         overlap(iorb,jorb)-tmb%linmat%ovrlp_%matrix(iorb,jorb,1),&
!                         frag_trans(iat,jat)%Werror,frag_trans(iat,jat)%theta/(4.0_gp*atan(1.d0)/180.0_gp)
!                 end if
!
!              end do
!           end do
!        end do
!     end do
!     close(98)
!     close(97)
!     close(96)
!     close(95)
!  end if
!
!
!  call f_free(overlap)
!  call f_free(psi_tmp_i)
!  call f_free(psi_tmp_j)
!  call f_free_ptr(psi_tmp)
!
!  do iat=1,at%astruct%nat
!     ! nullify these as we are pointing to at%astruct versions
!     nullify(ref_frags_atomic(iat)%astruct_frg%atomnames)
!     nullify(ref_frags_atomic(iat)%astruct_env%atomnames)
!     call fragment_free(ref_frags_atomic(iat))
!
!     nullify(ref_frags_atomic_dfrag(iat)%astruct_frg%atomnames)
!     nullify(ref_frags_atomic_dfrag(iat)%astruct_env%atomnames)
!     call fragment_free(ref_frags_atomic_dfrag(iat))
!  end do
!  deallocate(ref_frags_atomic_dfrag)
!  deallocate(frag_trans)
!  call f_free(ifrag_ref)
!
!
!contains
!
!
!  subroutine setup_frags_from_astruct(ref_frags)
!     implicit none
!     type(system_fragment), intent(out) :: ref_frags
!
!     ref_frags=fragment_null()
!
!     ! fill in relevant ref_frags info
!     ref_frags%astruct_frg%nat=1
!     ref_frags%astruct_env%nat=0
!
!     ! copy some stuff from astruct
!     ref_frags%astruct_frg%inputfile_format = at%astruct%inputfile_format
!     ref_frags%astruct_frg%units = at%astruct%units
!     ref_frags%astruct_frg%geocode = at%astruct%geocode
!     ! coordinates can just be zero as we only have 1 atom
!     ref_frags%astruct_frg%rxyz = f_malloc0_ptr((/3,1/),id=' ref_frags%astruct_frg%rxyz')
!
!     ! now deal with atom types - easier to keep this coherent with at%astruct rather than having one type
!     ref_frags%astruct_frg%ntypes = at%astruct%ntypes
!     ref_frags%astruct_frg%atomnames => at%astruct%atomnames
!     ref_frags%astruct_frg%iatype = f_malloc_ptr(1,id='ref_frags%astruct_frg%iatype')
!
!     ! polarization etc is irrelevant
!     ref_frags%astruct_frg%input_polarization = f_malloc0_ptr(1,&
!          id='ref_frags%astruct_frg%input_polarization')
!     ref_frags%astruct_frg%ifrztyp = f_malloc0_ptr(1,id='ref_frags%astruct_frg%ifrztyp')
!     ref_frags%astruct_frg%iatype(1) = at%astruct%iatype(iat)
!     ref_frags%astruct_frg%input_polarization(1) = 100
!
!     call init_minimal_orbitals_data(iproc, nproc, 1, input, ref_frags%astruct_frg, &
!          ref_frags%fbasis%forbs, at%astruct)
!
!  end subroutine setup_frags_from_astruct
!
!
!
!END SUBROUTINE tmb_overlap_onsite_rotate





!!subroutine tmb_overlap_onsite_rotate(iproc, nproc, at, tmb, rxyz)
!!
!!  use module_base
!!  use module_types
!!  use module_interfaces
!!  use module_fragments
!!  use communications_base, only: comms_linear_null, deallocate_comms_linear
!!  use communications_init, only: init_comms_linear
!!  use communications, only: transpose_localized
!!  implicit none
!!
!!  ! Calling arguments
!!  integer,intent(in) :: iproc, nproc
!!  type(atoms_data), intent(inout) :: at
!!  type(DFT_wavefunction),intent(in):: tmb
!!  real(gp),dimension(3,at%astruct%nat),intent(in) :: rxyz
!!
!!  ! Local variables
!!  logical :: reformat
!!  integer :: iorb,i_stat,i_all,istart,jstart
!!  integer :: iiorb,ilr,iiat,j,iis1,iie1,i1
!!  integer :: jlr,iiat_tmp,ndim_tmp,ndim,norb_tmp,iat,jjat,jjorb
!!  integer, dimension(3) :: ns,nsj,n,nj
!!  real(gp), dimension(3) :: centre_old_box, centre_new_box, da
!!  real(wp), dimension(:,:,:,:,:,:), allocatable :: phigold
!!  real(wp), dimension(:), pointer :: psi_tmp, psit_c_tmp, psit_f_tmp, norm
!!  integer, dimension(0:6) :: reformat_reason
!!  type(comms_linear) :: collcom_tmp
!!  type(local_zone_descriptors) :: lzd_tmp
!!  real(gp) :: tol
!!  character(len=*),parameter:: subname='tmb_overlap_onsite'
!!  type(fragment_transformation) :: frag_trans
!!  real(gp), dimension(:,:), allocatable :: rxyz4_new, rxyz4_ref, rxyz_new, rxyz_ref
!!  real(gp), dimension(:), allocatable :: dist
!!  integer, dimension(:), allocatable :: ipiv
!!
!!  ! move all psi into psi_tmp all centred in the same place and calculate overlap matrix
!!  tol=1.d-3
!!  reformat_reason=0
!!
!!  !arbitrarily pick the middle one as assuming it'll be near the centre of structure
!!  !and therefore have large fine grid
!!  norb_tmp=tmb%orbs%norb/2
!!  jlr=tmb%orbs%inwhichlocreg(norb_tmp)
!!  iiat_tmp=tmb%orbs%onwhichatom(norb_tmp)
!!  jjorb=norb_tmp
!!  jjat=iiat_tmp
!!
!!  ! find biggest instead
!!  !do ilr=1,tmb%lzr%nlr
!!  !  if (tmb%lzd%llr(ilr)%wfd%nvctr_c
!!  !end do
!!
!!  ! Determine size of phi_old and phi
!!  ndim_tmp=0
!!  ndim=0
!!  do iorb=1,tmb%orbs%norbp
!!      iiorb=tmb%orbs%isorb+iorb
!!      ilr=tmb%orbs%inwhichlocreg(iiorb)
!!      ndim=ndim+tmb%lzd%llr(ilr)%wfd%nvctr_c+7*tmb%lzd%llr(ilr)%wfd%nvctr_f
!!      ndim_tmp=ndim_tmp+tmb%lzd%llr(jlr)%wfd%nvctr_c+7*tmb%lzd%llr(jlr)%wfd%nvctr_f
!!  end do
!!
!!  ! should integrate bettwer with existing reformat routines, but restart needs tidying anyway
!!  allocate(psi_tmp(ndim_tmp),stat=i_stat)
!!  call memocc(i_stat,psi_tmp,'psi_tmp',subname)
!!
!!  allocate(rxyz4_ref(3,min(4,at%astruct%nat)), stat=i_stat)
!!  call memocc(i_stat, rxyz4_ref, 'rxyz4_ref', subname)
!!  allocate(rxyz4_new(3,min(4,at%astruct%nat)), stat=i_stat)
!!  call memocc(i_stat, rxyz4_new, 'rxyz4_ref', subname)
!!
!!  allocate(rxyz_ref(3,at%astruct%nat), stat=i_stat)
!!  call memocc(i_stat, rxyz_ref, 'rxyz_ref', subname)
!!  allocate(rxyz_new(3,at%astruct%nat), stat=i_stat)
!!  call memocc(i_stat, rxyz_new, 'rxyz_ref', subname)
!!  allocate(dist(at%astruct%nat), stat=i_stat)
!!  call memocc(i_stat, dist, 'dist', subname)
!!  allocate(ipiv(at%astruct%nat), stat=i_stat)
!!  call memocc(i_stat, ipiv, 'ipiv', subname)
!!
!!  istart=1
!!  jstart=1
!!  do iorb=1,tmb%orbs%norbp
!!      iiorb=tmb%orbs%isorb+iorb
!!      ilr=tmb%orbs%inwhichlocreg(iiorb)
!!      iiat=tmb%orbs%onwhichatom(iiorb)
!!
!!      !do jjorb=1,tmb%orbs%norb
!!      !   !jlr=tmb%orbs%inwhichlocreg(jjorb)
!!      !   jjat=tmb%orbs%onwhichatom(jjorb)
!!
!!         n(1)=tmb%lzd%Llr(ilr)%d%n1
!!         n(2)=tmb%lzd%Llr(ilr)%d%n2
!!         n(3)=tmb%lzd%Llr(ilr)%d%n3
!!         nj(1)=tmb%lzd%Llr(jlr)%d%n1
!!         nj(2)=tmb%lzd%Llr(jlr)%d%n2
!!         nj(3)=tmb%lzd%Llr(jlr)%d%n3
!!         ns(1)=tmb%lzd%Llr(ilr)%ns1
!!         ns(2)=tmb%lzd%Llr(ilr)%ns2
!!         ns(3)=tmb%lzd%Llr(ilr)%ns3
!!         nsj(1)=tmb%lzd%Llr(jlr)%ns1
!!         nsj(2)=tmb%lzd%Llr(jlr)%ns2
!!         nsj(3)=tmb%lzd%Llr(jlr)%ns3
!!
!!         ! find fragment transformation using 3 nearest neighbours
!!         do iat=1,at%astruct%nat
!!            rxyz_new(:,iat)=rxyz(:,iat)
!!            rxyz_ref(:,iat)=rxyz(:,iat)
!!         end do
!!
!!         ! use atom position
!!         frag_trans%rot_center=rxyz(:,iiat)
!!         frag_trans%rot_center_new=rxyz(:,jjat)
!!
!!        ! shift rxyz wrt center of rotation
!!         do iat=1,at%astruct%nat
!!            rxyz_ref(:,iat)=rxyz_ref(:,iat)-frag_trans%rot_center
!!            rxyz_new(:,iat)=rxyz_new(:,iat)-frag_trans%rot_center_new
!!         end do
!!
!!         ! find distances from this atom, sort atoms into neighbour order and take atom and 3 nearest neighbours
!!         do iat=1,at%astruct%nat
!!            dist(iat)=-dsqrt(rxyz_ref(1,iat)**2+rxyz_ref(2,iat)**2+rxyz_ref(3,iat)**2)
!!         end do
!!         call sort_positions(at%astruct%nat,dist,ipiv)
!!         do iat=1,min(4,at%astruct%nat)
!!            rxyz4_ref(:,iat)=rxyz_ref(:,ipiv(iat))
!!         end do
!!
!!         ! find distances from this atom, sort atoms into neighbour order and take atom and 3 nearest neighbours
!!         do iat=1,at%astruct%nat
!!            dist(iat)=-dsqrt(rxyz_new(1,iat)**2+rxyz_new(2,iat)**2+rxyz_new(3,iat)**2)
!!         end do
!!         call sort_positions(at%astruct%nat,dist,ipiv)
!!         do iat=1,min(4,at%astruct%nat)
!!            rxyz4_new(:,iat)=rxyz_new(:,ipiv(iat))
!!         end do
!!
!!         call find_frag_trans(min(4,at%astruct%nat),rxyz4_ref,rxyz4_new,frag_trans)
!!
!!         write(*,'(A,4(I3,1x),3(F12.6,1x),F12.6)') 'iorb,jorb,iat,jat,rot_axis,theta',&
!!              iiorb,jjorb,iiat,jjat,frag_trans%rot_axis,frag_trans%theta/(4.0_gp*atan(1.d0)/180.0_gp)
!!
!!         !frag_trans%theta=0.0d0*(4.0_gp*atan(1.d0)/180.0_gp)
!!         !frag_trans%rot_axis=(/1.0_gp,0.0_gp,0.0_gp/)
!!         !frag_trans%rot_center(:)=rxyz(:,iiat)
!!         !overwrite rot_center_new to account for llr_tmp being in different location
!!         frag_trans%rot_center_new(:)=rxyz(:,iiat_tmp)
!!
!!         call reformat_check(reformat,reformat_reason,tol,at,tmb%lzd%hgrids,tmb%lzd%hgrids,&
!!              tmb%lzd%llr(ilr)%wfd%nvctr_c,tmb%lzd%llr(ilr)%wfd%nvctr_f,&
!!              tmb%lzd%llr(jlr)%wfd%nvctr_c,tmb%lzd%llr(jlr)%wfd%nvctr_f,&
!!              n,nj,ns,nsj,frag_trans,centre_old_box,centre_new_box,da)
!!
!!         if (.not. reformat) then ! copy psi into psi_tmp
!!            do j=1,tmb%lzd%llr(jlr)%wfd%nvctr_c
!!               psi_tmp(jstart)=tmb%psi(istart)
!!               istart=istart+1
!!               jstart=jstart+1
!!            end do
!!            do j=1,7*tmb%lzd%llr(ilr)%wfd%nvctr_f-6,7
!!               psi_tmp(jstart+0)=tmb%psi(istart+0)
!!               psi_tmp(jstart+1)=tmb%psi(istart+1)
!!               psi_tmp(jstart+2)=tmb%psi(istart+2)
!!               psi_tmp(jstart+3)=tmb%psi(istart+3)
!!               psi_tmp(jstart+4)=tmb%psi(istart+4)
!!               psi_tmp(jstart+5)=tmb%psi(istart+5)
!!               psi_tmp(jstart+6)=tmb%psi(istart+6)
!!               istart=istart+7
!!               jstart=jstart+7
!!            end do
!!         else
!!            allocate(phigold(0:n(1),2,0:n(2),2,0:n(3),2+ndebug),stat=i_stat)
!!            call memocc(i_stat,phigold,'phigold',subname)
!!
!!            call psi_to_psig(n,tmb%lzd%llr(ilr)%wfd%nvctr_c,tmb%lzd%llr(ilr)%wfd%nvctr_f,&
!!                 tmb%lzd%llr(ilr)%wfd%nseg_c,tmb%lzd%llr(ilr)%wfd%nseg_f,&
!!                 tmb%lzd%llr(ilr)%wfd%keyvloc,tmb%lzd%llr(ilr)%wfd%keygloc,istart,tmb%psi(jstart),phigold)
!!
!!            call reformat_one_supportfunction(tmb%lzd%llr(jlr),tmb%lzd%llr(ilr),tmb%lzd%llr(jlr)%geocode,&
!!                 tmb%lzd%hgrids,n,phigold,tmb%lzd%hgrids,nj,centre_old_box,centre_new_box,da,&
!!                 frag_trans,psi_tmp(jstart:))
!!
!!            jstart=jstart+tmb%lzd%llr(jlr)%wfd%nvctr_c+7*tmb%lzd%llr(jlr)%wfd%nvctr_f
!!
!!            i_all=-product(shape(phigold))*kind(phigold)
!!            deallocate(phigold,stat=i_stat)
!!            call memocc(i_stat,i_all,'phigold',subname)
!!        end if
!!
!!     !end do
!!  end do
!!
!!
!!  i_all = -product(shape(ipiv))*kind(ipiv)
!!  deallocate(ipiv,stat=i_stat)
!!  call memocc(i_stat,i_all,'ipiv',subname)
!!  i_all = -product(shape(dist))*kind(dist)
!!  deallocate(dist,stat=i_stat)
!!  call memocc(i_stat,i_all,'dist',subname)
!!  i_all = -product(shape(rxyz_ref))*kind(rxyz_ref)
!!  deallocate(rxyz_ref,stat=i_stat)
!!  call memocc(i_stat,i_all,'rxyz_ref',subname)
!!  i_all = -product(shape(rxyz_new))*kind(rxyz_new)
!!  deallocate(rxyz_new,stat=i_stat)
!!  call memocc(i_stat,i_all,'rxyz_new',subname)
!!  i_all = -product(shape(rxyz4_ref))*kind(rxyz4_ref)
!!  deallocate(rxyz4_ref,stat=i_stat)
!!  call memocc(i_stat,i_all,'rxyz4_ref',subname)
!!  i_all = -product(shape(rxyz4_new))*kind(rxyz4_new)
!!  deallocate(rxyz4_new,stat=i_stat)
!!  call memocc(i_stat,i_all,'rxyz4_new',subname)
!!
!!  call print_reformat_summary(iproc,reformat_reason)
!!
!!  ! now that they are all in one lr, need to calculate overlap matrix
!!  ! make lzd_tmp contain all identical lrs
!!  lzd_tmp%linear=tmb%lzd%linear
!!  lzd_tmp%nlr=tmb%lzd%nlr
!!  lzd_tmp%lintyp=tmb%lzd%lintyp
!!  lzd_tmp%ndimpotisf=tmb%lzd%ndimpotisf
!!  lzd_tmp%hgrids(:)=tmb%lzd%hgrids(:)
!!
!!  call nullify_locreg_descriptors(lzd_tmp%glr)
!!  call copy_locreg_descriptors(tmb%lzd%glr, lzd_tmp%glr)
!!
!!  iis1=lbound(tmb%lzd%llr,1)
!!  iie1=ubound(tmb%lzd%llr,1)
!!  allocate(lzd_tmp%llr(iis1:iie1), stat=i_stat)
!!  do i1=iis1,iie1
!!     call nullify_locreg_descriptors(lzd_tmp%llr(i1))
!!     call copy_locreg_descriptors(tmb%lzd%llr(jlr), lzd_tmp%llr(i1))
!!  end do
!!
!!  !call nullify_comms_linear(collcom_tmp)
!!  collcom_tmp=comms_linear_null()
!!  call init_comms_linear(iproc, nproc, ndim_tmp, tmb%orbs, lzd_tmp, collcom_tmp)
!!
!!  allocate(psit_c_tmp(sum(collcom_tmp%nrecvcounts_c)), stat=i_stat)
!!  call memocc(i_stat, psit_c_tmp, 'psit_c_tmp', subname)
!!
!!  allocate(psit_f_tmp(7*sum(collcom_tmp%nrecvcounts_f)), stat=i_stat)
!!  call memocc(i_stat, psit_f_tmp, 'psit_f_tmp', subname)
!!
!!  call transpose_localized(iproc, nproc, ndim_tmp, tmb%orbs, collcom_tmp, &
!!       psi_tmp, psit_c_tmp, psit_f_tmp, lzd_tmp)
!!
!!  ! normalize psi
!!  allocate(norm(tmb%orbs%norb), stat=i_stat)
!!  call memocc(i_stat, norm, 'norm', subname)
!!  call normalize_transposed(iproc, nproc, tmb%orbs, collcom_tmp, psit_c_tmp, psit_f_tmp, norm)
!!  i_all = -product(shape(norm))*kind(norm)
!!  deallocate(norm,stat=i_stat)
!!  call memocc(i_stat,i_all,'norm',subname)
!!
!!  call calculate_pulay_overlap(iproc, nproc, tmb%orbs, tmb%orbs, collcom_tmp, collcom_tmp, &
!!       psit_c_tmp, psit_c_tmp, psit_f_tmp, psit_f_tmp, tmb%linmat%ovrlp%matrix)
!!
!!  call deallocate_comms_linear(collcom_tmp)
!!  call deallocate_local_zone_descriptors(lzd_tmp)
!!
!!  i_all = -product(shape(psit_c_tmp))*kind(psit_c_tmp)
!!  deallocate(psit_c_tmp,stat=i_stat)
!!  call memocc(i_stat,i_all,'psit_c_tmp',subname)
!!
!!  i_all = -product(shape(psit_f_tmp))*kind(psit_f_tmp)
!!  deallocate(psit_f_tmp,stat=i_stat)
!!  call memocc(i_stat,i_all,'psit_f_tmp',subname)
!!
!!  i_all = -product(shape(psi_tmp))*kind(psi_tmp)
!!  deallocate(psi_tmp,stat=i_stat)
!!  call memocc(i_stat,i_all,'psi_tmp',subname)
!!
!!END SUBROUTINE tmb_overlap_onsite_rotate


!!!not correct - not sure if it's worth fixing as only memguess uses it currently
!!subroutine readonewave_linear(unitwf,useFormattedInput,iorb,iproc,n,ns,&
!!     & hgrids,at,llr,rxyz_old,rxyz,locrad,locregCenter,confPotOrder,&
!!     & confPotprefac,psi,eval,onwhichatom,lr,glr,reformat_reason)
!!  use module_base
!!  use module_types
!!  !use internal_io
!!  use module_interfaces, only: reformat_one_supportfunction
!!  use yaml_output
!!  use module_fragments
!!  use io, only: io_read_descr_linear, io_error, read_psi_compress
!!  implicit none
!!  logical, intent(in) :: useFormattedInput
!!  integer, intent(in) :: unitwf,iorb,iproc
!!  integer, dimension(3), intent(in) :: n,ns
!!  !type(wavefunctions_descriptors), intent(in) :: wfd
!!  type(locreg_descriptors), intent(in) :: llr
!!  type(atoms_data), intent(in) :: at
!!  real(gp), dimension(3), intent(in) :: hgrids
!!  real(gp), dimension(3,at%astruct%nat), intent(in) :: rxyz
!!  integer, intent(out) :: confPotOrder
!!  real(gp), intent(out) :: locrad, confPotprefac
!!  real(wp), intent(out) :: eval
!!  real(gp), dimension(3), intent(out) :: locregCenter
!!  real(gp), dimension(3,at%astruct%nat), intent(out) :: rxyz_old
!!  real(wp), dimension(:), pointer :: psi
!!  integer, dimension(*), intent(in) :: onwhichatom
!!  type(locreg_descriptors), intent(in) :: lr, glr
!!  integer, dimension(0:6), intent(out) :: reformat_reason
!!
!!  !local variables
!!  character(len=*), parameter :: subname='readonewave_linear'
!!  character(len = 256) :: error
!!  logical :: lstat,reformat
!!  integer :: iorb_old,nvctr_c_old,nvctr_f_old,iiat
!!  integer :: i1,i2,i3,iel,onwhichatom_tmp
!!  integer, dimension(3) :: ns_old,n_old
!!  real(gp) :: tol
!!  real(gp), dimension(3) :: hgrids_old, centre_old_box, centre_new_box, da
!!  real(gp) :: tt,t1,t2,t3,t4,t5,t6,t7
!!  real(wp), dimension(:,:,:,:,:,:), allocatable :: psigold
!!  type(fragment_transformation) :: frag_trans
!!  ! DEBUG
!!  ! character(len=12) :: orbname
!!  ! real(wp), dimension(:), allocatable :: gpsi
!!
!!
!!  call io_read_descr_linear(unitwf, useFormattedInput, iorb_old, eval, n_old(1), n_old(2), n_old(3), &
!!       ns_old(1), ns_old(2), ns_old(3), hgrids_old, lstat, error, onwhichatom_tmp, locrad, &
!!       locregCenter, confPotOrder, confPotprefac, nvctr_c_old, nvctr_f_old, at%astruct%nat, rxyz_old)
!!
!!  if (.not. lstat) call io_error(trim(error))
!!  if (iorb_old /= iorb) stop 'readonewave_linear'
!!
!!  iiat=onwhichatom(iorb)
!!  tol=1.d-3
!!
!!  !why these hard-coded values?
!!  frag_trans%theta=20.0d0*(4.0_gp*atan(1.d0)/180.0_gp)
!!  frag_trans%rot_axis=(/1.0_gp,0.0_gp,0.0_gp/)
!!  frag_trans%rot_center(:)=(/7.8d0,11.8d0,11.6d0/)
!!  frag_trans%rot_center_new(:)=(/7.8d0,11.2d0,11.8d0/)
!!
!!  call reformat_check(reformat,reformat_reason,tol,at,hgrids,hgrids_old,&
!!       nvctr_c_old,nvctr_f_old,llr%wfd%nvctr_c,llr%wfd%nvctr_f,&
!!       n_old,n,ns_old,ns,frag_trans,centre_old_box,centre_new_box,da)
!!
!!  if (.not. reformat) then
!!     call read_psi_compress(unitwf, useFormattedInput, nvctr_c_old, nvctr_f_old, psi, lstat, error)
!!     if (.not. lstat) call io_error(trim(error))
!!  else
!!     ! add derivative functions at a later date? (needs orbs and lzd)
!!     psigold = f_malloc0((/ 0.to.n_old(1), 1.to.2, 0.to.n_old(2), 1.to.2, 0.to.n_old(3), 1.to.2 /),id='psigold')
!!
!!     !call f_zero(8*(n_old(1)+1)*(n_old(2)+1)*(n_old(3)+1),psigold)
!!     do iel=1,nvctr_c_old
!!        if (useFormattedInput) then
!!           read(unitwf,*) i1,i2,i3,tt
!!        else
!!           read(unitwf) i1,i2,i3,tt
!!        end if
!!        psigold(i1,1,i2,1,i3,1)=tt
!!     enddo
!!     do iel=1,nvctr_f_old
!!        if (useFormattedInput) then
!!           read(unitwf,*) i1,i2,i3,t1,t2,t3,t4,t5,t6,t7
!!        else
!!           read(unitwf) i1,i2,i3,t1,t2,t3,t4,t5,t6,t7
!!        end if
!!        psigold(i1,2,i2,1,i3,1)=t1
!!        psigold(i1,1,i2,2,i3,1)=t2
!!        psigold(i1,2,i2,2,i3,1)=t3
!!        psigold(i1,1,i2,1,i3,2)=t4
!!        psigold(i1,2,i2,1,i3,2)=t5
!!        psigold(i1,1,i2,2,i3,2)=t6
!!        psigold(i1,2,i2,2,i3,2)=t7
!!     enddo
!!
!!     ! NB assuming here geocode is the same in glr and llr
!!     call reformat_one_supportfunction(llr,llr,at%astruct%geocode,hgrids_old,n_old,psigold,hgrids,n, &
!!         centre_old_box,centre_new_box,da,frag_trans,psi)
!!
!!     call f_free(psigold)
!!
!!  endif
!!
!!  !! DEBUG - plot in global box - CHECK WITH REFORMAT ETC IN LRs
!!  !allocate (gpsi(glr%wfd%nvctr_c+7*glr%wfd%nvctr_f),stat=i_stat)
!!  !call memocc(i_stat,gpsi,'gpsi',subname)
!!  !
!!  !call f_zero(glr%wfd%nvctr_c+7*glr%wfd%nvctr_f,gpsi)
!!  !call Lpsi_to_global2(iproc, llr%%wfd%nvctr_c+7*lr%wfd%nvctr_f, glr%wfd%nvctr_c+7*glr%wfd%nvctr_f, &
!!  !     1, 1, 1, glr, lr, psi, gpsi)
!!  !
!!  !write(orbname,*) iorb
!!  !call plot_wf(trim(adjustl(orbname)),1,at,1.0_dp,glr,hgrids(1),hgrids(2),hgrids(3),rxyz,gpsi)
!!  !!call plot_wf(trim(adjustl(orbname)),1,at,1.0_dp,lr,hx,hy,hz,rxyz,psi)
!!  !
!!  !i_all=-product(shape(gpsi))*kind(gpsi)
!!  !deallocate(gpsi,stat=i_stat)
!!  !call memocc(i_stat,i_all,'gpsi',subname)
!!  !! END DEBUG
!!
!!END SUBROUTINE readonewave_linear

!!!!> Checks whether reformatting is needed based on various criteria and returns final shift and centres needed for reformat
!!!subroutine reformat_check(reformat_needed,reformat_reason,tol,at,hgrids_old,hgrids,nvctr_c_old,nvctr_f_old,&
!!!     nvctr_c,nvctr_f,n_old,n,ns_old,ns,nglr_old,nglr,geocode,frag_trans,wrap_around)
!!!  !,centre_old_box,centre_new_box,da
!!!  use module_base
!!!  use module_types
!!!  use module_fragments
!!!  use yaml_output
!!!  use box
!!!  use bounds, only: ext_buffers_coarse
!!!  use rototranslations
!!!  implicit none
!!!
!!!  logical, intent(out) :: reformat_needed ! logical telling whether reformat is needed
!!!  integer, dimension(0:7), intent(inout) :: reformat_reason ! array giving reasons for reformatting
!!!  real(gp), intent(in) :: tol ! tolerance for rotations and shifts
!!!  type(atoms_data), intent(in) :: at
!!!  real(gp), dimension(3), intent(in) :: hgrids, hgrids_old
!!!  integer, intent(in) :: nvctr_c, nvctr_f, nvctr_c_old, nvctr_f_old
!!!  integer, dimension(3), intent(in) :: n, n_old, ns, ns_old, nglr_old, nglr
!!!  character(len=1), intent(in) :: geocode !< @copydoc poisson_solver::doc::geocode
!!!!  real(gp), dimension(3), intent(out) :: centre_old_box, centre_new_box ! centres of rotation wrt box
!!!!  real(gp), dimension(3), intent(out) :: da ! shift to be used in reformat
!!!  type(rototranslation), intent(in) :: frag_trans ! includes centres of rotation in global coordinates, shift and angle
!!!  logical, intent(out) :: wrap_around ! periodic case where no reformatting is needed but tmb needs 'unwrapping'
!!!
!!!
!!!  ! local variables
!!!  real(gp) :: displ !, mindist
!!!  integer, dimension(3) :: nb
!!!  logical, dimension(3) :: per
!!!  integer :: i
!!!  !real(gp), dimension(3) :: centre_new
!!!  type(cell) :: mesh
!!!
!!!
!!!  mesh=cell_new(at%astruct%geocode,n,hgrids)
!!!  !conditions for periodicity in the three directions
!!!  per(1)=(geocode /= 'F')
!!!  per(2)=(geocode == 'P')
!!!  per(3)=(geocode /= 'F')
!!!
!!!  !buffers related to periodicity
!!!  !WARNING: the boundary conditions are not assumed to change between new and old
!!!  call ext_buffers_coarse(per(1),nb(1))
!!!  call ext_buffers_coarse(per(2),nb(2))
!!!  call ext_buffers_coarse(per(3),nb(3))
!!! 
!!!  
!!!!!$  !use new (internal) version of mindist, mindist doesn't do the right thing in this case
!!!!!$  ! centre of rotation with respect to start of box
!!!!!$  centre_old_box(1)=mindist_new(per(1),at%astruct%cell_dim(1),frag_trans%rot_center(1),hgrids_old(1)*(ns_old(1)-0.5_dp*nb(1)))
!!!!!$  centre_old_box(2)=mindist_new(per(2),at%astruct%cell_dim(2),frag_trans%rot_center(2),hgrids_old(2)*(ns_old(2)-0.5_dp*nb(2)))
!!!!!$  centre_old_box(3)=mindist_new(per(3),at%astruct%cell_dim(3),frag_trans%rot_center(3),hgrids_old(3)*(ns_old(3)-0.5_dp*nb(3)))
!!!!!$  centre_new_box(1)=mindist_new(per(1),at%astruct%cell_dim(1),frag_trans%rot_center_new(1),hgrids(1)*(ns(1)-0.5_dp*nb(1)))
!!!!!$  centre_new_box(2)=mindist_new(per(2),at%astruct%cell_dim(2),frag_trans%rot_center_new(2),hgrids(2)*(ns(2)-0.5_dp*nb(2)))
!!!!!$  centre_new_box(3)=mindist_new(per(3),at%astruct%cell_dim(3),frag_trans%rot_center_new(3),hgrids(3)*(ns(3)-0.5_dp*nb(3)))
!!!  !centre_old_box=closest_r(mesh,frag_trans%rot_center,center=hgrids_old*(ns_old-0.5_dp*nb))
!!!  !centre_new_box=closest_r(mesh,frag_trans%rot_center_new,center=hgrids*(ns-0.5_dp*nb))
!!!
!!!  ! centre_new_box(1)=mindist(per(1),at%astruct%cell_dim(1),frag_trans%rot_center_new(1),hgrids(1)*(ns(1)-0.5_dp*nb(1)))
!!!  ! centre_new_box(2)=mindist(per(2),at%astruct%cell_dim(2),frag_trans%rot_center_new(2),hgrids(2)*(ns(2)-0.5_dp*nb(2)))
!!!  ! centre_new_box(3)=mindist(per(3),at%astruct%cell_dim(3),frag_trans%rot_center_new(3),hgrids(3)*(ns(3)-0.5_dp*nb(3)))
!!!
!!!  !print*,'rotated nb',trim(yaml_toa(rotate_vector(frag_trans%rot_axis,frag_trans%theta,hgrids*-0.5_dp*nb),fmt='(f12.8)'))
!!!  !print*,'rotated centre old',trim(yaml_toa(rotate_vector(frag_trans%rot_axis,frag_trans%theta,centre_old_box),fmt='(f12.8)'))
!!!  !print*,'rotated centre new',trim(yaml_toa(rotate_vector(frag_trans%rot_axis,-frag_trans%theta,centre_new_box),fmt='(f12.8)'))
!!!  !Calculate the shift of the atom to be used in reformat
!!!  !da(1)=mindist(per(1),at%astruct%cell_dim(1),centre_new_box(1),centre_old_box(1))
!!!  !da(2)=mindist(per(2),at%astruct%cell_dim(2),centre_new_box(2),centre_old_box(2))
!!!  !da(3)=mindist(per(3),at%astruct%cell_dim(3),centre_new_box(3),centre_old_box(3))
!!!!!$  da=centre_new_box-centre_old_box-(hgrids_old-hgrids)*0.5d0
!!!
!!!  !print*,'reformat check',frag_trans%rot_center(2),ns_old(2),centre_old_box(2),&
!!!  !     frag_trans%rot_center_new(2),ns(2),centre_new_box(2),da(2)
!!!  !write(*,'(a,15I4)')'nb,ns_old,ns,n_old,n',nb,ns_old,ns,n_old,n
!!!  !write(*,'(a,3(3(f12.8,x),3x))') 'final centre box',centre_old_box,centre_new_box,da
!!!  !write(*,'(a,3(3(f12.8,x),3x))') 'final centre',frag_trans%rot_center,frag_trans%rot_center_new
!!!
!!!  displ=square_gd(mesh,da)!sqrt(da(1)**2+da(2)**2+da(3)**2)
!!!
!!!  !reformatting criterion
!!!  if (hgrids(1) == hgrids_old(1) .and. hgrids(2) == hgrids_old(2) .and. hgrids(3) == hgrids_old(3) &
!!!        .and. nvctr_c  == nvctr_c_old .and. nvctr_f  == nvctr_f_old &
!!!        .and. n_old(1)==n(1)  .and. n_old(2)==n(2) .and. n_old(3)==n(3) &
!!!        .and. abs(frag_trans%theta) <= tol .and. abs(displ) <= tol) then
!!!      reformat_reason(0) = reformat_reason(0) + 1
!!!      reformat_needed=.false.
!!!  else
!!!      reformat_needed=.true.
!!!      if (hgrids(1) /= hgrids_old(1) .or. hgrids(2) /= hgrids_old(2) .or. hgrids(3) /= hgrids_old(3)) then
!!!         reformat_reason(1) = reformat_reason(1) + 1
!!!      end if
!!!      if (nvctr_c  /= nvctr_c_old) then
!!!         reformat_reason(2) = reformat_reason(2) + 1
!!!      end if
!!!      if (nvctr_f  /= nvctr_f_old) then
!!!         reformat_reason(3) = reformat_reason(3) + 1
!!!      end if
!!!      if (n_old(1) /= n(1)  .or. n_old(2) /= n(2) .or. n_old(3) /= n(3) )  then
!!!         reformat_reason(4) = reformat_reason(4) + 1
!!!      end if
!!!      if (abs(displ) > tol)  then
!!!         reformat_reason(5) = reformat_reason(5) + 1
!!!      end if
!!!      if (abs(frag_trans%theta) > tol)  then
!!!         reformat_reason(6) = reformat_reason(6) + 1
!!!      end if
!!!  end if
!!!
!!!  ! check to make sure we don't need to 'unwrap' (or wrap) tmb in periodic case
!!!  wrap_around=.false.
!!!  !if (.not. reformat_needed) then
!!!     do i=1,3
!!!        if (tmb_wrap(per(i),ns(i),n(i),nglr(i)) .or. tmb_wrap(per(i),ns_old(i),n_old(i),nglr_old(i))) then
!!!           if (ns(i) /= ns_old(i)) then
!!!              wrap_around = .true.
!!!              exit
!!!           end if
!!!        end if
!!!     end do
!!!
!!!     if (wrap_around) then
!!!         reformat_reason(7) = reformat_reason(7) + 1
!!!     end if
!!!  !end if
!!!
!!!  !write(*,'(a,1(1x,I4),3(2x,F12.6),6(2x,I4),9(2x,F12.6))')'DEBUG:iproc,rc,ns,nb,alat,h,cob',&
!!!  !     bigdft_mpi%iproc, frag_trans%rot_center, ns_old, nb, at%astruct%cell_dim, hgrids_old, centre_old_box, wrap_around
!!!
!!!contains
!!!
!!!  !> Checks to see if a tmb wraps around the the unit cell
!!!  !! knowing that there could have been a modulo operation
!!!  function tmb_wrap(periodic,ns,n,nglr)
!!!    use module_base
!!!    implicit none
!!!    logical, intent(in) :: periodic
!!!    integer, intent(in) :: ns, n, nglr
!!!    logical :: tmb_wrap
!!!
!!!    tmb_wrap = .false.
!!!    if (periodic) then
!!!       !<=?
!!!       if (ns<0 .or. ns+n>nglr) then
!!!          tmb_wrap = .true.
!!!       end if
!!!    end if
!!!
!!!  end function tmb_wrap
!!!
!!!
!!!  !> Calculates the minimum difference between two coordinates
!!!  !! knowing that there could have been a modulo operation
!!!  function mindist_new(periodic,alat,r,r_old)
!!!    use module_base
!!!    implicit none
!!!    logical, intent(in) :: periodic
!!!    real(gp), intent(in) :: r,r_old,alat
!!!    real(gp) :: mindist_new
!!!
!!!    mindist_new = r - r_old
!!!    if (periodic) then
!!!       if (mindist_new > 0.5d0*alat) then
!!!          mindist_new = mindist_new - alat
!!!       else if (mindist_new < -0.5d0*alat) then
!!!          mindist_new = mindist_new + alat
!!!       end if
!!!    end if
!!!
!!!  end function mindist_new
!!!
!!!
!!!end subroutine reformat_check
!!!
