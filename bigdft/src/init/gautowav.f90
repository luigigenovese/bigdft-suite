!> @file
!!  Routines to check the accuracy of the gaussian expansion
!! @author
!!    Copyright (C) 2007-2015 BigDFT group (LG)
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS


!> Control the accuracy of the expansion in gaussian
subroutine check_gaussian_expansion(iproc,nproc,orbs,Lzd,psi,G,coeffs)
  use module_precisions
  use module_bigdft_mpi
  use module_types
  use orbitalbasis
  use yaml_output
  use module_bigdft_arrays
  use locregs
  implicit none
  !Arguments
  integer, intent(in) :: iproc,nproc
  type(orbitals_data), intent(in) :: orbs
  type(local_zone_descriptors), intent(in) :: Lzd
  type(gaussian_basis), intent(in) :: G
  real(wp), dimension(array_dim(Lzd%Glr),orbs%norbp), intent(in) :: psi
  real(wp), dimension(G%ncoeff,orbs%norbp), intent(in) :: coeffs
  !local variables
  character(len=*), parameter :: subname='check_gaussian_expansion'
  integer :: iorb,i,j
!! integer :: i_stat,i_all,
  real(wp) :: maxdiffp,maxdiff,orbdiff
  real(wp), dimension(:), allocatable :: workpsi
  type(orbital_basis) :: ob

  workpsi = f_malloc(array_dim(Lzd%Glr)*orbs%norbp,id='workpsi')

  call orbital_basis_associate(ob, orbs = orbs, Lzd = Lzd, phis_wvl = workpsi)
  call gaussians_to_wavelets(ob, G, coeffs)
  call orbital_basis_release(ob)

  maxdiffp=0.0_wp
  do iorb=1,orbs%norbp
     orbdiff=0.0_wp
     !if (iorb+iproc*norbp <= norb) then
        do i=1,array_dim(Lzd%Glr)
           j=i+(iorb-1)*array_dim(Lzd%Glr)
           orbdiff=max(orbdiff,(psi(i,iorb)-workpsi(j))**2)
        end do
     !end if
     maxdiffp=max(maxdiffp,orbdiff)
     !print *,'iproc,iorb,orbdiff',iorb,orbdiff
  end do

  !! @todo MPI_REDUCE call not safe for serial version
  if (nproc > 1) then
     maxdiff = fmpi_reduce(maxdiffp,op=FMPI_MAX,comm=bigdft_mpi%mpi_comm)
     !call MPI_REDUCE(maxdiffp,maxdiff,1,mpidtypw,MPI_MAX,0,bigdft_mpi%mpi_comm,ierr)
  else
     maxdiff=maxdiffp
  end if

  if (iproc == 0) call yaml_map('Mean L2 norm of gaussian-wavelet difference', &
     & trim(yaml_toa(sqrt(maxdiff/real(orbs%norb,wp)),fmt='(1pe12.4)')))
  !   write(*,'(1x,a,1pe12.4)')'Mean L2 norm of gaussian-wavelet difference:',&
  !        sqrt(maxdiff/real(orbs%norb,wp))
  call f_free(workpsi)

END SUBROUTINE check_gaussian_expansion


!> Parse the output of CP2K to read the basis set information
subroutine parse_cp2k_files(iproc,basisfile,orbitalfile,nat,ntypes,orbs,iatype,rxyz,&
     CP2K,wfn_cp2k)
  use module_precisions
  use module_types, only: orbitals_data, gaussian_basis
  use gaussians
  use module_bigdft_output
  use module_bigdft_arrays
  implicit none
  character(len=*), intent(in) :: basisfile,orbitalfile
  integer, intent(in) :: iproc,nat,ntypes
  type(orbitals_data), intent(in) :: orbs
  integer, dimension(nat), intent(in) :: iatype
  real(gp), dimension(3,nat), target, intent(in) :: rxyz
  type(gaussian_basis), intent(out) :: CP2K
  real(wp), dimension(:,:), pointer :: wfn_cp2k
  !local variables
  character(len=*), parameter :: subname='parse_cp2k_files'
  character(len=6) :: string,symbol
  character(len=100) :: line
  !n(c) integer, parameter :: nterm_max=3
  integer :: ngx,nbx,nst,nend,num,mmax,myshift,i,ipar,ipg,jat
  integer :: iorb,jorb,iat,ityp,i_stat,ibas,ig,iset,jbas,ishell,lmax
!! integer :: i_all,
  integer :: isat,iexpo,icoeff,iam
  real(dp) :: tt
  real(gp) :: exponent,coefficient
  integer, dimension(:), allocatable :: nshell,iorbtmp
  integer, dimension(:,:), allocatable :: nam,ndoc
  real(gp), dimension(:), allocatable :: ctmp
  real(gp), dimension(:,:,:), allocatable :: contcoeff,expo
  real(wp), dimension(:,:,:,:), allocatable :: cimu

  if (iproc==0) call yaml_comment('Reading Basis Set information and wavefunctions coefficients')
  !if (iproc==0) write(*,'(1x,a)',advance='no') 'Reading Basis Set information and wavefunctions coefficients...'

  ngx=0
  nbx=0
  lmax=0

  nshell = f_malloc(ntypes,id='nshell')

  open(unit=35,file=trim(basisfile),action='read')

  !read the lines for analyzing the maximum number of primitive gaussian functions
  !and also for the maximum angular momentum
  ityp=0
  ngx=0
  nbx=0
  lmax=0
  ipg=0
  for_ngx: do
     if (ityp > ntypes) exit for_ngx
     read(35,'(a100)')line
     !analyzing the different possibilities
     read(line,*,iostat=i_stat)tt,string,symbol
     if (i_stat == 0 .and. string=='Atomic' .and. symbol=='kind:') then
        ityp=ityp+1
        if (ityp > 1) then
           nshell(ityp-1)=ishell
           nbx=max(nbx,ishell)
        end if
        ishell=0
        cycle for_ngx
     end if
     read(line,*,iostat=i_stat)iset,num,num,num,exponent,coefficient
     if (i_stat==0) then
        !print *,num,exponent,coefficient
        ishell=ishell+1
        lmax=max(lmax,num)
        ngx=max(ngx,ipg)
        !print *,ishell,ipg,lmax
        ipg=1
        cycle for_ngx
     end if
     read(line,*,iostat=i_stat)exponent,coefficient
     if (i_stat==0) then
        ipg=ipg+1
        cycle for_ngx
     end if
  end do for_ngx

  !now store the values
  rewind(35)

  !here allocate arrays
  nam = f_malloc((/ nbx, ntypes /),id='nam')
  ndoc = f_malloc((/ nbx, ntypes /),id='ndoc')
  contcoeff = f_malloc((/ ngx, nbx, ntypes /),id='contcoeff')
  expo = f_malloc((/ ngx, nbx, ntypes /),id='expo')

  ityp=0
  ipg=0
  store_basis: do
     if (ityp > ntypes) exit store_basis
     read(35,'(a100)')line
     !analyzing the different possibilities
     read(line,*,iostat=i_stat)tt,string,symbol
     if (i_stat == 0 .and. string=='Atomic' .and. symbol=='kind:') then
        ityp=ityp+1
        if (ityp > 1) then
           ndoc(ishell,ityp-1)=ipg
        end if
        ishell=0
        cycle store_basis
     end if
     read(line,*,iostat=i_stat)iset,num,num,num,exponent,coefficient
     if (i_stat==0) then
        !print *,num,exponent,coefficient
        ishell=ishell+1
        nam(ishell,ityp)=num
        lmax=max(lmax,num)
        if (ishell > 1) ndoc(ishell-1,ityp)=ipg
        expo(1,ishell,ityp)=exponent
        contcoeff(1,ishell,ityp)=coefficient
        ipg=1
        cycle store_basis
     end if
     read(line,*,iostat=i_stat)exponent,coefficient
     if (i_stat==0) then
        ipg=ipg+1
        expo(ipg,ishell,ityp)=exponent
        contcoeff(ipg,ishell,ityp)=coefficient
        cycle store_basis
     end if
  end do store_basis

  !close the file of the basis definition
  close(35)

  !renormalize the coefficients in each shell
  do ityp=1,ntypes
     do ishell=1,nshell(ityp)
        call normalize_shell(ndoc(ishell,ityp),nam(ishell,ityp),&
             expo(1,ishell,ityp),contcoeff(1,ishell,ityp))
     end do
  end do


  !the number of gaussian centers are thus nat
  CP2K%nat=nat
  CP2K%rxyz => rxyz
  !copy the parsed values in the gaussian structure
  !count also the total number of shells
  CP2K%nshell = f_malloc_ptr(nat,id='CP2K%nshell')

  CP2K%nshltot=0
  do iat=1,nat
     ityp=iatype(iat)
     CP2K%nshell(iat)=nshell(ityp)
     CP2K%nshltot=CP2K%nshltot+nshell(ityp)
  end do

  CP2K%ndoc = f_malloc_ptr(CP2K%nshltot,id='CP2K%ndoc')
  CP2K%nam = f_malloc_ptr(CP2K%nshltot,id='CP2K%nam')

  !assign shell IDs and count the number of exponents and coefficients
  CP2K%ncplx=1
  CP2K%nexpo=0
  CP2K%ncoeff=0
  ishell=0
  do iat=1,nat
     ityp=iatype(iat)
     do isat=1,CP2K%nshell(iat)
        ishell=ishell+1
        CP2K%ndoc(ishell)=ndoc(isat,ityp)
        CP2K%nam(ishell)=nam(isat,ityp)+1
        CP2K%nexpo=CP2K%nexpo+ndoc(isat,ityp)
        CP2K%ncoeff=CP2K%ncoeff+2*nam(isat,ityp)+1
     end do
  end do

  !allocate and assign the exponents and the coefficients
  CP2K%xp = f_malloc_ptr((/ CP2K%ncplx, CP2K%nexpo /),id='CP2K%xp')
  CP2K%psiat = f_malloc_ptr((/ CP2K%ncplx, CP2K%nexpo /),id='CP2K%psiat')

  ishell=0
  iexpo=0
  do iat=1,nat
     ityp=iatype(iat)
     do isat=1,CP2K%nshell(iat)
        ishell=ishell+1
        do ig=1,CP2K%ndoc(ishell)
           iexpo=iexpo+1
           CP2K%psiat(1,iexpo)=contcoeff(ig,isat,ityp)
           CP2K%xp(1,iexpo)=sqrt(0.5_gp/expo(ig,isat,ityp))
        end do
     end do
  end do


!!!  !print the found values
!!!  do ityp=1,ntypes
!!!     do ishell=1,nshell(ityp)
!!!        print *,'ityp=',ityp,'ishell=',ishell,'l=',nam(ishell,ityp),'ndoc=',ndoc(ishell,ityp)
!!!        do ipg=1,ndoc(ishell,ityp)
!!!           print *,'expo=',expo(ipg,ishell,ityp),'coeff=',contcoeff(ipg,ishell,ityp)
!!!        end do
!!!     end do
!!!  end do


  call f_free(contcoeff)
  call f_free(expo)


  mmax=2*lmax+1
  !now read the coefficients of the gaussian converged orbitals
  open(unit=36,file=trim(orbitalfile),action='read')
  !here there is the orbital label, for the moment it is assumed to vary between 1 and 4
  ctmp = f_malloc(10,id='ctmp')
  iorbtmp = f_malloc(10,id='iorbtmp')
  cimu = f_malloc((/ mmax, nbx, nat, orbs%norb /),id='cimu')

  read(36,*)
  read_line1: do
     read(36,'(a100)')line
     !analyse how many orbitals are contained in a given line
     read_orbitals1: do ipar=10,1,-1
        read(line,*,iostat=i_stat)(iorbtmp(i),i=1,ipar)
        if (i_stat==0) then
           read(line,*)nst
           exit read_line1
        end if
     end do read_orbitals1
  end do read_line1
  nend=nst+ipar-1

!!!  nst=1
!!!  nend=4
  jat=0
  ishell=1
  jbas=0
  iat=nat
  !now read the data to assign the coefficients
  store_coeff: do
     read(36,'(a100)')line
     !choose between different cases
     read(line,*,iostat=i_stat)ibas,iat,symbol,string,(ctmp(iorb),iorb=1,nend-nst+1)
     if (i_stat==0) then
        !print *,line,nst,nend
        if (jat==iat) then
           jbas=jbas+1
           if (jbas > 2*nam(ishell,iatype(iat))+1) then
              jbas=1
              ishell=ishell+1
              if (ishell > nshell(iatype(iat))) then
                 !if (iproc==0)
                 call yaml_warning('Problem in the gaucoeff.dat file, the number of shells of atom' // &
                      & trim(yaml_toa(iat)) // ' is incoherent')
                 !write(*,'(1x,a,i0,a)') 'Problem in the gaucoeff.dat file, the number of shells of atom ',iat ,&
                 !  ' is incoherent'
                 stop
              end if
           end if
        else
           jbas=1
           ishell=1
        end if
       symbol=trim(string)
       do iorb=nst,nend
          cimu(jbas+myshift(symbol),ishell,iat,iorb)=ctmp(iorb-nst+1)
       end do
       jat=iat
       if (jbas==2*nam(ishell,iatype(iat))+1 .and. ishell==nshell(iatype(iat))&
            .and. iat==nat .and. nend==orbs%norb) then
          exit store_coeff
       else
          cycle store_coeff
       end if
     end if

     read_orbitals: do ipar=10,1,-1
        read(line,*,iostat=i_stat)(iorbtmp(i),i=1,ipar)
        if (i_stat==0) then
           read(line,*)nst
           nend=nst+ipar-1
           if (jat/=nat) then
              !if (iproc==0)
              call yaml_warning('Problem in the gaucoeff.dat file, only ' // trim(yaml_toa(iat)) // &
                   & ' atoms processed')
              !write(*,'(1x,a,i0,a)') 'Problem in the gaucoeff.dat file, only ',iat ,' atoms processed'
              stop
           else
              cycle store_coeff
           end if
        end if
     end do read_orbitals

  end do store_coeff
  close(36)

!!!  !print the found values
!!!  do iat=1,nat
!!!     ityp=iatype(iat)
!!!     do ishell=1,nshell(ityp)
!!!        do jbas=1,2*nam(ishell,ityp)+1
!!!           print *,iat,ishell,nam(ishell,ityp),jbas,(cimu(jbas,ishell,iat,iorb),iorb=1,norb)
!!!        end do
!!!     end do
!!!  end do

  !allocate and assign the coefficients of each orbital
  wfn_cp2k = f_malloc_ptr((/ CP2K%ncoeff, orbs%norbp /),id='wfn_cp2k')
  do iorb=1,orbs%norbp
     jorb=iorb+orbs%isorb
     icoeff=0
     ishell=0
     do iat=1,CP2K%nat
        do isat=1,CP2K%nshell(iat)
           ishell=ishell+1
           do iam=1,2*CP2K%nam(ishell)-1
              icoeff=icoeff+1
              if (jorb <= orbs%norb) wfn_cp2k(icoeff,iorb)=cimu(iam,isat,iat,jorb)
           end do
        end do
     end do
     call gaudim_check(1,icoeff+1,ishell,0,CP2K%ncoeff,CP2K%nshltot)
  end do

  call f_free(ctmp)
  call f_free(iorbtmp)
  call f_free(nshell)
  call f_free(nam)
  call f_free(ndoc)
  call f_free(cimu)

  !if (iproc==0) write(*,'(1x,a)')'done.'

END SUBROUTINE parse_cp2k_files

subroutine gaussians_to_wavelets_orb(daub,kpt,G,wfn_gau)
  use module_precisions
  use gaussians
  use locregs
  use liborbs_functions
  use f_arrays
  use module_bigdft_arrays
  implicit none
  type(wvf_daub_view), intent(inout) :: daub
  real(gp), dimension(3), intent(in) :: kpt
  type(gaussian_basis), intent(in) :: G
  real(wp), dimension(G%ncoeff,daub%manager%nspinor), intent(in) :: wfn_gau
  !local variables
  character(len=*), parameter :: subname='gaussians_to_wavelets_orb'
  integer, parameter :: nterm_max=3
  integer :: ishell,iexpo,icoeff,iat,isat,ng,l,m,i,nterm,ig
  type(f_scalar) :: gau_a, gau_f, c
  type(f_scalar), dimension(2) :: factor
  real(gp), dimension(3) :: rxyz
  integer, dimension(nterm_max) :: lx,ly,lz
  real(gp), dimension(nterm_max) :: fac_arr

  !initialize the wavefunction
  call f_zero(daub%c%mem)

  !calculate the number of terms for this orbital
  !loop over the atoms
  ishell=0
  iexpo=1
  icoeff=1
  !n(c) iscoeff=1
  do iat=1,G%nat
     rxyz = G%rxyz(:,iat)
     !loop over the number of shells of the atom type
     do isat=1,G%nshell(iat)
        ishell=ishell+1
        !the degree of contraction of the basis function
        !is the same as the ng value of the createAtomicOrbitals routine
        ng=G%ndoc(ishell)
        !angular momentum of the basis set(shifted for compatibility with BigDFT routines
        l=G%nam(ishell)
        !print *,iproc,iat,ishell,G%nam(ishell),G%nshell(iat)
        !multiply the values of the gaussian contraction times the orbital coefficient

        do m=1,2*l-1
           call calc_coeff_inguess(l,m,nterm_max,nterm,lx,ly,lz,fac_arr)
           !control whether the basis element may be
           !contribute to some of the orbital of the processor
           if (any(wfn_gau(icoeff, :) /= 0.0_wp)) then
              !assign the arrays
              !make sure that the coefficients returned by
              !gauss_to_daub are zero outside [ml:mr]
              do ig=1,ng
                 if (G%ncplx == 1) then
                    gau_a=G%xp(1,iexpo+ig-1)
                    gau_f=G%psiat(1,iexpo+ig-1)
                 else
                    gau_a=f_scalar(G%xp(1,iexpo+ig-1), G%xp(2,iexpo+ig-1))
                    gau_f=f_scalar(G%psiat(1,iexpo+ig-1), G%psiat(2,iexpo+ig-1))
                 end if
                 do i=1,nterm
                    if (daub%manager%nspinor == 1) then
                       factor(1) = wfn_gau(icoeff,1) * fac_arr(i) * gau_f
                    else if (daub%manager%nspinor == 2) then
                       c = f_scalar(wfn_gau(icoeff,1), wfn_gau(icoeff,2))
                       factor(1) = c * fac_arr(i) * gau_f
                    else if (daub%manager%nspinor == 4) then
                       c = f_scalar(wfn_gau(icoeff,1), wfn_gau(icoeff,2))
                       factor(1) = c * fac_arr(i) * gau_f
                       c = f_scalar(wfn_gau(icoeff,3), wfn_gau(icoeff,4))
                       factor(2) = c * fac_arr(i) * gau_f
                    end if
                    call wvf_accumulate_gaussian(daub, gau_a, rxyz, &
                         factor, (/ lx(i), ly(i), lz(i) /), kpt)
                 end do
              end do
           end if
           icoeff=icoeff+1
        end do
        iexpo=iexpo+ng
     end do
  end do

  call gaudim_check(iexpo,icoeff,ishell,G%nexpo,G%ncoeff,G%nshltot)

  !accumulate wavefunction
  call wvf_finish_accumulate_gaussians(daub)

END SUBROUTINE gaussians_to_wavelets_orb


!temporary creation, better to put in standby
!!!subroutine sumrho_gaussians(geocode,iproc,nproc,norb,norbp,nspin,nspinor,&
!!!     n1i,n2i,n3i,n3d,i3s,hxh,hyh,hzh,G,gaucoeff,occup,spinsgn,rho)
!!!  use module_base
!!!  use module_types
!!!  implicit none
!!!  character(len=1), intent(in) :: geocode !< @copydoc poisson_solver::doc::geocode
!!!  integer, intent(in) :: iproc,nproc,norb,norbp,n1i,n2i,n3i
!!!  real(gp), intent(in) :: hx,hy,hz
!!!  type(wavefunctions_descriptors), intent(in) :: wfd
!!!  type(gaussian_basis), intent(in) :: G
!!!  real(wp), dimension(G%ncoeff,norbp), intent(in) :: wfn_gau
!!!  real(wp), dimension(wfd%nvctr_c+7*wfd%nvctr_f,norbp), intent(out) :: psi
!!!
!!!  !local variables
!!!  character(len=*), parameter :: subname='gaussians_to_wavelets'
!!!  integer, parameter :: nterm_max=3
!!!  logical :: myorbital
!!!  integer :: i_stat,i_all,ishell,iexpo,icoeff,iat,isat,ng,l,m,iorb,jorb,i,nterm,ierr,ig
!!!  real(dp) :: normdev,tt,scpr
!!!  real(gp) :: rx,ry,rz
!!!  integer, dimension(nterm_max) :: lx,ly,lz
!!!  real(gp), dimension(nterm_max) :: fac_arr
!!!  real(wp), dimension(:), allocatable :: tpsi
!!!
!!!  if(iproc == 0) write(*,'(1x,a)',advance='no')'Writing wavefunctions in wavelet form '
!!!
!!!  allocate(tpsi(wfd%nvctr_c+7*wfd%nvctr_f+ndebug),stat=i_stat)
!!!  call memocc(i_stat,tpsi,'tpsi',subname)
!!!
!!!  !initialize the wavefunction
!!!  call to_zero((wfd%nvctr_c+7*wfd%nvctr_f)*norbp,psi)
!!!  !this can be changed to be passed only once to all the gaussian basis
!!!  !eks=0.d0
!!!  !loop over the atoms
!!!  ishell=0
!!!  iexpo=1
!!!  icoeff=1
!!!  do iat=1,G%nat
!!!     rx=G%rxyz(1,iat)
!!!     ry=G%rxyz(2,iat)
!!!     rz=G%rxyz(3,iat)
!!!     !loop over the number of shells of the atom type
!!!     do isat=1,G%nshell(iat)
!!!        ishell=ishell+1
!!!        !the degree of contraction of the basis function
!!!        !is the same as the ng value of the createAtomicOrbitals routine
!!!        ng=G%ndoc(ishell)
!!!        !angular momentum of the basis set(shifted for compatibility with BigDFT routines
!!!        l=G%nam(ishell)
!!!        !multiply the values of the gaussian contraction times the orbital coefficient
!!!        do m=1,2*l-1
!!!           call calc_coeff_inguess(l,m,nterm_max,nterm,lx,ly,lz,fac_arr)
           !this kinetic energy is not reliable
!!eks=eks+ek*occup(iorb)*cimu(m,ishell,iat,iorb)
!!!           call crtonewave(geocode,n1,n2,n3,ng,nterm,lx,ly,lz,fac_arr,G%xp(1,iexpo),G%psiat(1,iexpo),&
!!!                rx,ry,rz,hx,hy,hz,0,n1,0,n2,0,n3,nfl1,nfu1,nfl2,nfu2,nfl3,nfu3,  &
!!!                wfd,tpsi(1),tpsi(wfd%nvctr_c+1))
!!!           !sum the result inside the orbital wavefunction
!!!           !loop over the orbitals
!!!           do iorb=1,norb
!!!              if (myorbital(iorb,norb,iproc,nproc)) then
!!!                 jorb=iorb-iproc*norbp
!!!                 do i=1,wfd%nvctr_c+7*wfd%nvctr_f
!!!                    !for this also daxpy BLAS can be used
!!!                    psi(i,jorb)=psi(i,jorb)+wfn_gau(icoeff,jorb)*tpsi(i)
!!!                 end do
!!!              end if
!!!           end do
!!!           icoeff=icoeff+1
!!!        end do
!!!        iexpo=iexpo+ng
!!!     end do
!!!     if (iproc == 0) then
!!!        write(*,'(a)',advance='no') &
!!!             repeat('.',(iat*40)/G%nat-((iat-1)*40)/G%nat)
!!!     end if
!!!  end do
!!!
!!!  call gaudim_check(iexpo,icoeff,ishell,G%nexpo,G%ncoeff,G%nshltot)
!!!
!!!  if (iproc ==0 ) write(*,'(1x,a)')'done.'
!!!  !renormalize the orbitals
!!!  !calculate the deviation from 1 of the orbital norm
!!!  normdev=0.0_dp
!!!  tt=0.0_dp
!!!  do iorb=1,norb
!!!     if (myorbital(iorb,norb,iproc,nproc)) then
!!!        jorb=iorb-iproc*norbp
!!!        call wnrm(wfd%nvctr_c,wfd%nvctr_f,psi(1,jorb),psi(wfd%nvctr_c+1,jorb),scpr)
!!!        call wscal(wfd%nvctr_c,wfd%nvctr_f,real(1.0_dp/sqrt(scpr),wp),psi(1,jorb),psi(wfd%nvctr_c+1,jorb))
!!!        !print *,'norm of orbital ',iorb,scpr
!!!        tt=max(tt,abs(1.0_dp-scpr))
!!!     end if
!!!  end do
!!!  if (nproc > 1) then
!!!     call MPI_REDUCE(tt,normdev,1,mpidtypd,MPI_MAX,0,bigdft_mpi%mpi_comm,ierr)
!!!  else
!!!     normdev=tt
!!!  end if
!!!  if (iproc ==0 ) write(*,'(1x,a,1pe12.2)')&
!!!       'Deviation from normalization of the imported orbitals',normdev
!!!
!!!  i_all=-product(shape(tpsi))*kind(tpsi)
!!!  deallocate(tpsi,stat=i_stat)
!!!  call memocc(i_stat,i_all,'tpsi',subname)
!!!
!!!
!!!  !Creates charge density arising from the ionic PSP cores
!!!  if (n3pi >0 ) then
!!!
!!!     !conditions for periodicity in the three directions
!!!     perx=(geocode /= 'F')
!!!     pery=(geocode == 'P')
!!!     perz=(geocode /= 'F')
!!!
!!!     call ext_buffers(perx,nbl1,nbr1)
!!!     call ext_buffers(pery,nbl2,nbr2)
!!!     call ext_buffers(perz,nbl3,nbr3)
!!!
!!!     call to_zero(n1i*n2i*n3pi,pot_ion)
!!!
!!!     do iat=1,nat
!!!        ityp=iatype(iat)
!!!        rx=rxyz(1,iat)
!!!        ry=rxyz(2,iat)
!!!        rz=rxyz(3,iat)
!!!
!!!        rloc=psppar(0,0,ityp)
!!!        charge=real(nelpsp(ityp),kind=8)/(2.d0*pi*sqrt(2.d0*pi)*rloc**3)
!!!        cutoff=10.d0*rloc
!!!
!!!        isx=floor((rx-cutoff)/hxh)
!!!        isy=floor((ry-cutoff)/hyh)
!!!        isz=floor((rz-cutoff)/hzh)
!!!
!!!        iex=ceiling((rx+cutoff)/hxh)
!!!        iey=ceiling((ry+cutoff)/hyh)
!!!        iez=ceiling((rz+cutoff)/hzh)
!!!
!!!        !these nested loops will be used also for the actual ionic forces, to be recalculated
!!!        do i3=isz,iez
!!!           z=real(i3,kind=8)*hzh-rz
!!!           call ind_positions(perz,i3,n3,j3,goz)
!!!           j3=j3+nbl3+1
!!!           do i2=isy,iey
!!!              y=real(i2,kind=8)*hyh-ry
!!!              call ind_positions(pery,i2,n2,j2,goy)
!!!              do i1=isx,iex
!!!                 x=real(i1,kind=8)*hxh-rx
!!!                 call ind_positions(perx,i1,n1,j1,gox)
!!!                 r2=x**2+y**2+z**2
!!!                 arg=r2/rloc**2
!!!                 xp=exp(-.5d0*arg)
!!!                 if (j3 >= i3s .and. j3 <= i3s+n3pi-1  .and. goy  .and. gox ) then
!!!                    ind=j1+1+nbl1+(j2+nbl2)*n1i+(j3-i3s+1-1)*n1i*n2i
!!!                    pot_ion(ind)=pot_ion(ind)-xp*charge
!!!                 else if (.not. goz ) then
!!!                    rholeaked=rholeaked+xp*charge
!!!                 endif
!!!              enddo
!!!           enddo
!!!        enddo
!!!
!!!     enddo
!!!
!!!  end if
!!!
!!!  ! Check
!!!  tt=0.d0
!!!  do j3=1,n3pi
!!!     do i2= -nbl2,2*n2+1+nbr2
!!!        do i1= -nbl1,2*n1+1+nbr1
!!!           ind=i1+1+nbl1+(i2+nbl2)*n1i+(j3-1)*n1i*n2i
!!!           tt=tt+pot_ion(ind)
!!!        enddo
!!!     enddo
!!!  enddo
!!!
!!!  tt=tt*hxh*hyh*hzh
!!!  rholeaked=rholeaked*hxh*hyh*hzh
!!!
!!!  !print *,'test case input_rho_ion',iproc,i3start,i3end,n3pi,2*n3+16,tt
!!!
!!!  if (nproc > 1) then
!!!     charges_mpi(1)=tt
!!!     charges_mpi(2)=rholeaked
!!!     call MPI_ALLREDUCE(charges_mpi(1),charges_mpi(3),2,MPI_double_precision,  &
!!!          MPI_SUM,bigdft_mpi%mpi_comm,ierr)
!!!     tt_tot=charges_mpi(3)
!!!     rholeaked_tot=charges_mpi(4)
!!!  else
!!!     tt_tot=tt
!!!     rholeaked_tot=rholeaked
!!!  end if
!!!
!!!  if (iproc.eq.0) write(*,'(1x,a,f26.12,2x,1pe10.3)') &
!!!       'total ionic charge, leaked charge ',tt_tot,rholeaked_tot
!!!
!!!
!!!END SUBROUTINE sumrho_gaussians

!> Calculate the shift between the spherical harmonics of CP2K and the one of BigDFT
function myshift(symbol)
  implicit none
  character(len=5), intent(in) :: symbol
  integer :: myshift
  myshift=0
  if (symbol(2:2)=='s') then
     myshift=0
  else if (symbol(2:2)=='p') then
     if( symbol(3:3)=='y') then
        myshift=1
     else if( symbol(3:3)=='z') then
        myshift=1
     else if( symbol(3:3)=='x') then
        myshift=-2
     end if
  else if ( symbol(2:2)=='d') then
     if( symbol(3:4)=='-2') then
        myshift=2
     else if( symbol(3:4)=='-1') then
        myshift=-1
     else if( symbol(3:3)=='0') then
        myshift=2
     else if( symbol(3:4)=='+1') then
        myshift=-2
     else if( symbol(3:4)=='+2') then
        myshift=-1
     end if
  else if ( symbol(2:2)=='f') then
     if( symbol(3:4)=='-3') then
        myshift=4
     else if( symbol(3:4)=='-2') then
        myshift=5
     else if( symbol(3:4)=='-1') then
        myshift=-1
     else if( symbol(3:3)=='0') then
        myshift=-1
     else if( symbol(3:4)=='+1') then
        myshift=-4
     else if( symbol(3:4)=='+2') then
        myshift=0
     else if( symbol(3:4)=='+3') then
        myshift=-3
     end if
  else if ( symbol(2:2)=='g') then
     write(*,'(1x,a)')'the orbitals of type g are not yet implemented in BigDFT'
     stop
  end if

end function myshift


logical function myorbital(iorb,norbe,iproc,nproc)
  implicit none
  integer, intent(in) :: iorb,norbe,iproc,nproc
  !local variables
  real(kind=8), parameter :: eps_mach=1.d-12
  integer :: norbep
  real(kind=8) :: tt

  tt=dble(norbe)/dble(nproc)
  norbep=int((1.d0-eps_mach*tt) + tt)
  if (iorb >= iproc*norbep+1 .and. iorb <= min((iproc+1)*norbep,norbe)) then
     myorbital=.true.
  else
     myorbital=.false.
  endif

end function myorbital
