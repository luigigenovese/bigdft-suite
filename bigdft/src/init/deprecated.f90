!> Calculate the important objects related to the physical properties of the system
subroutine system_properties(iproc,nproc,in,atoms,orbs)!,radii_cf)
  use module_types
  use module_interfaces, only: orbitals_descriptors
  use public_enums
  use at_domain, only: domain_geocode
  use module_bigdft_errors
  implicit none
  integer, intent(in) :: iproc,nproc
  type(input_variables), intent(in) :: in
  type(atoms_data), intent(in) :: atoms
  type(orbitals_data), intent(inout) :: orbs
!  real(gp), dimension(atoms%astruct%ntypes,3), intent(out) :: radii_cf
  !local variables
  !n(c) character(len=*), parameter :: subname='system_properties'
  integer :: nspinor

!  radii_cf = atoms%radii_cf
 !!$  call read_radii_variables(atoms, radii_cf, in%crmult, in%frmult, in%projrad)
 !!$  call read_atomic_variables(atoms, trim(in%file_igpop),in%nspin)
  if (iproc == 0) call print_atomic_variables(atoms, max(in%hx,in%hy,in%hz), in%ixc, in%dispersion)
  if(in%nspin==4) then
     nspinor=4
  else
     nspinor=1
  end if
  call orbitals_descriptors(iproc, nproc,in%gen_norb,in%gen_norbu,in%gen_norbd,in%nspin,nspinor,&
       in%gen_nkpt,in%gen_kpt,in%gen_wkpt,orbs,LINEAR_PARTITION_NONE)
  orbs%occup(1:orbs%norb*orbs%nkpts) = in%gen_occup
  if (iproc==0) call print_orbitals(orbs, domain_geocode(atoms%astruct%dom))

  !Check if orbitals and electrons
  if (orbs%norb*orbs%nkpts == 0) &
     & call f_err_throw('No electrons in the system. Check your input variables or atomic positions', &
     & err_id=BIGDFT_INPUT_VARIABLES_ERROR)

END SUBROUTINE system_properties
