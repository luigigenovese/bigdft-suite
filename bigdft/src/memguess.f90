!> @file
!!   Program to guess the used memory by BigDFT
!! @author
!!   Copyright (C) 2007-2015 BigDFT group (LG)
!!   This file is distributed under the terms of the
!!   GNU General Public License, see ~/COPYING file
!!   or http://www.gnu.org/copyleft/gpl.txt .
!!   For the list of contributors, see ~/AUTHORS


!> Test the input files and estimates the memory occupation versus the number
!! of processors
program memguess

   use module_precisions
   use tools
   use module_bigdft_arrays
   use module_bigdft_profiling
   use module_bigdft_errors
   use sparsematrix_base!, only: sparse_matrix, matrices_null, assignment(=), SPARSE_FULL, &
                        !        sparsematrix_malloc_ptr, sparsematrix_malloc0_ptr, DENSE_FULL, &
                        !        sparsematrix_init_errors
                        !        sparsematrix_init_errors
   implicit none
   character(len=*), parameter :: subname='memguess'
   character(len=30) :: tatonam, radical
   character(len=128) :: fileFrom, fileTo,filename_wfn, coeff_file, ham_file, overlap_file, kernel_file, matrix_file
   character(len=128) :: ntmb_, norbks_, nat_, nsubmatrices_, ncategories_, cutoff_, power_
   character(len=128) :: amatrix_file, bmatrix_file, cmatrix_file, inmatrix_file, outmatrix_file, wf_file
   character(len=128) :: posinp_file, pdos_file, sigma_, npts_,filename_proj
   logical :: logfile=.false.
   logical :: disable_deprecation = .false.
   logical :: mem_guess = .true.
   integer :: ntimes,nproc,output_grid, i_arg,istat
   integer :: norbgpu,ng, nsubmatrices, ncategories
   integer :: export_wf_iband, export_wf_ispin, export_wf_ikpt, export_wf_ispinor
   real(gp) :: cutoff,power
   real(gp) :: sigma
   integer :: npts
   integer :: ierror, ntmb, norbks, npdos, nat
   character(len=6) :: direction

   call f_lib_initialize()
   !initialize errors and timings as bigdft routines are called
   call bigdft_init_errors()
   call sparsematrix_init_errors()
   call bigdft_init_timing_categories()
   ! Get arguments
   !call getarg(1,tatonam)
   call get_command_argument(1, value = tatonam, status = istat)

   !write(radical, "(A)") "input"
   call f_zero(radical)
   if(trim(tatonam)=='' .or. istat>0) then
      write(*,'(1x,a)')&
         &   'Usage: ./memguess <nproc> [option]'
      write(*,'(1x,a)')&
         &   'Indicate the number of processes after the executable'
      write(*,'(1x,a)')&
         &   '[option] can be the following: '
      write(*,'(1x,a)')&
         &   '"y": grid to be plotted with V_Sim'
      write(*,'(1x,a)')&
         &   '"o" rotate the molecule such that the volume of the simulation box is optimised'
      write(*,'(1x,a)')&
         &   '"GPUtest <nrep>" case of a CUDAGPU calculation, to test the speed of 3d operators'
      write(*,'(1x,a)')&
         &   '         <nrep> is the number of repeats'
      write(*,'(1x,a)')&
         &   '"upgrade" upgrades input files older than 1.2 into actual format'
      write(*,'(1x,a)')&
         &   '"convert" <from.[cube,etsf]> <to.[cube,etsf]>" converts "from" to file "to" using the given formats'
      write(*,'(1x,a)')&
         &   '"exportwf" <n>[u,d] <from.[bin,formatted,etsf]> "'//&
      ' converts n-th wavefunction of file "from" to cube using BigDFT uncompression'
      write(*,'(1x,a)')&
         &   '"atwf" <ng> calculates the atomic wavefunctions of the first atom in the gatom basis and write their expression '
      write(*,'(1x,a)')&
         &   '            in the "gatom-wfn.dat" file '
      write(*,'(1x,a)')&
         &   '           <ng> is the number of gaussians used for the gatom calculation'
      write(*,'(1x,a)')&
           &   '"convert-positions" <from.[xyz,ascii,yaml]> <to.[xyz,ascii,yaml]>" '
      write(*,'(1x,a)')&
           & 'converts input positions file "from" to file "to" using the given formats'
      write(*,'(1x,a)')&
           &   '"pdos" <ntmb> <norb> <coeffs.bin> <npdos>" '
      write(*,'(1x,a)')&
           & 'reads in the expansion coefficients "coeffs.bin" of dimension (nmtb x norb) &
           &and calculate "npdos" partial density of states'
      write(*,'(1x,a)')&
           &   '"kernel-analysis" <coeffs.bin> <kernel.bin>" '
      write(*,'(1x,a)')&
           & 'calculates a full kernel from the expansion coefficients "coeffs.bin" of dimension (nmtb x norb) &
           &and compare it with the sparse kernel in "kernel.bin"'
      write(*,'(1x,a)')&
           &   '"solve-eigensystem" <ham.bin> <overlap.bin> <coeffs.bin>" '
      write(*,'(1x,a)')&
           & 'solve the eigensystem Hc = lSc and write the coeffs c to disk'
      write(*,'(1x,a)')&
           &   '"analyse-coeffs" <coeff.bin>" '
      write(*,'(1x,a)')&
           & 'analyse the coefficients by assigning them in to ncategories categories'
      write(*,'(1x,a)')&
           &   '"peel-matrix" <matrix.bin>" '
      write(*,'(1x,a)')&
           & 'peel a matrix by stripping off elements which are outside of a cutoff'
      write(*,'(1x,a)')&
           &   '"multiply-matrices" <matrix.bin>" '
      write(*,'(1x,a)')&
           & 'multiply two matrices'
      write(*,'(1x,a)')&
           &   '"matrixpower" <matrix.bin>" '
      write(*,'(1x,a)')&
           & 'calculate the power of a matrix'
      write(*,'(1x,a)')&
           &   '"suggest-cutoff" <posinp.xyz>" '
      write(*,'(1x,a)')&
           & 'suggest cutoff radii for the linear scaling version'
      write(*,'(1x,a)')&
           &   '"charge-analysis"" '
      write(*,'(1x,a)')&
           & 'perform a Loewdin charge analysis'

      stop
   else
      read(unit=tatonam,fmt=*,iostat=ierror) nproc
      if (ierror /= 0) then
         call deprecation_message()
         stop
      end if
      i_arg = 2
      output_grid=0
      loop_getargs: do
         call get_command_argument(i_arg, value = tatonam, status = istat)

         if(istat > 0) exit loop_getargs

         !first restructuring in view of better behaviour
         select case(trim(tatonam))
         case('y')
            output_grid=1
            write(*,'(1x,a)') 'The system grid will be displayed in the "grid.xyz" file'
            exit loop_getargs
         case('o')
            write(*,'(1x,a)')&
                 &   'The optimised system grid will be displayed in the "grid.xyz" file and "posopt.xyz"'
            call minimiseBox(trim(radical))
            exit loop_getargs
         case('GPUtest')
            mem_guess = .false.
            write(*,'(1x,a)')&
               &   'Perform the test with GPU, if present.'
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = tatonam, status = istat)
            !call getarg(i_arg,tatonam)
            ntimes=1
            norbgpu=0
            read(unit=tatonam,fmt=*,iostat=ierror) ntimes
            if (ierror == 0) then
               write(*,'(1x,a,i0,a)')&
                  &   'Repeat each calculation ',ntimes,' times.'
               i_arg = i_arg + 1
               call get_command_argument(i_arg, value = tatonam)
               !call getarg(i_arg,tatonam)
               read(unit=tatonam,fmt=*,iostat=ierror) norbgpu
            end if
            call testGPU(trim(radical), nproc, ntimes, norbgpu)
            exit loop_getargs
         case('convert')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = fileFrom)
            !call getarg(i_arg,fileFrom)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = fileTo)
            !call getarg(i_arg,fileTo)
            write(*,'(1x,5a)')&
                 &   'convert "', trim(fileFrom),'" file to "', trim(fileTo),'"'
            call convertField(trim(fileFrom), trim(fileTo))
            exit loop_getargs
         case('exportwf')
            mem_guess = .false.
            !Export wavefunctions (cube format)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = filename_wfn)
            !call getarg(i_arg,filename_wfn)
            ! Read optional additional arguments with the iband, up/down and ikpt
            export_wf_iband = 1
            export_wf_ispin = 1
            export_wf_ikpt  = 1
            export_wf_ispinor = 1
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = tatonam, status = istat)
            if(trim(tatonam)/='' .and. istat == 0) then
               read(unit=tatonam,fmt=*) export_wf_iband
            end if
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = tatonam, status = istat)
            if(trim(tatonam)/='' .and. istat == 0) then
               read(unit=tatonam,fmt=*) export_wf_ispin
            end if
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = tatonam, status = istat)
            if(trim(tatonam)/='' .and. istat == 0) then
               read(unit=tatonam,fmt=*) export_wf_ikpt
            end if
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = tatonam, status = istat)
            if(trim(tatonam)/='' .and. istat == 0) then
               read(unit=tatonam,fmt=*) export_wf_ispinor
            end if
            write(*,'(1x,5a)')&
                 &   'convert "', trim(filename_wfn),'" file to cube'
            call exportWavefunction(trim(radical), trim(filename_wfn), &
                 export_wf_iband, export_wf_ikpt, export_wf_ispin, export_wf_ispinor)
            exit loop_getargs
         case('exportproj')
            mem_guess = .false.
            !Export wavefunctions (cube format)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = filename_proj)
            call exportProjector(trim(radical), trim(filename_proj))
            exit loop_getargs
         case('atwf')
            mem_guess = .false.
            write(*,'(1x,a)')&
                 &   'Perform the calculation of atomic wavefunction of the first atom'
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = tatonam)
            !call getarg(i_arg,tatonam)
            read(unit=tatonam,fmt=*,iostat=ierror) ng
            write(*,'(1x,a,i0,a)')&
                 &   'Use gaussian basis of',ng,' elements.'
            call atomicWavefunctions(trim(radical), ng)
            exit loop_getargs
         case('convert-positions')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = fileFrom)
            !call getarg(i_arg,fileFrom)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = fileTo)
            !call getarg(i_arg,fileTo)
            write(*,'(1x,5a)')&
                 &   'convert input file "', trim(fileFrom),'" file to "', trim(fileTo),'"'
            call convertPositions(trim(fileFrom), trim(fileTo))
            exit loop_getargs
         case('transform-coordinates')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = direction)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = fileFrom)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = fileTo)
            write(*,'(1x,5a)')&
                 &   'convert input file "', trim(fileFrom),'" file to "', trim(fileTo),'"'
            call transformCoordinates(direction, trim(fileFrom), trim(fileTo))
            exit loop_getargs
         case('pdos')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = coeff_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ham_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = overlap_file)
            !i_arg = i_arg + 1
            !call get_command_argument(i_arg, value = ntmb_)
            !read(ntmb_,fmt=*,iostat=ierror) ntmb
            !i_arg = i_arg + 1
            !call get_command_argument(i_arg, value = norbks_)
            !read(norbks_,fmt=*,iostat=ierror) norbks
            !i_arg = i_arg + 1
            !call get_command_argument(i_arg, value = nat_)
            !read(nat_,fmt=*,iostat=ierror) nat
            !i_arg = i_arg + 1
            !call get_command_argument(i_arg, value = interval_)
            !read(interval_,fmt=*,iostat=ierror) interval
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = pdos_file)
            !i_arg = i_arg + 1
            !call get_command_argument(i_arg, value = posinp_file)
            npdos = 1
            write(*,'(1x,a,i0,3a)')&
                 &   'calculate ', npdos,' PDOS based on the coeffs in the file "', trim(coeff_file),'"'
            !&   'calculate ', npdos,' PDOS based on the coeffs (', ntmb, 'x', norbks, ') in the file "', trim(coeff_file),'"'
            call f_err_throw('This functionality is now handled by utilities')
            exit loop_getargs
         case('dos')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = coeff_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ham_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = overlap_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = sigma_)
            read(sigma_,fmt=*,iostat=ierror) sigma
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = npts_)
            read(npts_,fmt=*,iostat=ierror) npts
            write(*,'(1x,3a)')&
                 &   'calculate total DOS based on the coeffs in the file "', trim(coeff_file),'"'
            call f_err_throw('This functionality is now handled by utilities')
            exit loop_getargs
         case('kernel-analysis')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = coeff_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = kernel_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ntmb_)
            read(ntmb_,fmt=*,iostat=ierror) ntmb
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = norbks_)
            read(norbks_,fmt=*,iostat=ierror) norbks
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = nat_)
            read(nat_,fmt=*,iostat=ierror) nat
            write(*,'(1x,5a)')&
                 &   'calculate a full kernel from the coeffs in "', trim(coeff_file), &
                 &'" and compres it to the sparse kernel in "', trim(kernel_file),'"'
            call kernelAnalysis(trim(coeff_file), ntmb, norbks, nat, trim(kernel_file))
            exit loop_getargs
         case('extract-submatrix')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = matrix_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ntmb_)
            read(ntmb_,fmt=*,iostat=ierror) ntmb
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = nat_)
            read(nat_,fmt=*,iostat=ierror) nat
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = nsubmatrices_)
            read(nsubmatrices_,fmt=*,iostat=ierror) nsubmatrices
            write(*,'(1x,a,i0,3a,2(i0,a))')&
                 &   'extract ',nsubmatrices,' submatrices from the matrix in "', trim(matrix_file),'" (size ',ntmb,'x',ntmb,')'
            call extractSubMatrix(trim(matrix_file), ntmb, nat, nsubmatrices)
            exit loop_getargs
         case('solve-eigensystem')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ham_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = overlap_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ntmb_)
            read(ntmb_,fmt=*,iostat=ierror) ntmb
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = nat_)
            read(nat_,fmt=*,iostat=ierror) nat
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = coeff_file)
            read(nsubmatrices_,fmt=*,iostat=ierror) nsubmatrices
            write(*,'(1x,2(a,i0))')&
                 &   'solve the eigensystem Hc=lSc of size ',ntmb,'x',ntmb
            call f_err_throw('This functionality is not implemented anymore.')
            exit loop_getargs
         case('analyze-coeffs')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = coeff_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ntmb_)
            read(ntmb_,fmt=*,iostat=ierror) ntmb
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = norbks_)
            read(norbks_,fmt=*,iostat=ierror) norbks
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = nat_)
            read(nat_,fmt=*,iostat=ierror) nat
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ncategories_)
            read(ncategories_,fmt=*,iostat=ierror) ncategories
            write(*,'(1x,a)')&
                 &   'analyze the coeffs'
            call analyseCoeffs(trim(coeff_file), ntmb, norbks, nat, ncategories)
            exit loop_getargs
         case('peel-matrix')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = matrix_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ntmb_)
            read(ntmb_,fmt=*,iostat=ierror) ntmb
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = nat_)
            read(nat_,fmt=*,iostat=ierror) nat
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = cutoff_)
            read(cutoff_,fmt=*,iostat=ierror) cutoff
            write(*,'(1x,a)')&
                 &   'peel the matrix'
            call peelMatrix(trim(matrix_file), ntmb, nat, cutoff)
            exit loop_getargs
         case('multiply-matrices')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = amatrix_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = bmatrix_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = cmatrix_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ntmb_)
            read(ntmb_,fmt=*,iostat=ierror) ntmb
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = nat_)
            read(nat_,fmt=*,iostat=ierror) nat
            i_arg = i_arg + 1
            write(*,'(1x,a,2(i0,a))')&
                 &   'multiply the matrices (size ',ntmb,'x',ntmb,')'
            call multiplyMatrices(trim(amatrix_file), trim(bmatrix_file), ntmb, nat, trim(cmatrix_file))
            exit loop_getargs
         case('matrixpower')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = inmatrix_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = outmatrix_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = ntmb_)
            read(ntmb_,fmt=*,iostat=ierror) ntmb
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = nat_)
            read(nat_,fmt=*,iostat=ierror) nat
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = power_)
            read(power_,fmt=*,iostat=ierror) power
            i_arg = i_arg + 1
            write(*,'(1x,a,2(i0,a))')&
                 &   'calculate the power of a matrix'
            call matrixPower(trim(inmatrix_file), ntmb, nat, power, trim(outmatrix_file))
            exit loop_getargs
         case('plot-wavefunction')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = wf_file)
            write(*,'(1x,a,2(i0,a))')&
                 &   'plot the wave function from file ',trim(wf_file)
            exit loop_getargs
         case('suggest-cutoff')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = posinp_file)
            write(*,'(1x,2a)')&
                 &   'suggest cutoff radii based on the atomic positions in ',trim(posinp_file)
            call suggestCutoff(trim(posinp_file))
            exit loop_getargs
         case('charge-analysis')
            mem_guess = .false.
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = overlap_file)
            i_arg = i_arg + 1
            call get_command_argument(i_arg, value = kernel_file)
            write(*,'(1x,2a)')&
                 &   'perform a Loewdin charge analysis'
            call f_err_throw('This functionality is now handled by utilities')
            exit loop_getargs
         case('dd')
            ! dd: disable deprecation message
            disable_deprecation = .true.
         case('l')
            ! l: log to disk
            logfile = .true.
         case('')
            exit loop_getargs
         case default
            ! Use value as radical for input files.
            write(radical, "(A)") trim(tatonam)
         end select
         i_arg = i_arg + 1
      end do loop_getargs
   end if

   if (mem_guess) then
      if (.not. disable_deprecation) call deprecation_message()
      call memoryGuess(trim(radical), nproc, logfile, output_grid > 0)
      if (.not. disable_deprecation) call deprecation_message()
      call f_lib_finalize()
      stop
   end if

   call f_lib_finalize()

END PROGRAM memguess

!> Deprecated message for memguess (do not use directly!!)
subroutine deprecation_message()
   implicit none
   write(*, "(15x,A)") "+--------------------------------------------+"
   write(*, "(15x,A)") "|                                            |"
   write(*, "(15x,A)") "| /!\ memguess is deprecated since 1.6.0 /!\ |"
   write(*, "(15x,A)") "|                                            |"
   write(*, "(15x,A)") "|     Use bigdft-tool  instead,  located     |"
   write(*, "(15x,A)") "|     in the  build directory or in  the     |"
   write(*, "(15x,A)") "|     bin directory of the install path.     |"
   write(*, "(15x,A)") "|       $ bigdft-tool -h for help            |"
   write(*, "(15x,A)") "|                                            |"
   write(*, "(15x,A)") "+--------------------------------------------+"
END SUBROUTINE deprecation_message
