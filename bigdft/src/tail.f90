!> @file
!!  Routines to do tail calculation (correct effect of finite size)
!! @author
!!    Copyright (C) 2007-2011 BigDFT group 
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS 


!> Calculate the finite size corrections over wavefunctions
!! Conceived only for isolated Boundary Conditions, no SIC correction
subroutine CalculateTailCorrection(iproc,nproc,at,rbuf,orbs,&
     Glr,nlpsp,ncongt,pot,hgrid,rxyz,crmult,frmult,nspin,&
     psi,output_denspot,ekin_sum,epot_sum,eproj_sum,paw)
  use module_precisions
  use module_types
  use yaml_output
  use module_interfaces, only: orbitals_descriptors, export_grids
  use psp_projectors_base
  use gaussians, only: gaussian_basis
  use public_enums
  use bounds
  use locregs
  use liborbs_functions
  use compression
  use orbitalbasis
  use psp_projectors
  use box, only: cell, cell_new
  use at_domain
  use numerics, only: onehalf,pi
  use f_precisions, only: f_byte
  use module_bigdft_mpi
  use module_bigdft_errors
  use module_bigdft_arrays
  use module_bigdft_profiling
  implicit none
  type(atoms_data), intent(in) :: at
  type(orbitals_data), intent(in) :: orbs
  type(locreg_descriptors), intent(in) :: Glr
  type(DFT_PSP_projectors), intent(inout) :: nlpsp
  integer, intent(in) :: iproc,nproc,ncongt,nspin
  logical, intent(in) :: output_denspot
  real(kind=8), dimension(3), intent(in) :: hgrid
  real(kind=8), intent(in) :: crmult,frmult,rbuf
  !real(kind=8), dimension(at%astruct%ntypes,3), intent(in) :: radii_cf
  real(kind=8), dimension(3,at%astruct%nat), intent(in) :: rxyz
  real(kind=8), dimension(Glr%mesh%ndims(1),Glr%mesh%ndims(2),Glr%mesh%ndims(3),nspin), intent(in) :: pot
  real(kind=8), dimension(array_dim(Glr),orbs%norbp), intent(in) :: psi
  real(kind=8), intent(out) :: ekin_sum,epot_sum,eproj_sum
  type(paw_objects),intent(inout)::paw
  !local variables
  type(locreg_descriptors), target :: lr
  character(len=*), parameter :: subname='CalculateTailCorrection'
  integer :: iat,iorb,npt,ipt,i,nbuf,ispin
  integer :: nb1,nb2,nb3
  integer :: n1,n2,n3
  real(kind=8) :: alatb1,alatb2,alatb3,ekin,epot,eproj,cprecr,sum_tail !n(c) eproj1 epot1,ekin1
  type(orbitals_data), target :: orbsb
  type(orbital_basis), target :: ob
  type(ket) :: psi_it
  type(DFT_PSP_projector_iter) :: psp_it
  type(cell) :: mesh
  logical(f_byte), dimension(:,:,:), allocatable :: logrid_c,logrid_f
  ! real(kind=8), dimension(:,:), allocatable :: wrkallred
  real(kind=8), dimension(:,:), pointer :: txyz
  integer, dimension(2, 3) :: nbox, nboxf
  
  integer nwarnings

  type(wvf_manager) :: manager
  type(wvf_mesh_view) :: psir
  type(wvf_daub_view) :: psib, hpsib
  
  interface
     subroutine apply_potential(n1,n2,n3,nl1,nl2,nl3,nbuf,nspinor,npot,psir,pot,epot,&
          &   ibyyzz_r) !optional
       use module_precisions, only: gp,dp,wp
       implicit none
       integer, intent(in) :: n1,n2,n3,nl1,nl2,nl3,nbuf,nspinor,npot
       real(wp), dimension(-nl1:2*n1+2+nl1,-nl2:2*n2+2+nl2,-nl3:2*n3+2+nl3,nspinor), intent(inout) :: psir
       real(wp), dimension(-nl1:2*n1+2+nl1-4*nbuf,-nl2:2*n2+2+nl2-4*nbuf,-nl3:2*n3+2+nl3-4*nbuf,npot), intent(in) :: pot
       integer, dimension(2,-14:2*n2+16,-14:2*n3+16), intent(in), optional :: ibyyzz_r
       real(gp), intent(out) :: epot
     END SUBROUTINE apply_potential
  end interface

  !SM This routine had as argument hgrid(1), but must now have all three dur to
  !the modified calling sequence of convolutions routine. Still only use the
  !first entry throughout the routine.

  call f_routine(id='CalculateTailCorrection')

  n1=Glr%mesh_coarse%ndims(1)-1
  n2=Glr%mesh_coarse%ndims(2)-1
  n3=Glr%mesh_coarse%ndims(3)-1

  nbuf=nint(rbuf/hgrid(1))
  !    --- new grid sizes n1,n2,n3
  nb1=n1+2*nbuf
  nb2=n2+2*nbuf
  nb3=n3+2*nbuf

  ! Create new structure with modified grid sizes
!!$  call init_lr(lr,Glr%geocode,0.5*hgrid,nb1,nb2,nb3,&
  !call init_lr(lr,domain_geocode(Glr%mesh%dom),0.5*hgrid,nb1,nb2,nb3,&
  nbox(1, :) = 0
  nbox(2, :) = (/ nb1, nb2, nb3 /)
  nboxf = Glr%nboxf + nbuf
  call init_lr(lr,Glr%mesh%dom,0.5*hgrid,nbox,nboxf,.true.)
 
  alatb1=real(nb1,kind=8)*hgrid(1) 
  alatb2=real(nb2,kind=8)*hgrid(1) 
  alatb3=real(nb3,kind=8)*hgrid(1)

  if (iproc == 0) then
     call yaml_comment('Finite-Size correction',hfill='-')
     call yaml_mapping_open('Estimation of Finite-Size Corrections')
     call yaml_map('Effective AU space more around each external atom',rbuf,fmt='(f6.3)')
     call yaml_map('Adding grid points around cell',nbuf)
     call yaml_map('Effective box size (AU)', (/ alatb1,alatb2,alatb3 /), fmt='(1pe12.5)')
     call yaml_map('Grid spacing units', (/ nb1,nb2,nb3 /))
     !write(*,'(1x,a)')&
     !     '---------------------------------------------- Estimation of Finite-Size Corrections'
     !write(*,'(1x,a,f6.3,a)') &
     !     'F-S Correction for an effective space of ',rbuf,' AU more around each external atom'
     !write(*,'(1x,a,i6,a)') &
     !     '                  requires the adding of ',nbuf,' additional grid points around cell'
     !write(*,'(1x,a,19x,a)') &
     !     '   Effective box size,   Atomic Units:','grid spacing units:'
     !write(*,'(1x,a,3(1x,1pe12.5),3x,3(1x,i9))')&
     !     '            ',alatb1,alatb2,alatb3,nb1,nb2,nb3
  end if

!end do

  !---reformat wavefunctions

  ! fine grid size (needed for creation of input wavefunction, preconditioning)
  if (iproc == 0) then
     call yaml_map('Extremes for the new high resolution grid points', &
          & (/ lr%nboxf(1,1), lr%nboxf(2,1), lr%nboxf(1,2),lr%nboxf(2,2),lr%nboxf(1,3),lr%nboxf(2,3) /))

     !write(*,'(1x,a,3x,3(3x,i4,a1,i0))')&
     !     '  Extremes for the new high resolution grid points:',&
     !     nbfl1,'<',nbfu1,nbfl2,'<',nbfu2,nbfl3,'<',nbfu3
  endif

  ! change atom coordinates according to the enlarged box
  txyz = f_malloc_ptr((/ 3, at%astruct%nat /),id='txyz')
  do iat=1,at%astruct%nat
     txyz(1,iat)=rxyz(1,iat)+real(nbuf,kind=8)*hgrid(1)
     txyz(2,iat)=rxyz(2,iat)+real(nbuf,kind=8)*hgrid(2)
     txyz(3,iat)=rxyz(3,iat)+real(nbuf,kind=8)*hgrid(3)
  enddo

  ! determine localization region for all orbitals, but do not yet fill the descriptor arrays
  logrid_c = f_malloc((/ 0.to.nb1, 0.to.nb2, 0.to.nb3 /),id='logrid_c')
  logrid_f = f_malloc((/ 0.to.nb1, 0.to.nb2, 0.to.nb3 /),id='logrid_f')

  if (domain_geocode(at%astruct%dom) /= 'F') call f_err_throw('Geocode has to be F to set mesh here',&
       err_name='BIGDFT_RUNTIME_ERROR')
  mesh=cell_new(at%astruct%dom,[nb1+1, nb2+1, nb3+1],hgrid)

  call fill_logrid(mesh,0,nb1,0,nb2,0,nb3,nbuf,at%astruct%nat,at%astruct%ntypes,at%astruct%iatype,txyz, & 
       at%radii_cf(1,1),crmult,logrid_c)
  call fill_logrid(mesh,0,nb1,0,nb2,0,nb3,0,at%astruct%nat,at%astruct%ntypes,at%astruct%iatype,txyz, & 
       at%radii_cf(1,2),frmult,logrid_f)

  call init_glr_from_grids(lr, logrid_c, logrid_f, .true., iproc == 0)
  if (iproc == 0) call print_lr_compression(lr)

  ! Create the file grid.xyz to visualize the grid of functions
  if (iproc ==0 .and. output_denspot) then
     call yaml_comment('Writing the file describing the new atomic positions of the effective system')
     call export_grids("grid_tail.xyz", at, txyz, hgrid(1), hgrid(2), hgrid(3), &
          nb1, nb2, nb3, logrid_c, logrid_f)
  endif

  call f_free(logrid_c)
  call f_free(logrid_f)


  if (iproc == 0) then
     call yaml_map('Wavefunction memory occupation in the extended grid (Bytes):', &
          array_dim(lr)*8)
     call yaml_comment('Calculating tail corrections, orbitals are processed separately')
     !write(*,'(1x,a,i0)') &
     !     'Wavefunction memory occupation in the extended grid (Bytes): ',&
     !     (nvctrb_c+7*nvctrb_f)*8
     !write(*,'(1x,a,i0,a)') &
     !     'Calculating tail corrections on ',orbs%norbp,' orbitals per processor.'
     !write(*,'(1x,a)',advance='no') &
     !     '     orbitals are processed separately'
  end if

  manager = wvf_manager_new()

  ekin_sum=0.d0
  epot_sum=0.d0
  eproj_sum=0.d0

  !allocate the fake orbital structure for the application of projectors
  call orbitals_descriptors(0,1,1,1,0,1,1,1, &
       reshape((/0._gp,0._gp,0._gp/),(/3,1/)),(/1._gp /),orbsb,LINEAR_PARTITION_NONE)

  !change positions in gaussian projectors
  call psp_update_positions(nlpsp, lr, Glr, txyz)

  do iorb=1,orbs%norbp
     psib = wvf_view_on_daub(manager, lr)

     if(orbs%spinsgn(iorb+orbs%isorb)>0.0d0) then
        ispin=1
     else
        ispin=2
     end if
     psi_it%ncplx = 1
     psi_it%n_ket = 1
     psi_it%lr => lr
     psi_it%phi_wvl => f_subptr(psib%c%mem(1, 1), size = array_dim(lr))
     psi_it%phi = psib
     psi_it%ispin = ispin
     psi_it%ispsi = 1
     psi_it%nphidim = array_dim(lr)
     psi_it%ob => ob
     ob%orbs => orbsb
     orbsb%npsidim_orbs = array_dim(lr)
       
     !build the compressed wavefunction in the enlarged box
     call lr_rewrap(lr, Glr, psi(:,iorb), psi_it%phi_wvl)

     npt=2
     tail_adding: do ipt=1,npt

        !for the tail application leave the old-fashioned hamiltonian
        !since it only deals with Free BC and thus no k-points or so

        !calculate gradient
        psir = wvf_view_to_mesh(psib, store = .true.)
        call apply_potential(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1, &
             lr%mesh_coarse%ndims(3)-1,1,1,1,nbuf,1,1,psir%c%mem,pot(1,1,1,ispin),epot,&
             lr%bounds%ibyyzz_r) !optional
        hpsib = wvf_kinetic_operator(psir, ekin = ekin)
        !write(*,'(a,3i3,2f12.8)') 'applylocpotkinone finished',iproc,iorb,ipt,epot,ekin

        eproj=0.0d0
        call DFT_PSP_projectors_iter_new(psp_it, nlpsp)
        loop_proj: do while (DFT_PSP_projectors_iter_next(psp_it, ilr = 1, lr = lr, glr = lr))
           call DFT_PSP_projectors_iter_ensure(psp_it, [0._gp, 0._gp, 0._gp], 0, nwarnings, lr)
           call DFT_PSP_projectors_iter_apply(psp_it, psi_it, at, eproj, hpsib%c%mem(:,1), paw)
        end do loop_proj

        if (ipt == npt) exit tail_adding

        !calculate residue for the single orbital

        !calculate tail using the preconditioner as solver for the green function application
        cprecr=-orbs%eval(iorb+orbs%isorb)
        call wvf_add(hpsib, cprecr * psib)
        call precong(lr,ncongt,cprecr,hpsib%c%mem)
        !call plot_wf(10,nb1,nb2,nb3,hgrid,nsegb_c,nvctrb_c,keyg,keyv,nsegb_f,nvctrb_f,  & 
        !      txyz(1,1),txyz(2,1),txyz(3,1),psib)

        ! add tail to the bulk wavefunction
        sum_tail=0.d0
        do i=1,array_dim(lr)
           psib%c%mem(i,1)=psib%c%mem(i,1)-hpsib%c%mem(i,1)
           sum_tail=sum_tail+psib%c%mem(i,1)**2
        enddo
        sum_tail=sqrt(sum_tail)
        !write(*,'(1x,a,i3,3(1x,1pe13.6),1x,1pe9.2)') &
        !     'BIG: iorb,ekin,epot,eproj,gnrm',iorb,ekin,epot,eproj,tt

        !values of the energies before tail application
        !n(c) ekin1=ekin
        !n(c) epot1=epot
        !n(c) eproj1=eproj
        !write(*,'(1x,a,1x,i0,f18.14)') 'norm orbital + tail',iorb,sum_tail
        !call plot_wf(20,nb1,nb2,nb3,hgrid,nsegb_c,nvctrb_c,keyg,keyv,nsegb_f,nvctrb_f,  & 
        !      txyz(1,1),txyz(2,1),txyz(3,1),psib)

        sum_tail=1.d0/sum_tail
        do i=1,array_dim(lr)
           psib%c%mem(i,1)=psib%c%mem(i,1)*sum_tail
        enddo

        call wvf_manager_release(manager, psir)
        call wvf_manager_release(manager, hpsib)
     end do tail_adding

!!!     write(*,'(1x,a,i3,3(1x,1pe13.6),2(1x,1pe9.2))') &
!!!          'BIG: iorb,denergies,gnrm,dnorm',&
!!!          iorb,ekin-ekin1,epot-epot1,eproj-eproj1,tt,sum_tail-1.d0

     ! if (iproc == 0) then
     !    !write(*,'(a)',advance='no') &
     !    !     repeat('.',((iorb+orbs%isorb)*40)/orbs%norbp-((iorb-1)*40)/orbs%norbp)
     ! end if
     ekin_sum=ekin_sum+ekin*orbs%occup(iorb+orbs%isorb)
     epot_sum=epot_sum+epot*orbs%occup(iorb+orbs%isorb)
     eproj_sum=eproj_sum+eproj*orbs%occup(iorb+orbs%isorb)

     call wvf_manager_release(manager)
  end do

  call deallocate_orbs(orbsb)

  call f_free_ptr(txyz)

  call deallocate_locreg_descriptors(lr)

  call wvf_deallocate_manager(manager)

  if (nproc > 1) then
   call fmpi_allreduce(ekin_sum,epot_sum,eproj_sum,op=FMPI_SUM,comm=bigdft_mpi%mpi_comm)
     ! !if (iproc == 0) then
     ! !   write(*,'(1x,a,f27.14)')'Tail calculation ended'
     ! !endif
     ! wrkallred = f_malloc((/ 3, 2 /),id='wrkallred')
     ! wrkallred(1,2)=ekin_sum
     ! wrkallred(2,2)=epot_sum 
     ! wrkallred(3,2)=eproj_sum 
     ! call MPI_ALLREDUCE(wrkallred(1,2),wrkallred(1,1),3,&
     !      MPI_DOUBLE_PRECISION,MPI_SUM,bigdft_mpi%mpi_comm,ierr)
     ! ekin_sum=wrkallred(1,1) 
     ! epot_sum=wrkallred(2,1) 
     ! eproj_sum=wrkallred(3,1)
     ! call f_free(wrkallred)
  endif

  call f_release_routine()

END SUBROUTINE CalculateTailCorrection

!>   routine for applying the local potentials
!!   supports the non-collinear case, the buffer for tails and different Boundary Conditions
!!   Optimal also for the complex wavefuntion case
!!   might generalize the buffers for two different localization regions, provided that the potential lies in a bigger region
subroutine apply_potential(n1,n2,n3,nl1,nl2,nl3,nbuf,nspinor,npot,psir,pot,epot,&
     ibyyzz_r) !optional
  use module_precisions
  implicit none
  integer, intent(in) :: n1,n2,n3,nl1,nl2,nl3,nbuf,nspinor,npot
  real(wp), dimension(-14*nl1:2*n1+1+15*nl1,-14*nl2:2*n2+1+15*nl2,-14*nl3:2*n3+1+15*nl3,nspinor), intent(inout) :: psir
  real(wp), dimension(-14*nl1:2*n1+1+15*nl1-4*nbuf,-14*nl2:2*n2+1+15*nl2-4*nbuf,&
       -14*nl3:2*n3+1+15*nl3-4*nbuf,npot), intent(in) :: pot
  integer, dimension(2,-14:2*n2+16,-14:2*n3+16), intent(in), optional :: ibyyzz_r
  real(gp), intent(out) :: epot
  !local variables
  integer :: i1,i2,i3,i1s,i1e,ispinor
  real(wp) :: tt11,tt22,tt33,tt44,tt13,tt14,tt23,tt24,tt31,tt32,tt41,tt42,tt
  real(wp) :: psir1,psir2,psir3,psir4,pot1,pot2,pot3,pot4
  real(gp) :: epot_p
 
  !the Tail treatment is allowed only in the Free BC case
  if (nbuf /= 0 .and. nl1*nl2*nl3 == 0) stop 'NONSENSE: nbuf/=0 only for Free BC'

  epot=0.0_wp
!$omp parallel default(private)&
!$omp shared(pot,psir,n1,n2,n3,epot,ibyyzz_r,nl1,nl2,nl3,nbuf,nspinor)
  !case without bounds
  i1s=-14*nl1
  i1e=2*n1+1+15*nl1
  epot_p=0._gp
!$omp do
  do i3=-14*nl3,2*n3+1+15*nl3
     if (i3 >= -14+2*nbuf .and. i3 <= 2*n3+16-2*nbuf) then !check for the nbuf case
        do i2=-14*nl2,2*n2+1+15*nl2
           if (i2 >= -14+2*nbuf .and. i2 <= 2*n2+16-2*nbuf) then !check for the nbuf case
              !this if statement is inserted here for avoiding code duplication
              !it is to be seen whether the code results to be too much unoptimised
              if (present(ibyyzz_r)) then
                 !in this case we are surely in Free BC
                 !the min is to avoid to calculate for no bounds
                 do i1=-14+2*nbuf,min(ibyyzz_r(1,i2,i3),ibyyzz_r(2,i2,i3))-14-1
                    psir(i1,i2,i3,:)=0.0_wp
                 enddo
                 i1s=max(ibyyzz_r(1,i2,i3)-14,-14+2*nbuf)
                 i1e=min(ibyyzz_r(2,i2,i3)-14,2*n1+16-2*nbuf)
              end if
              !here we put the branchments wrt to the spin
              if (nspinor == 4) then
                 do i1=i1s,i1e
                    !wavefunctions
                    psir1=psir(i1,i2,i3,1)
                    psir2=psir(i1,i2,i3,2)
                    psir3=psir(i1,i2,i3,3)
                    psir4=psir(i1,i2,i3,4)
                    !potentials
                    pot1=pot(i1-2*nbuf,i2-2*nbuf,i3-2*nbuf,1)
                    pot2=pot(i1-2*nbuf,i2-2*nbuf,i3-2*nbuf,2)
                    pot3=pot(i1-2*nbuf,i2-2*nbuf,i3-2*nbuf,3)
                    pot4=pot(i1-2*nbuf,i2-2*nbuf,i3-2*nbuf,4)

                    !diagonal terms
                    tt11=pot1*psir1 !p1
                    tt22=pot1*psir2 !p2
                    tt33=pot4*psir3 !p3
                    tt44=pot4*psir4 !p4
                    !Rab*Rb
                    tt13=pot2*psir3 !p1
                    !Iab*Ib
                    tt14=pot3*psir4 !p1
                    !Rab*Ib
                    tt23=pot2*psir4 !p2
                    !Iab*Rb
                    tt24=pot3*psir3 !p2
                    !Rab*Ra
                    tt31=pot2*psir1 !p3
                    !Iab*Ia
                    tt32=pot3*psir2 !p3
                    !Rab*Ia
                    tt41=pot2*psir2 !p4
                    !Iab*Ra
                    tt42=pot3*psir1 !p4

                    ! Change epot later
                    epot_p=epot_p+tt11*psir1+tt22*psir2+tt33*psir3+tt44*psir4+&
                         2.0_gp*tt31*psir3-2.0_gp*tt42*psir4+2.0_gp*tt41*psir4+2.0_gp*tt32*psir3

                    !wavefunction update
                    !p1=h1p1+h2p3-h3p4
                    !p2=h1p2+h2p4+h3p3
                    !p3=h2p1+h3p2+h4p3
                    !p4=h2p2-h3p1+h4p4
                    psir(i1,i2,i3,1)=tt11+tt13-tt14
                    psir(i1,i2,i3,2)=tt22+tt23+tt24
                    psir(i1,i2,i3,3)=tt33+tt31+tt32
                    psir(i1,i2,i3,4)=tt44+tt41-tt42
                 end do
              else
                 do ispinor=1,nspinor
                    do i1=i1s,i1e
                       !the local potential is always real
                       tt=pot(i1-2*nbuf,i2-2*nbuf,i3-2*nbuf,1)*psir(i1,i2,i3,ispinor)
                       epot_p=epot_p+real(tt*psir(i1,i2,i3,ispinor),gp)
                       psir(i1,i2,i3,ispinor)=tt
                    end do
                 end do
              end if
              
              if (present(ibyyzz_r)) then
                 !the max is to avoid the calculation for no bounds
                 do i1=max(ibyyzz_r(1,i2,i3),ibyyzz_r(2,i2,i3))-14+1,2*n1+16-2*nbuf
                    psir(i1,i2,i3,:)=0.0_wp
                 enddo
              end if

           else
              do i1=-14,2*n1+16
                 psir(i1,i2,i3,:)=0.0_wp
              enddo
           endif
        enddo
     else
        do i2=-14,2*n2+16
           do i1=-14,2*n1+16
              psir(i1,i2,i3,:)=0.0_wp
           enddo
        enddo
     endif
  enddo
!$omp end do

!$omp critical
  epot=epot+epot_p
!$omp end critical

!$omp end parallel

END SUBROUTINE apply_potential
