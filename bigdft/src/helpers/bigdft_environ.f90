module module_bigdft_environ
  use f_environment
  use f_precisions
  implicit none

  public

  real(f_double) :: unitary_test_memory_tolerance=1.e-14_f_double

  real(f_double) :: unitary_test_calculation_strict_tolerance=1.e-12_f_double

  real(f_double) :: unitary_test_calculation_loose_tolerance=1.e-10_f_double

  contains

     subroutine bigdft_environment_acquire()
      implicit none

      call f_getenv('BIGDFT_UNITARY_TESTS_MEMTOL',unitary_test_memory_tolerance)

      call f_getenv('BIGDFT_UNITARY_TESTS_STRICTTOL',unitary_test_calculation_strict_tolerance)

      call f_getenv('BIGDFT_UNITARY_TESTS_LOOSETOL',unitary_test_calculation_loose_tolerance)

    end subroutine bigdft_environment_acquire

end module module_bigdft_environ