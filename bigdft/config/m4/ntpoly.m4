# -*- Autoconf -*-
#
# Copyright (c) 2020 BigDFT Group (D. Caliste, L.Genovese)
# All rights reserved.
#
# This file is part of the BigDFT software package. For license information,
# please see the COPYING file in the top-level directory of the BigDFT source
# distribution.
#
dnl we might think of inserting this option in the suitepkg.m4 file
AC_DEFUN([AX_NTPOLY],
[dnl Test for ntpoly
ax_have_NTPOLY_search="yes"
AC_ARG_ENABLE(ntpoly, AS_HELP_STRING([--disable-ntpoly], [Disable detection of NTPOLY compilation (enabled by default).]),
			 ax_have_NTPOLY_search=$enableval, ax_have_NTPOLY_search="yes")
if test x"$ax_have_NTPOLY_search" = "xyes" ; then

ax_tmp_incdirs=""
if test -n "$C_INCLUDE_PATH" ; then
  for path in ${C_INCLUDE_PATH//:/ }; do
    ax_tmp_incdirs="$ax_tmp_incdirs -I$path/NTPoly"
  done
fi

AX_PACKAGE([NTPOLY],[3],[-lNTPoly],[],[$ax_tmp_incdirs],
           [
program test
use processgridmodule
call test_ntpoly_link()
end program
],
           [
use processgridmodule
call test_ntpoly_link()
],
           [], [],[])
else
  ax_have_NTPOLY="no"
fi
])
