Installation
============

BigDFT can be installed directly using a package manager or compiled from 
source.
In general, the preferred installation way depends on the reason for the usage of the code.
In a supercomputer, we advise to install :ref:`from source<Install from Source>` first, in order to identify the correct installation environment, which would trigger the :ref:`remote compilation<Remote Compilation>` approach.

For instructions of a minimal install for Ubuntu see
:ref:`minimum install`.
If you run into errors during the install process, you may be able to find a
solution to your problem on the :ref:`errors and fixes` page.

There are also other installation methods, which are more adapted for local workstations and small laboratory clusters, which are listed below.

Install via pip
---------------

pip can be used to install the client version of BigDFT. The client version
can't be used to run actual calculations, but can be used to generate input
files and perform postprocessing.

.. code:: bash

    pip install PyBigDFT


Install via Docker
------------------

Docker is a virtualization platform that allows us to distribute BigDFT and
all of its associated dependencies in a single container. The BigDFT container,
along with detailed instructions, is available here_.

.. code:: bash

    sudo docker run -ti \
      -e OMP_NUM_THREADS=4 \
      -v $PWD:/results  -w /results \
      bigdft/runtime:openmpi \
      mpirun -np 2 bigdft

.. _here: https://hub.docker.com/r/bigdft/runtime//

Install via Package Mangers
---------------------------

Conda is a cross-platform package and environment management system which can
be used to install BigDFT on Linux / Mac platforms.

.. code:: bash

    conda install -c conda-forge bigdft-suite

BigDFT packages are also available for the more HPC-centric
`Spack <https://spack.io/>`_:

.. code:: bash

    spack install bigdft-suite

and `Guix-HPC <https://hpc.guix.info/>`_.

Install from Source
-------------------

.. toctree::
   :maxdepth: 1

   source-install
   minimum-install
   errors-and-fixes

Remote Compilation
------------------

.. toctree::
   :maxdepth: 1

   RemoteCompilation.ipynb