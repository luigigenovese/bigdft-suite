#ifndef IOBOX_H
#define IOBOX_H

#include "futile_cst.h"
#include <stdbool.h>
#include "box.h"
#include "dynamic_memory.h"
#include "dynamic_memory.h"


void read_field_dimensions(const char* filename,
  char geocode,
  int ndims[3],
  int* nspin);

void read_field(const char* filename,
  char geocode,
  int ndims[3],
  double hgrids[3],
  int* nspin,
  int ldrho,
  int nrho,
  double* rho,
  int (*nat),
  double_2d_pointer (*rxyz),
  int_1d_pointer (*iatypes),
  int_1d_pointer (*znucl));

void dump_field(const char* filename,
  const f90_cell* mesh,
  int nspin,
  const double* rho,
  const double (*rxyz),
  size_t rxyz_dim_0,
  size_t rxyz_dim_1,
  const int (*iatype),
  size_t iatype_dim_0,
  const int (*nzatom),
  size_t nzatom_dim_0,
  const int (*nelpsp),
  size_t nelpsp_dim_0,
  const int (*ixyz0)[3]);

#endif
