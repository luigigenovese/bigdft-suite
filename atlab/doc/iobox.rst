Reading and dumping scalar data on a grid: the :f:mod:`iobox` module
====================================================================

.. f:automodule:: iobox
