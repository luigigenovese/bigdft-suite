!> @file
!! Basic operations relative to the localization regions
!! @author
!!    Copyright (C) 2007-2014 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS
subroutine get_one_derivative_supportfunction(lr,phi,phid)
   use locregs
   implicit none
   
   ! Calling arguments
   type(locreg_descriptors),intent(in) :: lr
   real(kind=8),dimension(array_dim(lr)),intent(in) :: phi !< Basis functions
   real(kind=8),dimension(3*array_dim(lr)),intent(inout) :: phid  !< Derivative basis functions

   ! Local variables
   integer :: nf
   real(kind=8),dimension(0:3),parameter :: scal=1.d0
   real(kind=8),dimension(:),allocatable :: w_f1, w_f2, w_f3
   real(kind=8),dimension(:),allocatable :: w_c, phix_c, phiy_c, phiz_c
   real(kind=8),dimension(:,:,:,:),allocatable :: w_f, phix_f, phiy_f, phiz_f
   character(len=*),parameter :: subname='get_one_derivative_supportfunction'

   call allocateWorkarrays()

   ! Uncompress the wavefunction.
   call decompress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
        w_c, w_f, phi, scal, w_f1, w_f2, w_f3)

   call createDerivativeBasis(lr%mesh_coarse%ndims(1)-1,lr%mesh_coarse%ndims(2)-1,lr%mesh_coarse%ndims(3)-1, &
        lr%nboxf(1,1), lr%nboxf(2,1), lr%nboxf(1,2), lr%nboxf(2,2), &
        lr%nboxf(1,3), lr%nboxf(2,3),  &
        lr%bounds%kb%ibyz_c, lr%bounds%kb%ibxz_c, lr%bounds%kb%ibxy_c, &
        lr%bounds%kb%ibyz_f, lr%bounds%kb%ibxz_f, lr%bounds%kb%ibxy_f, &
        w_c, w_f, w_f1, w_f2, w_f3, phix_c, phix_f, phiy_c, phiy_f, phiz_c, phiz_f)

   ! Compress the x wavefunction.
   call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, phid, phix_c, phix_f, scal)

   ! Compress the y wavefunction.
   call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
        phid(array_dim(lr)+1), phiy_c, phiy_f, scal)

   ! Compress the z wavefunction.
   call compress_forstandard(lr%wfd, lr%nboxc, lr%nboxf, &
        phid(2*array_dim(lr)+1), phiy_c, phiy_f, scal)

   call deallocateWorkarrays()

contains

  subroutine allocateWorkarrays()
    use dynamic_memory
    ! THIS IS COPIED FROM allocate_work_arrays. Works only for free boundary.
    nf=(lr%nboxf(2,1)-lr%nboxf(1,1)+1)*(lr%nboxf(2,2)-lr%nboxf(1,2)+1)* &
       (lr%nboxf(2,3)-lr%nboxf(1,3)+1)

    ! Allocate work arrays
    w_c = f_malloc0(lr%mesh_coarse%ndim,id='w_c')

    w_f = f_malloc0((/ 1.to.7 , lr%nboxf(1,1).to.lr%nboxf(2,1) , lr%nboxf(1,2).to.lr%nboxf(2,2) , &
                 lr%nboxf(1,3) .to. lr%nboxf(2,3)/),id='w_f')

  
    w_f1 = f_malloc0(nf,id='w_f1')
    
    w_f2 = f_malloc0(nf,id='w_f2')

    w_f3 = f_malloc0(nf,id='w_f3')
  
  
    phix_f = f_malloc0((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), lr%nboxf(1,2).to.lr%nboxf(2,2), &
                              lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='phix_f')

    phix_c = f_malloc0(lr%mesh_coarse%ndim,id='phix_c')
    !call to_zero((lr%d%n1+1)*(lr%d%n2+1)*(lr%d%n3+1), phix_c(0,0,0))

    phiy_f = f_malloc0((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), lr%nboxf(1,2).to.lr%nboxf(2,2), &
                              lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='phiy_f')

    phiy_c = f_malloc0(lr%mesh_coarse%ndim,id='phiy_c')

    phiz_f = f_malloc0((/ 1.to.7, lr%nboxf(1,1).to.lr%nboxf(2,1), lr%nboxf(1,2).to.lr%nboxf(2,2), &
                              lr%nboxf(1,3).to.lr%nboxf(2,3) /),id='phiz_f')

    phiz_c = f_malloc0(lr%mesh_coarse%ndim,id='phiz_c')
  
  end subroutine allocateWorkarrays


  subroutine deallocateWorkarrays
    use dynamic_memory
    call f_free(w_c)
    call f_free(w_f)
    call f_free(w_f1)
    call f_free(w_f2)
    call f_free(w_f3)
    call f_free(phix_f)
    call f_free(phix_c)
    call f_free(phiy_f)
    call f_free(phiy_c)
    call f_free(phiz_f)
    call f_free(phiz_c)

  end subroutine deallocateWorkarrays

end subroutine get_one_derivative_supportfunction
