module liborbs_functions
  use f_precisions, only: f_long, f_address, f_double
  use f_arrays, only: f_scalar
  use liborbs_precisions
  use locregs, only: locreg_descriptors
  use liborbs_workarrays, only: workarr_sumrho, workarr_locham, workarr_precond, &
       workarr_quartic_convolutions, workarr_tensor_prod
  use liborbs_potentials, only: confpot_data
  implicit none

  type wvf_mem_buffer
     real(wp), dimension(:,:), pointer :: mem => null()
     integer(f_address), dimension(:,:), pointer :: gpu => null()
  end type wvf_mem_buffer

  type, public :: wvf_manager
     integer :: nspinor = 1

     !> Non zero in case of multiple lr
     integer(f_long) :: max_daub = 0
     integer(f_long) :: max_mesh = 0

     type(workarr_sumrho), pointer :: w_sumrho => null()
     type(workarr_locham), pointer :: w_locham => null()
     type(workarr_precond), pointer :: w_precond => null()
     type(workarr_quartic_convolutions), pointer :: w_quartic => null()
     type(workarr_tensor_prod), pointer :: w_tensp => null()
     integer :: w_tensp_iterm = 0

     integer :: i_mesh = 0
     type(wvf_mem_buffer) :: w_mesh_1
     type(wvf_mem_buffer) :: w_mesh_2
     type(wvf_mem_buffer) :: w_mesh_3
     type(wvf_mem_buffer) :: w_mesh_4
     type(wvf_mem_buffer) :: w_mesh_5

     integer :: i_daub = 0
     type(wvf_mem_buffer) :: w_daub_1
     type(wvf_mem_buffer) :: w_daub_2
     type(wvf_mem_buffer) :: w_daub_3
     type(wvf_mem_buffer) :: w_daub_4
     type(wvf_mem_buffer) :: w_daub_5

     integer(f_address) :: gpu_context = 0_f_address, gpu_queue = 0_f_address
  end type wvf_manager

  type wvf_daub_view
     type(wvf_manager), pointer :: manager => null()

     type(f_scalar) :: factor = f_scalar(1._f_double, 0._f_double)
     type(locreg_descriptors), pointer :: lr => null()
     type(wvf_mem_buffer) :: c
  end type wvf_daub_view

  type wvf_mesh_view
     type(wvf_manager), pointer :: manager => null()
     
     type(f_scalar) :: factor = f_scalar(1._f_double, 0._f_double)
     type(locreg_descriptors), pointer :: lr => null()
     type(wvf_mem_buffer) :: c
  end type wvf_mesh_view

  interface wvf_manager_new
     module procedure wvf_manager_new, wvf_manager_llr, wvf_manager_nlr
  end interface wvf_manager_new

  interface wvf_manager_release
     module procedure release_all, release_daub, release_mesh
  end interface wvf_manager_release

  public :: wvf_manager_new, wvf_deallocate_manager
  public :: wvf_manager_attach_gpu
  public :: wvf_manager_release

  interface wvf_view_on_daub_glr
     module procedure view_on_daub_glr, view_on_daub_glr_add
  end interface wvf_view_on_daub_glr

  interface wvf_view_to_mesh
     module procedure to_mesh, from_mesh, to_mesh_1d, to_mesh_add
  end interface wvf_view_to_mesh

  interface wvf_kinetic_operator
     module procedure mesh_kinetic_operator, mesh1d_kinetic_operator, meshadd_kinetic_operator
     module procedure daub_kinetic_operator, daub1d_kinetic_operator
  end interface wvf_kinetic_operator

  interface wvf_accumulate_gaussian
     module procedure daub_accumulate_gaussian, mesh_accumulate_gaussian
  end interface wvf_accumulate_gaussian
  
  interface wvf_add
     module procedure mesh_add, daub_add
  end interface wvf_add

  interface wvf_nrm
     module procedure daub_nrm, mesh_nrm
  end interface wvf_nrm

  interface wvf_normalize
     module procedure daub_normalize, mesh_normalize
  end interface wvf_normalize

  public :: wvf_view_on_daub, wvf_view_on_mesh
  public :: wvf_view_on_daub_glr
  public :: wvf_view_to_daub, wvf_view_to_mesh
  public :: wvf_potential_operator, wvf_potential_inplace
  public :: wvf_kinetic_operator, wvf_kinetic_operator_dir
  public :: wvf_non_local_inplace
  public :: wvf_precondition, wvf_precondition_ocl
  public :: wvf_scal_proj
  public :: wvf_add_noise
  public :: wvf_nrm, wvf_normalize
  public :: wvf_add, wvf_daub_view_new
  public :: wvf_accumulate_gaussian, wvf_finish_accumulate_gaussians

  interface operator(*)
     module procedure alpha_mesh, alpha_daub, scalar_mesh, scalar_daub
  end interface operator(*)

  interface operator(-)
     module procedure minus_mesh, minus_daub
  end interface operator(-)

  interface assignment(=)
     module procedure ass_to_mesh, ass_to_daub
  end interface assignment(=)

  interface operator(.dot.)
     module procedure dot_daub
  end interface operator(.dot.)

  public :: confpot_data

contains

  pure function wvf_daub_view_new() result(view)
      implicit none
      type(wvf_daub_view) :: view
      call nullify_wvf_daub_view(view)
  end function wvf_daub_view_new

  pure subroutine nullify_wvf_daub_view(view)
     implicit none
     type(wvf_daub_view), intent(out) :: view
  end subroutine nullify_wvf_daub_view

  pure subroutine nullify_wvf_manager(manager)
     implicit none
     type(wvf_manager), intent(out) :: manager
  end subroutine nullify_wvf_manager

  subroutine mem_buffer_free(buf)
    use dynamic_memory
    implicit none
    type(wvf_mem_buffer), intent(inout) :: buf

    integer :: ispinor

    if (associated(buf%mem)) call f_free_ptr(buf%mem)
    if (associated(buf%gpu)) then
       do ispinor = 1, size(buf%gpu, 2)
          if (buf%gpu(1, ispinor) /= 0_f_address) call ocl_release_mem_object(buf%gpu(1, ispinor))
          if (buf%gpu(2, ispinor) /= 0_f_address) call ocl_release_mem_object(buf%gpu(2, ispinor))
       end do
       call f_free_ptr(buf%gpu)
    end if
  end subroutine mem_buffer_free
  
  subroutine mem_buffer_pin(buf, context, queue, lr)
    use locregs
    use liborbs_errors
    use dynamic_memory
    implicit none
    type(wvf_mem_buffer), intent(inout) :: buf
    integer(f_address), intent(in) :: context, queue
    type(locreg_descriptors), intent(in) :: lr

    integer :: ispinor

    if (f_err_raise(context == 0_f_address .or. &
         queue == 0_f_address, "no GPU context", &
         err_id = LIBORBS_OPERATION_ERROR())) return

    buf%gpu = f_malloc_ptr((/ 2, size(buf%mem, 2) /), id = "buf%gpu")
    do ispinor = 1, size(buf%mem, 2)
       call ocl_pin_read_buffer(context, queue, lr%wfd%nvctr_c*8, &
            buf%mem(1, ispinor), buf%gpu(1, ispinor))
       if (lr%wfd%nvctr_f > 0) &
            call ocl_pin_read_buffer(context, queue, 7*lr%wfd%nvctr_f*8, &
            buf%mem(1 + lr%wfd%nvctr_c, ispinor), buf%gpu(2, ispinor))
    end do
  end subroutine mem_buffer_pin

  function wvf_manager_new(nspinor) result(manager)
    implicit none
    integer, intent(in), optional :: nspinor

    type(wvf_manager) :: manager

    !call nullify_wvf_manager(manager) ! to make this function pure 

    if (present(nspinor)) manager%nspinor = nspinor
  end function wvf_manager_new

  function wvf_manager_llr(llr, nspinor, ncplx) result(manager)
    use locregs
    use liborbs_workarrays
    use at_domain
    implicit none
    type(locreg_descriptors), dimension(:), intent(in) :: llr
    integer, intent(in) :: nspinor
    integer, intent(in), optional :: ncplx

    type(wvf_manager) :: manager
    integer :: ilr, ncplx_
    logical :: identical

    manager%nspinor = nspinor
    ncplx_ = 1
    if (present(ncplx)) ncplx_ = ncplx
    identical = .true.
    do ilr = 1, size(llr)
       manager%max_daub = max(manager%max_daub, array_dim(llr(ilr)))
       manager%max_mesh = max(manager%max_mesh, llr(ilr)%mesh%ndim)
       if (any(domain_periodic_dims(llr(ilr)%mesh%dom) .neqv. &
            domain_periodic_dims(llr(1)%mesh%dom))) identical = .false.
    end do
    if (identical) then
       allocate(manager%w_locham)
       call initialize_work_arrays_locham(size(llr), llr, manager%w_locham)
       allocate(manager%w_sumrho)
       call initialize_work_arrays_sumrho(size(llr), llr, manager%w_sumrho)
       allocate(manager%w_precond)
       call initialize_work_arrays_precond(llr, ncplx_, manager%w_precond)
       allocate(manager%w_quartic)
       call initialize_work_arrays_quartic(llr, manager%w_quartic)
    end if
    allocate(manager%w_tensp)
    call initialize_work_arrays_tensor_prod(llr, manager%w_tensp)
  end function wvf_manager_llr

  function wvf_manager_nlr(nlr, llr, nspinor, ncplx) result(manager)
    use locregs
    use liborbs_workarrays
    implicit none
    integer, intent(in) :: nlr
    type(locreg_descriptors), dimension(1:nlr), intent(in) :: llr
    integer, intent(in) :: nspinor
    integer, intent(in), optional :: ncplx

    type(wvf_manager) :: manager

    manager = wvf_manager_llr(llr, nspinor, ncplx)
  end function wvf_manager_nlr

  subroutine wvf_manager_attach_gpu(manager, context, queue)
    implicit none
    type(wvf_manager), intent(inout) :: manager
    integer(f_address), intent(in) :: context
    integer(f_address), intent(in), optional :: queue

    manager%gpu_context = context
    if (present(queue)) manager%gpu_queue = queue
  end subroutine wvf_manager_attach_gpu

  subroutine deallocate_mesh(manager)
    use dynamic_memory
    implicit none
    type(wvf_manager), intent(inout) :: manager
    
    call mem_buffer_free(manager%w_mesh_1)
    call mem_buffer_free(manager%w_mesh_2)
    call mem_buffer_free(manager%w_mesh_3)
    call mem_buffer_free(manager%w_mesh_4)
    call mem_buffer_free(manager%w_mesh_5)
    manager%i_mesh = 0
  end subroutine deallocate_mesh

  subroutine deallocate_daub(manager)
    use dynamic_memory
    implicit none
    type(wvf_manager), intent(inout) :: manager
    
    call mem_buffer_free(manager%w_daub_1)
    call mem_buffer_free(manager%w_daub_2)
    call mem_buffer_free(manager%w_daub_3)
    call mem_buffer_free(manager%w_daub_4)
    call mem_buffer_free(manager%w_daub_5)
    manager%i_daub = 0
  end subroutine deallocate_daub

  subroutine wvf_deallocate_manager(manager)
    use liborbs_workarrays
    implicit none
    type(wvf_manager), intent(inout) :: manager

    if (associated(manager%w_sumrho)) then
       call deallocate_work_arrays_sumrho(manager%w_sumrho)
       deallocate(manager%w_sumrho)
       nullify(manager%w_sumrho)
    end if
    if (associated(manager%w_locham)) then
       call deallocate_work_arrays_locham(manager%w_locham)
       deallocate(manager%w_locham)
       nullify(manager%w_locham)
    end if
    if (associated(manager%w_precond)) then
       call deallocate_work_arrays_precond(manager%w_precond)
       deallocate(manager%w_precond)
       nullify(manager%w_precond)
    end if
    if (associated(manager%w_quartic)) then
       call deallocate_work_arrays_quartic(manager%w_quartic)
       deallocate(manager%w_quartic)
       nullify(manager%w_quartic)
    end if
    if (associated(manager%w_tensp)) then
       call deallocate_work_arrays_tensor_prod(manager%w_tensp)
       deallocate(manager%w_tensp)
       nullify(manager%w_tensp)
    end if
    call deallocate_mesh(manager)
    call deallocate_daub(manager)
  end subroutine wvf_deallocate_manager

  subroutine release_all(manager)
    implicit none
    type(wvf_manager), intent(inout) :: manager

    manager%i_mesh = 0
    manager%i_daub = 0
  end subroutine release_all

  subroutine release_daub(manager, view)
    use liborbs_errors
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(wvf_daub_view), intent(inout) :: view

    logical :: ok

    ok = .false.
    select case (manager%i_daub)
    case (0)
       ok = .false.
    case (1)
       ok = associated(view%c%mem, target = manager%w_daub_1%mem)
    case (2)
       ok = associated(view%c%mem, target = manager%w_daub_2%mem)
    case (3)
       ok = associated(view%c%mem, target = manager%w_daub_3%mem)
    case (4)
       ok = associated(view%c%mem, target = manager%w_daub_4%mem)
    case (5)
       ok = associated(view%c%mem, target = manager%w_daub_5%mem)
    end select
    if (f_err_raise(.not. ok, "daub view not associated", &
         err_id = LIBORBS_LOCREG_ERROR())) return

    view%c = wvf_mem_buffer()
    manager%i_daub = manager%i_daub - 1
  end subroutine release_daub

  subroutine release_mesh(manager, view)
    use liborbs_errors
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(wvf_mesh_view), intent(inout) :: view

    logical :: ok

    ok = .false.
    select case (manager%i_mesh)
    case (0)
       ok = .false.
    case (1)
       ok = associated(view%c%mem, target = manager%w_mesh_1%mem)
    case (2)
       ok = associated(view%c%mem, target = manager%w_mesh_2%mem)
    case (3)
       ok = associated(view%c%mem, target = manager%w_mesh_3%mem)
    case (4)
       ok = associated(view%c%mem, target = manager%w_mesh_4%mem)
    case (5)
       ok = associated(view%c%mem, target = manager%w_mesh_5%mem)
    end select
    if (f_err_raise(.not. ok, "mesh view not associated", &
         err_id = LIBORBS_LOCREG_ERROR())) return

    view%c = wvf_mem_buffer()
    manager%i_mesh = manager%i_mesh - 1
  end subroutine release_mesh

  function work_array_locham(manager, lr) result(w)
    use liborbs_workarrays
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(locreg_descriptors), intent(in) :: lr

    type(workarr_locham), pointer :: w

    if (.not. associated(manager%w_locham)) then
       allocate(manager%w_locham)
       call initialize_work_arrays_locham(lr, manager%nspinor, manager%w_locham)
    else
       call reset_work_arrays_locham(manager%w_locham, lr, manager%nspinor)
    end if
    w => manager%w_locham
  end function work_array_locham

  function work_array_sumrho(manager, lr) result(w)
    use liborbs_workarrays
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(locreg_descriptors), intent(in) :: lr

    type(workarr_sumrho), pointer :: w

    if (.not. associated(manager%w_sumrho)) then
       allocate(manager%w_sumrho)
       call initialize_work_arrays_sumrho(lr, manager%w_sumrho)
    else
       call reset_work_arrays_sumrho(manager%w_sumrho, lr)
    end if
    w => manager%w_sumrho
  end function work_array_sumrho

  function work_array_precond(manager, lr, ncplx) result(w)
    use liborbs_workarrays
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(locreg_descriptors), intent(in) :: lr
    integer, intent(in) :: ncplx

    type(workarr_precond), pointer :: w

    if (.not. associated(manager%w_precond)) then
       allocate(manager%w_precond)
       call initialize_work_arrays_precond(lr, ncplx, manager%w_precond)
    else
       call reset_work_arrays_precond(manager%w_precond, lr, ncplx)
    end if
    w => manager%w_precond
  end function work_array_precond

  function work_array_quartic(manager, lr, with_confpot) result(w)
    use liborbs_workarrays
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(locreg_descriptors), intent(in) :: lr
    logical, intent(in) :: with_confpot

    type(workarr_quartic_convolutions), pointer :: w

    if (.not. associated(manager%w_quartic)) then
       allocate(manager%w_quartic)
       call initialize_work_arrays_quartic(lr, with_confpot, manager%w_quartic)
    else
       call reset_work_arrays_quartic(manager%w_quartic, lr, with_confpot)
    end if
    w => manager%w_quartic
  end function work_array_quartic

  function work_array_tensor_prod(manager, lr, ncplx, maxkbsize, nterms) result(w)
    use liborbs_workarrays
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(locreg_descriptors), intent(in) :: lr
    integer, intent(in) :: ncplx
    integer, intent(in), optional :: maxkbsize, nterms

    type(workarr_tensor_prod), pointer :: w

    if (manager%w_tensp_iterm > 0) then
       w => manager%w_tensp
       return
    end if

    if (.not. associated(manager%w_tensp)) then
       allocate(manager%w_tensp)
       call initialize_work_arrays_tensor_prod(lr, manager%nspinor, ncplx, manager%w_tensp, &
            maxkbsize, nterms)
    else
       call reset_work_arrays_tensor_prod(manager%w_tensp, lr, manager%nspinor, ncplx, &
            maxkbsize, nterms)
    end if
    w => manager%w_tensp
  end function work_array_tensor_prod

  subroutine next_work_array_tensor_prod(manager, isfull)
    implicit none
    type(wvf_manager), intent(inout) :: manager
    logical, intent(out) :: isfull

    manager%w_tensp_iterm = manager%w_tensp_iterm + 1
    isfull = manager%w_tensp_iterm > manager%w_tensp%nterms
    if (isfull) manager%w_tensp_iterm = 1
  end subroutine next_work_array_tensor_prod

  subroutine lr_accumulate_(lr,ncplx,nterm,wx,wy,wz,ncplx_psi,psi)
    use liborbs_precisions
    use locregs
    implicit none
    integer, intent(in) :: ncplx,nterm,ncplx_psi
    type(locreg_descriptors), intent(in) :: lr
    real(wp), dimension(ncplx,lr%mesh_coarse%ndims(1),2,nterm), intent(in) :: wx
    real(wp), dimension(ncplx,lr%mesh_coarse%ndims(2),2,nterm), intent(in) :: wy
    real(wp), dimension(ncplx,lr%mesh_coarse%ndims(3),2,nterm), intent(in) :: wz
    real(wp), dimension(array_dim(lr)*ncplx_psi), intent(inout) :: psi

    call lr_accumulate(lr, ncplx, nterm, wx, wy, wz, ncplx_psi, psi)
  end subroutine lr_accumulate_

  subroutine release_work_array_tensor_prod(manager, view)
    use liborbs_workarrays
    use liborbs_precisions
    use dynamic_memory
    use locregs
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(wvf_daub_view), intent(inout) :: view

    integer :: ispinor, ncplx_k, nterm
    real(wp), dimension(:), pointer :: fullpsi, projx

    if (manager%w_tensp_iterm > 1) then
       nterm = manager%w_tensp_iterm - 1
    else
       nterm = manager%w_tensp%nterms
    end if

    ncplx_k = min(2, manager%nspinor)
    do ispinor = 1, manager%nspinor, ncplx_k
       fullpsi => f_subptr(view%c%mem(1, ispinor), &
            size = ncplx_k * array_dim(view%lr))
       projx => f_subptr(manager%w_tensp%wprojx(1, (ispinor - 1) / ncplx_k + 1), &
            size = 2 * manager%w_tensp%ncplx * view%lr%mesh_coarse%ndims(1) * nterm)
       call lr_accumulate_(view%lr, manager%w_tensp%ncplx, nterm, &
            projx, manager%w_tensp%wprojy, manager%w_tensp%wprojz, ncplx_k, fullpsi)
    end do

    manager%w_tensp_iterm = 0
  end subroutine release_work_array_tensor_prod

  function work_array_mesh(manager, lr) result(w)
    use dynamic_memory
    use liborbs_errors
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(locreg_descriptors), intent(in) :: lr

    real(wp), dimension(:,:), pointer :: w
    integer(f_long), dimension(2) :: dims

    nullify(w)

    if (manager%max_mesh > 0) then
       if (f_err_raise(lr%mesh%ndim > manager%max_mesh, "too small mesh for lr", &
            err_id = LIBORBS_LOCREG_ERROR())) return
    else if (associated(manager%w_mesh_1%mem)) then
       if (size(manager%w_mesh_1%mem, 1) < lr%mesh%ndim) &
            call deallocate_mesh(manager)
    end if

    manager%i_mesh = manager%i_mesh + 1
    if (f_err_raise(manager%i_mesh > 5, "too many mesh work arrays", &
         err_id = LIBORBS_OPERATION_ERROR())) return

    dims = (/ max(manager%max_mesh, lr%mesh%ndim), int(manager%nspinor, f_long) /)
    select case(manager%i_mesh)
    case (1)
       if (.not. associated(manager%w_mesh_1%mem)) &
            manager%w_mesh_1%mem = f_malloc_ptr(dims, id = "manager%w_mesh_1")
       w => manager%w_mesh_1%mem
    case (2)
       if (.not. associated(manager%w_mesh_2%mem)) &
            manager%w_mesh_2%mem = f_malloc_ptr(dims, id = "manager%w_mesh_2")
       w => manager%w_mesh_2%mem
    case (3)
       if (.not. associated(manager%w_mesh_3%mem)) &
            manager%w_mesh_3%mem = f_malloc_ptr(dims, id = "manager%w_mesh_3")
       w => manager%w_mesh_3%mem
    case (4)
       if (.not. associated(manager%w_mesh_4%mem)) &
            manager%w_mesh_4%mem = f_malloc_ptr(dims, id = "manager%w_mesh_4")
       w => manager%w_mesh_4%mem
    case (5)
       if (.not. associated(manager%w_mesh_5%mem)) &
            manager%w_mesh_5%mem = f_malloc_ptr(dims, id = "manager%w_mesh_5")
       w => manager%w_mesh_5%mem
    end select
  end function work_array_mesh

  function work_array_daub(manager, lr) result(w)
    use dynamic_memory
    use liborbs_errors
    use locregs
    implicit none
    type(wvf_manager), intent(inout) :: manager
    type(locreg_descriptors), intent(in) :: lr

    real(wp), dimension(:,:), pointer :: w
    integer(f_long), dimension(2) :: dims

    nullify(w)

    if (manager%max_daub > 0) then
       if (f_err_raise(array_dim(lr) > manager%max_daub, "too small daub for lr", &
            err_id = LIBORBS_LOCREG_ERROR())) return
    else if (associated(manager%w_daub_1%mem)) then
       if (size(manager%w_daub_1%mem, 1) < array_dim(lr)) &
            call deallocate_daub(manager)
    end if

    manager%i_daub = manager%i_daub + 1
    if (f_err_raise(manager%i_daub > 5, "too many daub work arrays", &
         err_id = LIBORBS_OPERATION_ERROR())) return

    dims = (/ max(manager%max_daub, array_dim(lr)), int(manager%nspinor, f_long) /)
    select case(manager%i_daub)
    case (1)
       if (.not. associated(manager%w_daub_1%mem)) &
            manager%w_daub_1%mem = f_malloc_ptr(dims, id = "manager%w_daub_1")
       w => manager%w_daub_1%mem
    case (2)
       if (.not. associated(manager%w_daub_2%mem)) &
            manager%w_daub_2%mem = f_malloc_ptr(dims, id = "manager%w_daub_2")
       w => manager%w_daub_2%mem
    case (3)
       if (.not. associated(manager%w_daub_3%mem)) &
            manager%w_daub_3%mem = f_malloc_ptr(dims, id = "manager%w_daub_3")
       w => manager%w_daub_3%mem
    case (4)
       if (.not. associated(manager%w_daub_4%mem)) &
            manager%w_daub_4%mem = f_malloc_ptr(dims, id = "manager%w_daub_4")
       w => manager%w_daub_4%mem
    case (5)
       if (.not. associated(manager%w_daub_5%mem)) &
            manager%w_daub_5%mem = f_malloc_ptr(dims, id = "manager%w_daub_5")
       w => manager%w_daub_5%mem
    end select
  end function work_array_daub

  function view_on_daub_glr(manager, glr, lr, psi) result(view)
    use locregs
    use locreg_operations
    use f_utils
    implicit none
    type(wvf_manager), intent(inout), target :: manager
    type(locreg_descriptors), intent(in), target :: glr
    type(locreg_descriptors), intent(in) :: lr
    real(wp), dimension(array_dim(lr), manager%nspinor), intent(in) :: psi

    type(wvf_daub_view) :: view

    view%manager => manager
    view%lr => glr
    view%c%mem => work_array_daub(manager, glr)

    call f_zero(view%c%mem)
    call Lpsi_to_global2(array_dim(lr) * manager%nspinor, &
         array_dim(glr) * manager%nspinor, 1, 1, glr, lr, psi, view%c%mem)
  end function view_on_daub_glr

  function view_on_daub_glr_add(manager, glr, lr, psiadd) result(view)
    use dynamic_memory
    use locregs
    implicit none
    type(wvf_manager), intent(inout), target :: manager
    type(locreg_descriptors), intent(in), target :: glr
    type(locreg_descriptors), intent(in) :: lr
    real(wp), intent(in) :: psiadd

    type(wvf_daub_view) :: view
    real(wp), dimension(:), pointer :: psi

    psi => f_subptr(psiadd, size = array_dim(lr) * manager%nspinor)
    view = view_on_daub_glr(manager, glr, lr, psi)
  end function view_on_daub_glr_add

  function wvf_view_on_daub(manager, lr, psi, psisrc, pin) result(view)
    use locregs
    use dynamic_memory
    use liborbs_workarrays
    implicit none
    type(wvf_manager), intent(inout), target :: manager
    type(locreg_descriptors), intent(in), target :: lr
    real(wp), dimension(array_dim(lr), manager%nspinor), intent(in), target, optional :: psi
    real(wp), dimension(array_dim(lr), manager%nspinor), intent(in), optional :: psisrc
    logical, intent(in), optional :: pin

    type(wvf_daub_view) :: view

    view%manager => manager
    view%lr => lr
    if (present(psi)) then
       view%c%mem => psi
    else
       view%c%mem => work_array_daub(manager, lr)
       if (present(pin)) then
          if (pin) call mem_buffer_pin(view%c, manager%gpu_context, manager%gpu_queue, lr)
       end if
    end if
    if (present(psisrc)) call f_memcpy(src = psisrc, dest = view%c%mem)
  end function wvf_view_on_daub

  function wvf_view_on_mesh(manager, lr, psir) result(view)
    implicit none
    type(wvf_manager), intent(inout), target :: manager
    type(locreg_descriptors), intent(in), target :: lr
    real(wp), dimension(lr%mesh%ndim, manager%nspinor), intent(in), target, optional :: psir

    type(wvf_mesh_view) :: view

    view%manager => manager
    view%lr => lr
    if (present(psir)) then
       view%c%mem => psir
    else
       view%c%mem => work_array_mesh(manager, lr)
    end if
  end function wvf_view_on_mesh

  function to_mesh(view, psir, store) result(out)
    use locregs
    use locreg_operations
    use f_utils
    use at_domain
    implicit none
    type(wvf_daub_view), intent(in) :: view
    real(wp), dimension(view%lr%mesh%ndim, view%manager%nspinor), intent(out), optional :: psir
    logical, intent(in), optional :: store

    type(wvf_mesh_view) :: out
    logical :: store_
    integer :: ispinor
    type(workarr_locham), pointer :: w_locham

    store_ = .false.
    if (present(store)) store_ = store

    out = wvf_view_on_mesh(view%manager, view%lr, psir)
    out%factor = view%factor

    if (.not. present(psir) .and. .not. store_ .and. &
        domain_geocode(view%lr%mesh%dom) == 'F') call f_zero(out%c%mem)

    if (store_) then
       w_locham => work_array_locham(view%manager, view%lr)
       call daub_to_isf_locham(view%manager%nspinor, view%lr, &
            w_locham, view%c%mem, out%c%mem)
    else
       do ispinor = 1, view%manager%nspinor
          call daub_to_isf(view%lr, work_array_sumrho(view%manager, view%lr), &
               view%c%mem(1, ispinor), out%c%mem(1, ispinor))
       end do
    end if
  end function to_mesh

  function to_mesh_1d(view, psir, store) result(out)
    use liborbs_errors
    implicit none
    type(wvf_daub_view), intent(in) :: view
    real(wp), dimension(view%lr%mesh%ndim), intent(out) :: psir
    logical, intent(in), optional :: store

    type(wvf_mesh_view) :: out

    if (f_err_raise(view%manager%nspinor /= 1, &
         "nspinor is greater than 1", err_id = LIBORBS_OPERATION_ERROR())) return
    out = to_mesh(view, psir, store)
  end function to_mesh_1d

  function to_mesh_add(view, psiradd, store) result(out)
    use dynamic_memory
    implicit none
    type(wvf_daub_view), intent(in) :: view
    real(wp), intent(inout) :: psiradd
    logical, intent(in), optional :: store

    type(wvf_mesh_view) :: out
    real(wp), dimension(:), pointer :: psir

    psir => f_subptr(psiradd, size = view%lr%mesh%ndim * view%manager%nspinor)
    out = to_mesh(view, psir, store)
  end function to_mesh_add

  function from_mesh(view, psir) result(out)
    use dynamic_memory
    use locreg_operations
    implicit none
    type(wvf_mesh_view), intent(in) :: view
    real(wp), dimension(view%lr%mesh%ndim, view%manager%nspinor), intent(out), optional :: psir

    type(wvf_mesh_view) :: out

    out = wvf_view_on_mesh(view%manager, view%lr, psir)
    out%factor = view%factor

    call f_memcpy(src = view%c%mem, dest = out%c%mem)
  end function from_mesh

  subroutine ass_to_mesh(mesh, daub)
    implicit none
    type(wvf_mesh_view), intent(out) :: mesh
    type(wvf_daub_view), intent(in) :: daub

    mesh = wvf_view_to_mesh(daub)
  end subroutine ass_to_mesh

  subroutine ass_to_daub(daub, mesh)
    implicit none
    type(wvf_daub_view), intent(out) :: daub
    type(wvf_mesh_view), intent(in) :: mesh


    type(wvf_mesh_view) :: tmp

    tmp = wvf_view_to_mesh(mesh)
    daub = wvf_view_to_daub(tmp)
  end subroutine ass_to_daub

  subroutine mesh_add(mesh, right, ncplx)
    use wrapper_linalg, only: axpy
    use liborbs_errors
    use f_arrays
    implicit none
    type(wvf_mesh_view), intent(inout) :: mesh
    type(wvf_mesh_view), intent(in) :: right
    integer, intent(in), optional :: ncplx

    integer :: ncplx_, i
    type(f_scalar) :: a

    ncplx_ = 1
    if (present(ncplx)) ncplx_ = ncplx

    if (f_err_raise(mesh%lr%mesh%ndim /= right%lr%mesh%ndim, &
         "incompatible lr in addition", err_id = LIBORBS_LOCREG_ERROR())) return
    if (f_err_raise(mesh%lr%mesh%ndim > 2_f_long ** 31, &
         "AXPY overflow", err_id = LIBORBS_OPERATION_ERROR())) return
    if (f_err_raise(ncplx_ > mesh%manager%nspinor, &
         "complex functions with incompatible nspinor", err_id = LIBORBS_OPERATION_ERROR())) return
    if (f_err_raise(ncplx_ == 1 .and. imag(right%factor) /= 0._f_double, &
         "real functions with complex factor", err_id = LIBORBS_OPERATION_ERROR())) return

    if (ncplx_ == 1) then
       call axpy(int(mesh%lr%mesh%ndim * mesh%manager%nspinor), &
            real(right%factor) / real(mesh%factor), &
            right%c%mem(1,1), 1, mesh%c%mem(1,1), 1)
    else
       a = right%factor / mesh%factor
       do i = 1, mesh%manager%nspinor, ncplx_
          call axpy(int(mesh%lr%mesh%ndim), real(a), &
               right%c%mem(1,i), 1, mesh%c%mem(1,i), 1)
          call axpy(int(mesh%lr%mesh%ndim), -imag(a), &
               right%c%mem(1,i+1), 1, mesh%c%mem(1,i), 1)

          call axpy(int(mesh%lr%mesh%ndim), real(a), &
               right%c%mem(1,i+1), 1, mesh%c%mem(1,i+1), 1)
          call axpy(int(mesh%lr%mesh%ndim), imag(a), &
               right%c%mem(1,i), 1, mesh%c%mem(1,i+1), 1)
       end do
    end if
  end subroutine mesh_add

  subroutine daub_add(daub, right, ncplx)
    use wrapper_linalg, only: axpy
    use liborbs_errors
    use locregs
    use f_arrays
    implicit none
    type(wvf_daub_view), intent(inout) :: daub
    type(wvf_daub_view), intent(in) :: right
    integer, intent(in), optional :: ncplx

    integer :: ncplx_, i
    type(f_scalar) :: a

    ncplx_ = 1
    if (present(ncplx)) ncplx_ = ncplx

    if (f_err_raise(array_dim(daub%lr) /= array_dim(right%lr), &
         "incompatible lr in addition", err_id = LIBORBS_LOCREG_ERROR())) return
    if (f_err_raise(array_dim(daub%lr) > 2_f_long ** 31, &
         "AXPY overflow", err_id = LIBORBS_OPERATION_ERROR())) return

    if (f_err_raise(ncplx_ > daub%manager%nspinor, &
         "complex functions with incompatible nspinor", err_id = LIBORBS_OPERATION_ERROR())) return
    if (f_err_raise(ncplx_ == 1 .and. imag(right%factor) /= 0._f_double, &
         "real functions with complex factor", err_id = LIBORBS_OPERATION_ERROR())) return

    if (ncplx_ == 1) then
       call axpy(int(array_dim(daub%lr) * daub%manager%nspinor), &
            real(right%factor) / real(daub%factor), &
            right%c%mem(1,1), 1, daub%c%mem(1,1), 1)
    else
       a = right%factor / daub%factor
       do i = 1, daub%manager%nspinor, ncplx_
          call axpy(int(array_dim(daub%lr)), real(a), &
               right%c%mem(1,i), 1, daub%c%mem(1,i), 1)
          call axpy(int(array_dim(daub%lr)), -imag(a), &
               right%c%mem(1,i+1), 1, daub%c%mem(1,i), 1)

          call axpy(int(array_dim(daub%lr)), real(a), &
               right%c%mem(1,i+1), 1, daub%c%mem(1,i+1), 1)
          call axpy(int(array_dim(daub%lr)), imag(a), &
               right%c%mem(1,i), 1, daub%c%mem(1,i+1), 1)
       end do
    end if
  end subroutine daub_add

  function wvf_potential_operator(view, pot, npot, epot, vpsi) result(out)
    use liborbs_potentials
    implicit none
    type(wvf_mesh_view), intent(in) :: view
    integer, intent(in) :: npot
    real(wp), dimension(view%lr%mesh%ndim, npot), intent(in) :: pot
    real(gp), intent(out) :: epot
    real(wp), dimension(view%lr%mesh%ndim, view%manager%nspinor), intent(out), optional :: vpsi

    type(wvf_mesh_view) :: out

    out = wvf_view_to_mesh(view, vpsi)
    call psir_to_vpsi(npot, view%manager%nspinor, view%lr, pot, out%c%mem, epot)
  end function wvf_potential_operator
  
  subroutine wvf_potential_inplace(view, pot, npot, epot, confdata, noconf, enoconf)
    use liborbs_potentials
    implicit none
    type(wvf_mesh_view), intent(inout) :: view
    integer, intent(in) :: npot
    real(wp), dimension(view%lr%mesh%ndim, npot), intent(in) :: pot
    real(gp), intent(out) :: epot
    type(confpot_data), intent(in), optional :: confdata
    type(wvf_mesh_view), intent(out), optional :: noconf
    real(gp), intent(out), optional :: enoconf

    if (present(noconf) .and. present(confdata)) then
       noconf = wvf_view_to_mesh(view)
       call psir_to_vpsi(npot, view%manager%nspinor, view%lr, &
            pot, view%c%mem, epot, confdata = confdata, vpsir_noconf = noconf%c%mem, econf = enoconf)
    else
       call psir_to_vpsi(npot, view%manager%nspinor, view%lr, &
            pot, view%c%mem, epot, confdata = confdata)
    end if
  end subroutine wvf_potential_inplace

  function mesh_kinetic_operator(view, tpsi, kpt, ekin, stress) result(out)
    use locregs
    use locreg_operations
    use liborbs_errors
    use f_utils
    implicit none
    type(wvf_mesh_view), intent(in) :: view
    real(wp), dimension(array_dim(view%lr), view%manager%nspinor), intent(out), optional :: tpsi
    real(gp), intent(out), optional :: ekin
    real(gp), dimension(6), intent(out), optional :: stress
    real(gp), dimension(3), intent(in), optional :: kpt

    type(wvf_daub_view) :: out
    real(gp), dimension(3) :: kpt_
    real(gp) :: ekin_
    real(wp), dimension(6) :: stress_

    if (f_err_raise(.not. associated(view%manager%w_locham), &
         "mesh view not created with store = .true.", &
         err_id = LIBORBS_OPERATION_ERROR())) return

    kpt_ = 0._gp
    if (present(kpt)) kpt_ = kpt
    out = wvf_view_on_daub(view%manager, view%lr, tpsi)
    if (.not. present(tpsi)) call f_zero(out%c%mem)
    call isf_to_daub_kinetic(view%lr, kpt_(1), kpt_(2), kpt_(3), &
         view%manager%nspinor, view%manager%w_locham, view%c%mem, out%c%mem, ekin_, stress_)
    if (present(ekin)) ekin = ekin_
    if (present(stress)) stress = stress_
  end function mesh_kinetic_operator

  function mesh1d_kinetic_operator(view, tpsi, kpt, ekin, stress) result(out)
    use locregs
    use locreg_operations
    implicit none
    type(wvf_mesh_view), intent(in) :: view
    real(wp), dimension(array_dim(view%lr) * view%manager%nspinor), intent(out) :: tpsi
    real(gp), intent(out), optional :: ekin
    real(gp), dimension(6), intent(out), optional :: stress
    real(gp), dimension(3), intent(in), optional :: kpt

    type(wvf_daub_view) :: out

    out = mesh_kinetic_operator(view, tpsi, kpt, ekin, stress)
  end function mesh1d_kinetic_operator

  function meshadd_kinetic_operator(view, tpsiadd, kpt, ekin, stress) result(out)
    use locregs
    use locreg_operations
    use dynamic_memory
    implicit none
    type(wvf_mesh_view), intent(in) :: view
    real(wp), intent(in) :: tpsiadd
    real(gp), intent(out), optional :: ekin
    real(gp), dimension(6), intent(out), optional :: stress
    real(gp), dimension(3), intent(in), optional :: kpt

    type(wvf_daub_view) :: out
    real(wp), dimension(:), pointer :: tpsi

    tpsi => f_subptr(tpsiadd, size = array_dim(view%lr) * view%manager%nspinor)
    out = mesh_kinetic_operator(view, tpsi, kpt, ekin, stress)
  end function meshadd_kinetic_operator

  function daub_kinetic_operator(view, tpsi, kpt, ekin, stress) result(out)
    use locregs
    use locreg_operations
    use f_utils
    implicit none
    type(wvf_daub_view), intent(in) :: view
    real(wp), dimension(array_dim(view%lr), view%manager%nspinor), intent(out), optional :: tpsi
    real(gp), intent(out), optional :: ekin
    real(gp), dimension(6), intent(out), optional :: stress
    real(gp), dimension(3), intent(in), optional :: kpt

    type(workarr_locham), pointer :: w_locham
    type(wvf_daub_view) :: out
    real(gp), dimension(3) :: kpt_
    real(gp) :: ekin_
    real(wp), dimension(6) :: stress_

    kpt_ = 0._gp
    if (present(kpt)) kpt_ = kpt
    out = wvf_view_on_daub(view%manager, view%lr, tpsi)
    if (.not. present(tpsi)) call f_zero(out%c%mem)
    w_locham => work_array_locham(view%manager, view%lr)
    call psi_to_tpsi(kpt_, view%manager%nspinor, &
         view%lr, view%c%mem, w_locham, out%c%mem, ekin_, stress_)
    if (present(ekin)) ekin = ekin_
    if (present(stress)) stress = stress_
  end function daub_kinetic_operator
  
  function daub1d_kinetic_operator(view, tpsi, kpt, ekin, stress) result(out)
    use locregs
    use locreg_operations
    implicit none
    type(wvf_daub_view), intent(in) :: view
    real(wp), dimension(array_dim(view%lr) * view%manager%nspinor), intent(out) :: tpsi
    real(gp), intent(out), optional :: ekin
    real(gp), dimension(6), intent(out), optional :: stress
    real(gp), dimension(3), intent(in), optional :: kpt

    type(wvf_daub_view) :: out

    out = daub_kinetic_operator(view, tpsi, kpt, ekin, stress)
  end function daub1d_kinetic_operator

  function wvf_kinetic_operator_dir(view, tpsix, tpsiy, tpsiz) result(out)
    use locregs
    use liborbs_errors
    use at_domain, only: domain_geocode
    use f_utils
    implicit none
    type(wvf_daub_view), intent(in) :: view
    real(wp), dimension(array_dim(view%lr) * view%manager%nspinor), intent(out), target, optional :: tpsix, tpsiy, tpsiz

    type(wvf_daub_view), dimension(3) :: out
    type(workarr_locham), pointer :: w_locham
    real(wp), dimension(0:3), parameter :: scal = (/ 1._wp, 1._wp, 1._wp, 1._wp /)
    integer :: ispinor, idir
    real(gp) :: ekino

    if (f_err_raise(domain_geocode(view%lr%mesh%dom) /= "F", &
         "wvf_kinetic_operator_dir() only implemented for free BC", err_id = LIBORBS_LOCREG_ERROR())) return

    out(1) = wvf_view_on_daub(view%manager, view%lr, tpsix)
    out(2) = wvf_view_on_daub(view%manager, view%lr, tpsiy)
    out(3) = wvf_view_on_daub(view%manager, view%lr, tpsiz)
    w_locham => work_array_locham(view%manager, view%lr)

    call ensure_locreg_bounds(view%lr)

    do ispinor = 1, view%manager%nspinor
       call decompress_forstandard(view%lr%wfd, view%lr%nboxc, view%lr%nboxf, &
            w_locham%x_c(1, ispinor), w_locham%x_f(1, ispinor), view%c%mem(1, ispinor), scal, &
            w_locham%x_f1(1, ispinor), w_locham%x_f2(1, ispinor), w_locham%x_f3(1, ispinor))

       do idir = 1, 3
          call f_zero(w_locham%y_c(1, ispinor), size = size(w_locham%y_c, 1))
          call f_zero(w_locham%y_f(1, ispinor), size = size(w_locham%y_f, 1))
          call ConvolkineticT(view%lr%mesh_coarse%ndims(1)-1, &
               view%lr%mesh_coarse%ndims(2)-1, view%lr%mesh_coarse%ndims(3)-1, &
               view%lr%nboxf(1,1), view%lr%nboxf(2,1), &
               view%lr%nboxf(1,2), view%lr%nboxf(2,2), &
               view%lr%nboxf(1,3), view%lr%nboxf(2,3), &
               view%lr%mesh_coarse%hgrids(1), view%lr%mesh_coarse%hgrids(2), view%lr%mesh_coarse%hgrids(3), &
               view%lr%bounds%kb%ibyz_c, view%lr%bounds%kb%ibxz_c, view%lr%bounds%kb%ibxy_c, &
               view%lr%bounds%kb%ibyz_f, view%lr%bounds%kb%ibxz_f, view%lr%bounds%kb%ibxy_f, &
               w_locham%x_c(1, ispinor), w_locham%x_f(1, ispinor), &
               w_locham%y_c(1, ispinor), w_locham%y_f(1, ispinor), ekino, &
               w_locham%x_f1(1, ispinor), w_locham%x_f2(1, ispinor), w_locham%x_f3(1, ispinor), 10**(3-idir))

          call compress_forstandard(view%lr%wfd, view%lr%nboxc, view%lr%nboxf, out(idir)%c%mem(1, ispinor), &
               w_locham%y_c(1, ispinor), w_locham%y_f(1, ispinor), scal)
       end do
    end do
  end function wvf_kinetic_operator_dir

  function wvf_view_to_daub(view, psi) result(out)
    use locregs
    use f_utils
    implicit none
    type(wvf_mesh_view), intent(inout) :: view
    real(wp), dimension(array_dim(view%lr), view%manager%nspinor), intent(inout), optional :: psi

    type(wvf_daub_view) :: out
    integer :: ispinor

    out = wvf_view_on_daub(view%manager, view%lr, psi)
    if (.not. present(psi)) call f_zero(out%c%mem)
    do ispinor = 1, view%manager%nspinor
       call isf_to_daub(view%lr, work_array_sumrho(view%manager, view%lr), &
            view%c%mem(1, ispinor), out%c%mem(1, ispinor))
    end do
  end function wvf_view_to_daub

  function alpha_mesh(alpha, mesh) result(out)
    use f_arrays
    implicit none
    type(wvf_mesh_view), intent(in) :: mesh
    real(wp), intent(in) :: alpha

    type(wvf_mesh_view) :: out

    out = mesh
    out%factor = out%factor * alpha
  end function alpha_mesh

  function scalar_mesh(alpha, mesh) result(out)
    use f_arrays
    implicit none
    type(wvf_mesh_view), intent(in) :: mesh
    type(f_scalar), intent(in) :: alpha

    type(wvf_mesh_view) :: out

    out = mesh
    out%factor = out%factor * alpha
  end function scalar_mesh

  function alpha_daub(alpha, daub) result(out)
    use f_arrays
    implicit none
    type(wvf_daub_view), intent(in) :: daub
    real(wp), intent(in) :: alpha

    type(wvf_daub_view) :: out

    out = daub
    out%factor = out%factor * alpha
  end function alpha_daub

  function scalar_daub(alpha, daub) result(out)
    use f_arrays
    implicit none
    type(wvf_daub_view), intent(in) :: daub
    type(f_scalar), intent(in) :: alpha

    type(wvf_daub_view) :: out

    out = daub
    out%factor = out%factor * alpha
  end function scalar_daub

  function minus_mesh(mesh) result(out)
    use f_arrays
    implicit none
    type(wvf_mesh_view), intent(in) :: mesh

    type(wvf_mesh_view) :: out

    out = mesh
    out%factor = -out%factor
  end function minus_mesh

  function minus_daub(daub) result(out)
    use f_arrays
    implicit none
    type(wvf_daub_view), intent(in) :: daub

    type(wvf_daub_view) :: out

    out = daub
    out%factor = -out%factor
  end function minus_daub

  subroutine wvf_precondition(view, ncong, eval, eval_zero, scpr2, kpt, ncplx, confdata)
    use liborbs_precisions
    use liborbs_potentials
    use locregs
    use dynamic_memory
    implicit none
    type(wvf_daub_view), intent(inout) :: view
    integer, intent(in) :: ncong
    real(gp), intent(in) :: eval, eval_zero
    real(wp), intent(out) :: scpr2
    real(gp), dimension(3), intent(in), optional :: kpt
    integer, intent(in), optional :: ncplx
    type(confpot_data), intent(in), optional :: confdata

    type(confpot_data) :: confdata_
    real(gp), dimension(3) :: kpt_
    integer :: ncplx_, inds
    real(wp) :: scpr
    type(workarr_precond), pointer :: w
    type(workarr_quartic_convolutions), pointer :: q
    type(workarr_quartic_convolutions), target :: q_fake
    real(wp), dimension(:), pointer :: sub_psi

    if (present(confdata)) then
       confdata_ = confdata
    else
       call nullify_confpot_data(confdata_)
    end if

    if (present(kpt)) then
       kpt_ = kpt
    else
       kpt_ = 0._gp
    end if

    if (present(ncplx)) then
       ncplx_ = ncplx
    else
       ncplx_ = 1
    end if

    w => work_array_precond(view%manager, view%lr, ncplx_)
    q => q_fake
    if (present(confdata)) then
       if (confdata%prefac > 0.0_gp .or. confdata%potorder > 0) &
            q => work_array_quartic(view%manager, view%lr, .true.)
    end if

    scpr2 = 0._wp
    do inds = 1, view%manager%nspinor, ncplx_
       sub_psi => f_subptr(view%c%mem(1,inds), size = ncplx_ * array_dim(view%lr))
       call precondition_ket(ncong, confdata_, ncplx_, kpt_, &
            view%lr, eval, eval_zero, sub_psi, scpr, w, q)
       scpr2 = scpr2 + scpr ** 2
    end do
  end subroutine wvf_precondition
  
  subroutine wvf_precondition_ocl(view, ncong, eval, eval_zero, scpr2, &
       ekin, b, ocl_sumrho, ocl_precond, p_addr, h_addr, offset, kpt, ncplx)
    use liborbs_precisions
    use locregs
    use locreg_operations
    use dynamic_memory
    use wrapper_linalg
    implicit none
    type(wvf_daub_view), intent(inout) :: view
    integer, intent(in) :: ncong, offset
    real(gp), intent(in) :: eval, eval_zero
    real(wp), intent(out) :: scpr2
    real(gp), dimension(view%manager%nspinor), intent(out) :: ekin
    type(wvf_daub_view), intent(inout) :: b
    type(workarrays_ocl_sumrho), dimension(2), intent(inout) :: ocl_sumrho
    type(workarrays_ocl_precond), dimension(2), intent(inout) :: ocl_precond
    integer(f_address), intent(in) :: h_addr
    integer(f_address), dimension(2, view%manager%nspinor), intent(in) :: p_addr
    real(gp), dimension(3), intent(in), optional :: kpt
    integer, intent(in), optional :: ncplx

    real(gp), dimension(3) :: kpt_
    integer :: ncplx_, inds
    real(gp) :: cprecr
    type(workarr_precond), pointer :: w
    real(wp), dimension(:), pointer :: sub_psi

    if (present(kpt)) then
       kpt_ = kpt
    else
       kpt_ = 0._gp
    end if

    if (present(ncplx)) then
       ncplx_ = ncplx
    else
       ncplx_ = 1
    end if

    w => work_array_precond(view%manager, view%lr, ncplx_)

    scpr2 = 0._wp
    do inds = 1, view%manager%nspinor, ncplx_
       sub_psi => f_subptr(view%c%mem(1,inds), size = ncplx_ * array_dim(view%lr))

       scpr2 = scpr2 + nrm2(ncplx_ * array_dim(view%lr), sub_psi(1), 1) ** 2

       call cprecr_from_eval(view%lr%mesh_coarse, eval_zero, eval, cprecr)
       call precondition_residue_ocl(view%lr, view%manager%gpu_queue, kpt_, &
            sub_psi, ncplx_, ncong, cprecr, ekin(inds), w, b%c%mem, &
            ocl_sumrho, ocl_precond, p_addr, h_addr, offset)
    end do
  end subroutine wvf_precondition_ocl
  
  subroutine wvf_add_noise(view, factor, id)
    use liborbs_precisions
    use numerics, only: twopi
    use locregs
    use dynamic_memory
    implicit none
    type(wvf_daub_view), intent(inout) :: view
    real(wp), intent(in), optional :: factor
    integer, intent(in), optional :: id
    !local variables
    real(wp), dimension(:,:,:,:), allocatable :: ww
    integer :: i,j
    real(wp) :: rfreq,scal,scal2
    real(wp), dimension(:), pointer :: sub_psi

    j = 1
    if (present(id)) j = id

    !pseudo-random frequency (from 0 to 10*2pi)
    rfreq = 10.0_wp*twopi
    if (present(factor)) rfreq = rfreq * factor
    scal = 0.5_wp ** (1._wp / 3._wp)
    scal2 = 0.15_wp ** (1._wp / 3._wp)
    ww = f_malloc((/ 1, maxval(view%lr%mesh_coarse%ndims), 2, 1 /), id = 'ww')
    do i = 1, size(ww, 2), 1
       ww(1,i,1,1) = scal * sin(rfreq*(i+real(j,wp)))
       ww(1,i,2,1) = scal2 * sin(rfreq*(i+real(j,wp)))
    end do
    do i = 1, view%manager%nspinor
       sub_psi => f_subptr(view%c%mem(1, i), size = array_dim(view%lr))
       call lr_accumulate(view%lr, 1, 1, ww, ww, ww, 1, sub_psi)
    end do
    call f_free(ww)
  end subroutine wvf_add_noise

  function daub_nrm(view) result(wnrm)
    use liborbs_precisions
    use locregs
    use wrapper_linalg
    use f_arrays
    implicit none
    type(wvf_daub_view), intent(in) :: view
    real(wp) :: wnrm

    wnrm = abs(view%factor) * nrm2(array_dim(view%lr) * view%manager%nspinor, view%c%mem(1,1), 1)
  end function daub_nrm

  function mesh_nrm(view) result(wnrm)
    use liborbs_precisions
    use locregs
    use liborbs_errors
    use wrapper_linalg
    use f_arrays
    implicit none
    type(wvf_mesh_view), intent(in) :: view
    real(wp) :: wnrm

    wnrm = 0._wp
    if (f_err_raise(view%lr%mesh%ndim > 2_f_long ** 31, &
         "NRM2 overflow", err_id = LIBORBS_OPERATION_ERROR())) return
    wnrm = abs(view%factor) * nrm2(int(view%lr%mesh%ndim) * view%manager%nspinor, view%c%mem(1,1), 1)
  end function mesh_nrm

  subroutine daub_normalize(view, oldnrm)
    use liborbs_precisions
    use locregs
    use wrapper_linalg
    implicit none
    type(wvf_daub_view), intent(inout) :: view
    real(wp), intent(out), optional :: oldnrm

    real(wp) :: nrm

    nrm = daub_nrm(view)
    call vscal(array_dim(view%lr) * view%manager%nspinor, 1.0_wp / nrm, view%c%mem(1,1), 1)
    if (present(oldnrm)) oldnrm = nrm
  end subroutine daub_normalize

  subroutine mesh_normalize(view, oldnrm)
    use liborbs_precisions
    use liborbs_errors
    use locregs
    use wrapper_linalg
    implicit none
    type(wvf_mesh_view), intent(inout) :: view
    real(wp), intent(out), optional :: oldnrm

    real(wp) :: nrm

    nrm = mesh_nrm(view)
    if (f_err_raise(view%lr%mesh%ndim > 2_f_long ** 31, &
         "VSCAL overflow", err_id = LIBORBS_OPERATION_ERROR())) return
    call vscal(int(view%lr%mesh%ndim * view%manager%nspinor), 1.0_wp / nrm, view%c%mem(1,1), 1)
    if (present(oldnrm)) oldnrm = nrm
  end subroutine mesh_normalize

  subroutine daub_accumulate_gaussian(view, sigma, rxyz, &
       factors, lxyz, kpt, gau_cut)
    use liborbs_precisions
    use liborbs_errors
    use liborbs_workarrays
    use locregs
    use f_arrays
    use at_domain, only: domain_periodic_dims
    use dynamic_memory
    use wrapper_linalg
    implicit none
    type(wvf_daub_view), intent(inout) :: view
    type(f_scalar), intent(in) :: sigma
    real(gp), dimension(3), intent(in) :: rxyz
    type(f_scalar), dimension(view%manager%nspinor / min(2, view%manager%nspinor)), intent(in), target, optional :: factors
    integer, dimension(3), intent(in), target, optional :: lxyz
    real(gp), dimension(3), intent(in), optional :: kpt
    real(gp), intent(in), optional :: gau_cut

    real(gp) :: kx, ky, kz, cut
    integer :: ncplx_g, ncplx_k, ispinor
    type(f_scalar), dimension(1), target :: one
    type(f_scalar), dimension(:), pointer :: factors_
    integer, dimension(3), target :: zeros
    integer, dimension(:), pointer :: lxyz_
    logical, dimension(3) :: peri
    logical :: isfull
    integer :: ml1,ml2,ml3,mu1,mu2,mu3
    integer, dimension(3) :: sh
    type(workarr_tensor_prod), pointer :: wpr

    kx = 0._gp
    if (present(kpt)) kx = kpt(1)
    ky = 0._gp
    if (present(kpt)) ky = kpt(2)
    kz = 0._gp
    if (present(kpt)) kz = kpt(3)

    cut = 10._gp
    if (present(gau_cut)) cut = gau_cut

    one = 1._f_double
    if (present(factors)) then
       factors_ => factors
    else
       factors_ => one
    end if
    if (present(lxyz)) then
       lxyz_ => lxyz
    else
       zeros = 0
       lxyz_ => zeros
    end if

    ncplx_k = min(2, view%manager%nspinor)
    if (kx /= 0._gp .or. ky /= 0._gp .or. kz /= 0._gp) then
       if (f_err_raise(ncplx_k < 2, "wrong spinor size for complex k point.", &
            err_id = LIBORBS_OPERATION_ERROR())) return
    end if

    ncplx_g = max(ncplx_k, 1)
    if (imag(sigma) /= 0._f_double) ncplx_g = 2

    peri = domain_periodic_dims(view%lr%mesh%dom)

    wpr => work_array_tensor_prod(view%manager, view%lr, ncplx_g, maxkbsize = 2048)
    if (view%manager%w_tensp_iterm == 0) view%manager%w_tensp_iterm = 1

    sh = ncplx_g * view%lr%mesh_coarse%ndims * 2
    sh = sh * (view%manager%w_tensp_iterm - 1) + 1

    do ispinor = 1, view%manager%nspinor / ncplx_k
       call gauss_to_daub_k(view%lr%mesh_coarse%hgrids(1), &
            kx*view%lr%mesh_coarse%hgrids(1), &
            ncplx_g, ncplx_k, factors_(min(ispinor, size(factors_))), rxyz(1), sigma, &
            lxyz_(1), view%lr%nboxc(1,1), view%lr%mesh_coarse%ndims(1)-1, &
            ml1, mu1, wpr%wprojx(sh(1), ispinor), &
            wpr%work, size(wpr%work, 1)-1, peri(1), cut)
    end do
    call gauss_to_daub_k(view%lr%mesh_coarse%hgrids(2), &
         ky*view%lr%mesh_coarse%hgrids(2), &
         ncplx_g, ncplx_k, one, rxyz(2), sigma, &
         lxyz_(2), view%lr%nboxc(1,2), view%lr%mesh_coarse%ndims(2)-1, &
         ml2, mu2, wpr%wprojy(sh(2)), &
         wpr%work, size(wpr%work, 1)-1, peri(2), cut)
    call gauss_to_daub_k(view%lr%mesh_coarse%hgrids(3), &
         kz*view%lr%mesh_coarse%hgrids(3), &
         ncplx_g, ncplx_k, one, rxyz(3), sigma, &
         lxyz_(3), view%lr%nboxc(1,3), view%lr%mesh_coarse%ndims(3)-1, &
         ml3, mu3, wpr%wprojz(sh(3)), &
         wpr%work, size(wpr%work, 1)-1, peri(3), cut)
    
    call next_work_array_tensor_prod(view%manager, isfull)
    if (isfull) then
       call release_work_array_tensor_prod(view%manager, view)
    end if
  end subroutine daub_accumulate_gaussian

  subroutine wvf_finish_accumulate_gaussians(view)
    use dynamic_memory
    use locregs
    implicit none
    type(wvf_daub_view), intent(inout) :: view

    call release_work_array_tensor_prod(view%manager, view)
  end subroutine wvf_finish_accumulate_gaussians

  subroutine mesh_accumulate_gaussian(view, sigma, rxyz, &
       nterms, factors, lxyz, gau_cut, mp_isf_order)
    use liborbs_precisions
    use liborbs_errors
    use numerics
    use box
    use locregs
    use f_arrays
    use multipole_preserving
    implicit none
    type(wvf_mesh_view), intent(inout) :: view
    type(f_scalar), intent(in) :: sigma
    integer, intent(in) :: nterms
    real(gp), dimension(3), intent(in) :: rxyz
    type(f_scalar), dimension(nterms, view%manager%nspinor / min(2, view%manager%nspinor)), intent(in) :: factors
    integer, dimension(3, nterms), intent(in) :: lxyz
    real(gp), intent(in), optional :: gau_cut
    integer, intent(in), optional :: mp_isf_order

    real(gp) :: cut
    integer :: mp_isf, ispinor, icomp
    integer :: ithread, nthread
    integer, dimension(2,3) :: nbox
    real(gp), dimension(3) :: oxyz
    type(f_scalar) :: expo
    type(box_iterator) :: boxit
    real(gp) :: gaussian_radial_value, prefactor
    !$ integer, external :: omp_get_thread_num,omp_get_num_threads

    cut = 10._gp * abs(sigma)
    if (present(gau_cut)) cut = gau_cut

    mp_isf = 0
    if (present(mp_isf_order)) mp_isf = mp_isf_order

    oxyz = rxyz - [cell_r(view%lr%mesh, view%lr%nboxi(1,1) + 1, 1), &
         & cell_r(view%lr%mesh, view%lr%nboxi(1,2) + 1, 2), &
         & cell_r(view%lr%mesh, view%lr%nboxi(1,3) + 1, 3)]

    expo = 0.5_gp / sigma / sigma
    prefactor = sqrt(view%lr%mesh%volume_element)

    !first calculate the box that the gaussian would have without MP treatment
    nbox = box_nbox_from_cutoff(view%lr%mesh, oxyz + view%lr%bit%oxyz, cut)
    !then further enlarge such box in each dimension in the case of separable MP treatment
    if (mp_initialized()) then
       nbox(1,:) = nbox(1,:) - mp_isf
       nbox(2,:) = nbox(2,:) + mp_isf
    end if
    !we might increase the box in each direction for multipole preserving and pass such box to the set_nbox routine
    call box_iter_set_nbox(view%lr%bit, nbox = nbox)
    if (mp_isf > 0) then
       call mp_gaussian_workarrays(nterms, nbox, abs(expo), lxyz, &
            oxyz, view%lr%mesh%hgrids)
    end if

    do icomp = 1, view%manager%nspinor / min(2, view%manager%nspinor)
       ithread=0
       nthread=1
       boxit = view%lr%bit
       !$omp parallel default(shared) &
       !$omp private(ithread) &
       !$omp firstprivate(boxit)
       !$ ithread=omp_get_thread_num()
       !$ nthread=omp_get_num_threads()
       call box_iter_split(boxit, nthread, ithread)
       do while(box_next_point(boxit))
          ispinor = icomp * min(2, view%manager%nspinor)
          view%c%mem(boxit%ind, ispinor) = &
               view%c%mem(boxit%ind, ispinor) + &
               prefactor * gaussian_radial_value(expo, oxyz, &
               nterms, factors(1, icomp), lxyz, boxit, mp_isf)
       end do
       !$omp end parallel
    end do

    call box_iter_expand_nbox(view%lr%bit)
  end subroutine mesh_accumulate_gaussian

  function dot_daub(daub1, daub2) result(val)
    use liborbs_errors
    use wrapper_linalg
    use locregs
    use f_arrays
    implicit none
    type(wvf_daub_view), intent(in) :: daub1, daub2
    real(wp), dimension(daub1%manager%nspinor) :: val

    type(f_scalar) :: F, D, P

    val = 0._wp
    if (f_err_raise(daub1%manager%nspinor /= daub2%manager%nspinor, &
         "incompatible nspinor value in dot product", &
         err_id = LIBORBS_OPERATION_ERROR())) return
    if (f_err_raise(.not. associated(daub1%lr, target = daub2%lr), &
         "incompatible locreg in dot product", &
         err_id = LIBORBS_OPERATION_ERROR())) return

    F = daub1%factor * daub2%factor
    if (daub1%manager%nspinor == 1) then
       val(1) = real(F) * dot(array_dim(daub1%lr), daub1%c%mem(1,1), 1, daub2%c%mem(1,1), 1)
    else if (daub1%manager%nspinor == 2) then
       D = f_scalar( &
            dot(array_dim(daub1%lr), daub1%c%mem(1,1), 1, daub2%c%mem(1,1), 1) - &
            dot(array_dim(daub1%lr), daub1%c%mem(1,2), 1, daub2%c%mem(1,2), 1), &
            dot(array_dim(daub1%lr), daub1%c%mem(1,1), 1, daub2%c%mem(1,2), 1) + &
            dot(array_dim(daub1%lr), daub1%c%mem(1,2), 1, daub2%c%mem(1,1), 1))
       P = F * D
       val(1) = real(P)
       val(2) = imag(P)
    else if (daub1%manager%nspinor == 4) then
       D = f_scalar( &
            dot(array_dim(daub1%lr), daub1%c%mem(1,1), 1, daub2%c%mem(1,1), 1) - &
            dot(array_dim(daub1%lr), daub1%c%mem(1,2), 1, daub2%c%mem(1,2), 1), &
            dot(array_dim(daub1%lr), daub1%c%mem(1,1), 1, daub2%c%mem(1,2), 1) + &
            dot(array_dim(daub1%lr), daub1%c%mem(1,2), 1, daub2%c%mem(1,1), 1))
       P = F * D
       val(1) = real(P)
       val(2) = imag(P)
       D = f_scalar( &
            dot(array_dim(daub1%lr), daub1%c%mem(1,3), 1, daub2%c%mem(1,3), 1) - &
            dot(array_dim(daub1%lr), daub1%c%mem(1,4), 1, daub2%c%mem(1,4), 1), &
            dot(array_dim(daub1%lr), daub1%c%mem(1,3), 1, daub2%c%mem(1,4), 1) + &
            dot(array_dim(daub1%lr), daub1%c%mem(1,4), 1, daub2%c%mem(1,3), 1))
       P = F * D
       val(3) = real(P)
       val(4) = imag(P)
    else
       call f_err_throw("unknown spinor value.", &
            err_id = LIBORBS_OPERATION_ERROR())
    end if
  end function dot_daub

  function wvf_scal_proj(view, n_p, lr_p, pr, tolr, w_manager) result(scpr)
    use locregs
    use compression
    use f_utils
    implicit none
    type(wvf_daub_view), intent(in) :: view
    integer, intent(in) :: n_p !< number of elements of the projector
    type(locreg_descriptors), intent(in) :: lr_p !< descriptors of projectors
    !> components of the projectors, real and imaginary parts
    real(wp), dimension(array_dim(lr_p), n_p), intent(in) :: pr
    type(wfd_to_wfd), intent(in) :: tolr
    type(wvf_manager), intent(inout) :: w_manager
    !> array of the scalar product between the projectors and the wavefunctions
    real(wp), dimension(view%manager%nspinor, n_p) :: scpr

    !> workspaces for the packing array
    type(wvf_daub_view) :: wpack

    wpack = wvf_view_on_daub(w_manager, lr_p)
    call f_zero(wpack%c%mem)
    !here also the strategy can be considered
    call proj_dot_psi(n_p, lr_p%wfd, pr, &
         view%manager%nspinor, view%lr%wfd, view%c%mem, &
         tolr, wpack%c%mem, scpr)
    call wvf_manager_release(w_manager, wpack)
  end function wvf_scal_proj

  subroutine wvf_non_local_inplace(view, scpr, n_p, lr_p, pr, &
       tolr, w_manager)
    use locregs
    use compression
    implicit none
    type(wvf_daub_view), intent(inout) :: view
    integer, intent(in) :: n_p
    real(wp), dimension(view%manager%nspinor, n_p), intent(in) :: scpr
    type(locreg_descriptors), intent(in) :: lr_p
    real(wp), dimension(array_dim(lr_p), n_p), intent(in) :: pr
    type(wfd_to_wfd), intent(in) :: tolr
    type(wvf_manager), intent(inout) :: w_manager

    ! Work arrays
    type(wvf_daub_view) :: wpack

    wpack = wvf_view_on_daub(w_manager, lr_p)
    call scpr_proj_p_hpsi(n_p, lr_p%wfd, pr, view%manager%nspinor, &
         view%lr%wfd, tolr, wpack%c%mem, scpr, view%c%mem)
    call wvf_manager_release(w_manager, wpack)
  end subroutine wvf_non_local_inplace

end module liborbs_functions
