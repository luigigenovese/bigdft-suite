#ifndef REDUCTION_H
#define REDUCTION_H

#include "liborbs_ocl.h"

void create_reduction_kernels(liborbs_context * context, struct liborbs_kernels * kernels);
void build_reduction_programs(liborbs_context * context);

void clean_reduction_kernels(struct liborbs_kernels * kernels);
void clean_reduction_programs(liborbs_context * context);

/** Initializes every component of a vector to a given value.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vectors.
 *  @param value used to initialize the vector.
 *  @param x buffer to initialize, of size n * sizeof(double).
 */
void set_d(liborbs_command_queue *command_queue,
           cl_uint n, cl_double val, cl_mem x);

/** Computex y = x for vectors.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vectors.
 *  @param x input buffer, vector of size n * sizeof(double).
 *  @param y output buffer, vector of size n * sizeof(double).
 */
void copy_d(liborbs_command_queue *command_queue,
            cl_uint n, cl_mem in, cl_mem out);

/** Computes x = alpha * x for vectors.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vectors.
 *  @param alpha scaling coefficient.
 *  @param x input and output buffer, vector of size ndat * sizeof(double).
 */
void scal_self_d(liborbs_command_queue *command_queue,
                 cl_uint n, cl_double alpha, cl_mem inout);
/** Computes y = alpha * x for vectors.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vectors.
 *  @param alpha scaling coefficient.
 *  @param x input buffer, vector of size n * sizeof(double).
 *  @param y output buffer, vector of size n * sizeof(double).
 */
void scal_d(liborbs_command_queue *command_queue,
            cl_uint n, cl_double alpha, cl_mem in, cl_mem out);

/** Computes y = alpha * x + y for vectors.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vectors.
 *  @param alpha scaling coefficient.
 *  @param x input buffer, vector of size n * sizeof(double).
 *  @param y input and output buffer, vector of size n * sizeof(double).
 */
void axpy_self_d(liborbs_command_queue *command_queue,
                 cl_uint n, cl_double alpha, cl_mem in, cl_mem inout);

/** Computes z = alpha * x + y for vectors.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vectors.
 *  @param alpha scaling coefficient.
 *  @param x input buffer, vector of size n * sizeof(double).
 *  @param y input buffer, vector of size n * sizeof(double).
 *  @param z output buffer, vector of size n * sizeof(double).
 */
void axpy_d(liborbs_command_queue *command_queue,
            cl_uint n, cl_double alpha, cl_mem x, cl_mem y, cl_mem z);

/** Computes z[offset_z:offset_z+n) = alpha * x[offset_x:offset_x+n) + y[offset_y:offset_y+x)..
 *  @param command_queue used to process the data.
 *  @param n number of element to process.
 *  @param alpha scaling coefficient.
 *  @param offset_x offset in the vector x.
 *  @param x input buffer, vector of size n * sizeof(double).
 *  @param offset_y offset in the vector y.
 *  @param y input buffer, vector of size n * sizeof(double).
 *  @param offset_z offset in the vector z.
 *  @param z output buffer, vector of size n * sizeof(double).
 */
void axpy_offset_d(liborbs_command_queue *command_queue,
                   cl_uint n, cl_double alpha,
                   cl_uint offset_x, cl_mem x,
                   cl_uint offset_y, cl_mem y,
                   cl_uint offset_z, cl_mem z);

/** Computes y[offset_y:offset_y+n) = alpha * x[offset_x:offset_x+n) + y[offset_y:offset_y+x)..
 *  @param command_queue used to process the data.
 *  @param n number of element to process.
 *  @param alpha scaling coefficient.
 *  @param offset_x offset in the vector x.
 *  @param x input buffer, vector of size n * sizeof(double).
 *  @param offset_y offset in the vector y.
 *  @param y input buffer, vector of size n * sizeof(double).
 */
void axpy_offset_self_d(liborbs_command_queue *command_queue,
                        cl_uint n, cl_double alpha,
                        cl_uint offset_x, cl_mem x,
                        cl_uint offset_y, cl_mem y);

void gemm_volkov_d(liborbs_command_queue *command_queue,
                   char transa, char transb, cl_uint m, cl_uint n, cl_uint k,
                   cl_double alpha, cl_mem a, cl_uint lda, cl_mem b, cl_uint ldb,
                   cl_double beta, cl_mem c, cl_uint ldc);

/** Computes the multiplication of 2 matrix.
 *  Usage is identical to the BLAS routine DGEMM.
 *  Refer to the BLAS documentation for the meaning of the
 *  different parameters, in respect of the different
 *  transposition and conjugation possible.
 *  @param command_queue used to process the data.
 */
void gemm_d(liborbs_command_queue *command_queue,
            char transa, char transb, cl_uint m, cl_uint n, cl_uint k,
            cl_double alpha, cl_mem a, cl_uint lda, cl_mem b, cl_uint ldb,
            cl_double beta, cl_mem c, cl_uint ldc);

/** Slightly more performing version of gemm_d. @see gemm_d. */
void gemm_block_d(liborbs_command_queue *command_queue,
                  char transa, char transb, cl_uint m, cl_uint n, cl_uint k,
                  cl_double alpha, cl_mem a, cl_uint lda, cl_mem b, cl_uint ldb,
                  cl_double beta, cl_mem c, cl_uint ldc);

/** Computes the multiplication of 2 matrix, knowing the result to be a symmetric matrix.
 *  m and n have to be identical.
 *  Usage is identical to the BLAS routine DGEMM.
 *  Refer to the BLAS documentation for the meaning of the
 *  different parameters, in respect of the different
 *  transposition and conjugation possible.
 *  @param command_queue used to process the data.
 */
void gemmsy_d(liborbs_command_queue *command_queue,
              char transa, char transb, cl_uint m, cl_uint n, cl_uint k,
              cl_double alpha, cl_mem a, cl_uint lda, cl_mem b, cl_uint ldb,
              cl_double beta, cl_mem c, cl_uint ldc);

/** Computes the multiplication of 2 complex matrix.
 *  Usage is identical to the BLAS routine ZGEMM.
 *  Refer to the BLAS documentation for the meaning of the
 *  different parameters, in respect of the different
 *  transposition and conjugation possible.
 *  @param command_queue used to process the data.
 */
void gemm_z(liborbs_command_queue *command_queue,
            char transa, char transb, cl_uint m, cl_uint n, cl_uint k,
            cl_double2 alpha, cl_mem a, cl_uint lda, cl_mem b, cl_uint ldb,
            cl_double2 beta, cl_mem c, cl_uint ldc);

/** Version of asum_d without the second temporary buffer, erasing the input. @see asum_d. */
void asum_self_d(liborbs_command_queue *command_queue,
                 cl_uint ndat, cl_mem in, cl_mem work, cl_double *out);

/** Version of nrm2sq_d without the second temporary buffer, erasing the input. @see nrm2sq_d. */
void nrm2sq_self_d(liborbs_command_queue *command_queue,
                   cl_uint ndat, cl_mem in, cl_mem work, cl_double *out);

/** Computes the sum of the components of a vector.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vector.
 *  @param in buffer of size n * sizeof(double), containing the input data.
 *  @param work1 temporary buffer of size n * sizeof(double).
 *  @param work2 temporary buffer of size n * sizeof(double).
 *  @param out pointer to the resulting sum.
 */
void asum_d(liborbs_command_queue *command_queue,
            cl_uint ndat, cl_mem in, cl_mem work1, cl_mem work2, cl_double *out);

/** Computes the squared norm 2 of a vector.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vector.
 *  @param in buffer of size n * sizeof(double), containing the input data.
 *  @param work1 temporary buffer of size n * sizeof(double).
 *  @param work2 temporary buffer of size n * sizeof(double).
 *  @param out pointer to the resulting sum.
 */
void nrm2sq_d(liborbs_command_queue *command_queue,
              cl_uint ndat, cl_mem in, cl_mem work1, cl_mem work2, cl_double *out);

/** Computes the dot product of 2 vectors.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vectors.
 *  @param x input buffer, vector of size n * sizeof(double).
 *  @param y input buffer, vector of size n * sizeof(double).
 *  @param work1 temporary buffer of size n * sizeof(double).
 *  @param work2 temporary buffer of size n * sizeof(double).
 *  @param out result.
 */
void dot_d(liborbs_command_queue *command_queue,
           cl_uint ndat, cl_mem x, cl_mem y, cl_mem work1, cl_mem work2, cl_double *out);

/** Computes the dot product of 2 vectors asynchronously.
 *  @param command_queue used to process the data.
 *  @param n number of element of the vectors.
 *  @param x input buffer, vector of size n * sizeof(double).
 *  @param y input buffer, vector of size n * sizeof(double).
 *  @param work1 temporary buffer of size n * sizeof(double).
 *  @param work2 temporary buffer of size n * sizeof(double).
 *  @param out result.
 */
void dot_d_async(liborbs_command_queue *command_queue,
                 cl_uint ndat, cl_mem x, cl_mem y, cl_mem work1, cl_mem work2, cl_double *out);

#endif
