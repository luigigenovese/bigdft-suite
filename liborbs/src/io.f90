module liborbs_io
  use dictionaries, only: dictionary
  use locregs, only: locreg_descriptors
  implicit none

  private

  !> Public routines
  type io_descriptor
     character(256) :: path
     type(locreg_descriptors) :: glr
     type(dictionary), pointer :: funcs => null()
  end type io_descriptor

  public :: io_descriptor
  public :: io_descriptor_from_dict, io_deallocate_descriptor
  public :: io_descr_files_exist, io_descr_is_valid, io_descr_invalidate
  public :: io_descr_locrad, io_descr_onwhichatom
  public :: io_descr_phi, io_descr_phig, io_descr_read_phi

  public :: io_descr_funcs_to_dict
  public :: io_descr_add_func

  interface io_descr_phi
     module procedure phi_iorbp, phi_indices
  end interface io_descr_phi

  interface io_descr_phig
     module procedure phig_iorbp
  end interface io_descr_phig

  interface io_descr_read_phi
     module procedure inplace_iorbp_glr
  end interface io_descr_read_phi

  character(len = *), parameter :: LOCREG = "locreg"
  character(len = *), parameter :: KPT = "kpt"
  character(len = *), parameter :: SPIN = "spin"
  character(len = *), parameter :: EIGEN = "eigenvalue"
  character(len = *), parameter :: IND = "index"
  character(len = *), parameter :: FUNC = "function"
  character(len = *), parameter :: BINFUNC = "function (binary)"
  character(len = *), parameter :: ON_WHICH_ATOM = "onwhichatom"
  character(len = *), parameter :: WITH_HEADER = "legacy (header)"
  character(len = *), parameter :: WITHOUT_KEYS = "legacy (nvctr_[c,f])"

contains

  function io_descriptor_from_dict(allwaves, glr, path) result(descr)
    use dictionaries
    use locregs
    implicit none
    type(dictionary), pointer :: allwaves
    type(dictionary), pointer, optional :: glr
    character(len = *), intent(in), optional :: path
    type(io_descriptor) :: descr

    descr%path = "./"
    if (present(path)) then
       descr%path = path
    end if

    nullify(descr%funcs)
    if (associated(allwaves)) then
       if (dict_islist(allwaves)) then
          call dict_copy(descr%funcs, allwaves)
       end if
    end if

    descr%glr = locreg_null()
    if (present(glr)) then
       if (associated(glr)) then
          call lr_set_from_dict(descr%glr, glr, path = trim(descr%path))
       end if
    end if
  end function io_descriptor_from_dict

  subroutine io_deallocate_descriptor(descr)
    use dictionaries
    use locregs
    implicit none
    type(io_descriptor), intent(inout) :: descr

    call dict_free(descr%funcs)
    call deallocate_locreg_descriptors(descr%glr)
  end subroutine io_deallocate_descriptor

  function io_descr_is_valid(descr) result(valid)
    implicit none
    type(io_descriptor), intent(in) :: descr
    logical :: valid

    valid = associated(descr%funcs)
  end function io_descr_is_valid

  subroutine io_descr_invalidate(descr)
    use dictionaries
    implicit none
    type(io_descriptor), intent(inout) :: descr

    call dict_free(descr%funcs)
  end subroutine io_descr_invalidate

  function io_descr_get_filenames(descr, iorb, filenames, nspinor, lbin) result(orb)
    use dictionaries
    use liborbs_errors
    implicit none
    type(io_descriptor), intent(in) :: descr
    integer, intent(in) :: iorb
    character(len = max_field_length), dimension(4), intent(out) :: filenames
    integer, intent(out) :: nspinor
    logical, intent(out) :: lbin
    type(dictionary), pointer :: orb

    type(dictionary), pointer :: onewave
    integer :: i

    orb => io_descr_get_orb(descr, iorb)
    if (.not. associated(orb)) return

    lbin = .false.
    if (FUNC .in. orb) then
       onewave => orb // FUNC
    else if (BINFUNC .in. orb) then
       onewave => orb // BINFUNC
       lbin = .true.
    else
       call f_err_throw(FUNC // " key not in description for iorbp = " // yaml_toa(iorb), &
            err_id = LIBORBS_IO_FORMAT_ERROR())
       nullify(orb)
       return
    end if

    if (.not. dict_islist(onewave)) then
       nspinor = 1
       filenames(1) = onewave
    else
       nspinor = dict_len(onewave)
       do i = 1, nspinor
          filenames(i) = onewave // (i - 1)
       end do
    end if
  end function io_descr_get_filenames

  function io_descr_files_exist(descr) result(ok)
    use dictionaries
    use f_utils
    implicit none
    type(io_descriptor), intent(in) :: descr
    logical :: ok

    type(dictionary), pointer :: onewave
    integer :: iorb, nspinor, i
    character(len = max_field_length), dimension(4) :: fwaves
    logical :: lbin

    ok = .false.
    iorb = 1
    onewave => io_descr_get_filenames(descr, iorb, fwaves, nspinor, lbin)
    loop_wave: do while (associated(onewave))
       do i = 1, nspinor
          call f_file_exists(trim(descr%path) // fwaves(i), exists = ok)
          if (.not. ok) exit loop_wave
       end do
       iorb = iorb + 1
       onewave => io_descr_get_filenames(descr, iorb, fwaves, nspinor, lbin)
    end do loop_wave
  end function io_descr_files_exist

  function io_descr_get_orb(descr, iorbp) result(orb)
    use liborbs_precisions
    use dictionaries
    implicit none
    type(io_descriptor), intent(in) :: descr
    integer, intent(in) :: iorbp
    type(dictionary), pointer :: orb

    nullify(orb)
    if (.not. associated(descr%funcs)) return
    if (iorbp < 1 .or. iorbp > dict_len(descr%funcs)) return
    orb => descr%funcs // (iorbp - 1)
  end function io_descr_get_orb

  function io_descr_locrad(descr, iorbp) result(locrad)
    use f_precisions, only: UNINITIALIZED
    use liborbs_precisions
    use dictionaries
    use locregs
    implicit none
    type(io_descriptor), intent(in) :: descr
    integer, intent(in) :: iorbp
    real(gp) :: locrad
    type(dictionary), pointer :: orb
    type(locreg_descriptors) :: lr

    locrad = UNINITIALIZED(locrad)
    orb => io_descr_get_orb(descr, iorbp)
    if (.not. associated(orb)) return
    locrad = 1d99
    if (LOCREG .notin. orb) return
    call lr_set_from_dict(lr, orb // LOCREG)
    locrad = lr%locrad
    call deallocate_locreg_descriptors(lr)
  end function io_descr_locrad

  function io_descr_onwhichatom(descr, iorbp) result(onwhichatom)
    use f_precisions, only: UNINITIALIZED
    use liborbs_precisions
    use dictionaries
    implicit none
    type(io_descriptor), intent(in) :: descr
    integer, intent(in) :: iorbp
    integer :: onwhichatom
    type(dictionary), pointer :: orb

    onwhichatom = UNINITIALIZED(onwhichatom)
    orb => io_descr_get_orb(descr, iorbp)
    if (.not. associated(orb)) return
    if (ON_WHICH_ATOM .notin. orb) return
    onwhichatom = orb // ON_WHICH_ATOM
  end function io_descr_onwhichatom

  subroutine skip_old_header(unitwf, formatted)
    use liborbs_precisions
    use f_precisions, only: UNINITIALIZED
    implicit none

    integer, intent(in) :: unitwf
    logical, intent(in) :: formatted

    integer :: i, iat, i_stat, nat, n1, n2, n3, ival, ns1, ns2, ns3
    real(wp) :: wval
    real(gp) :: rxyz_(3), hgrids(3), centre(3), rad, fac
    character(len = 256) :: line
    logical :: legacy

    if (formatted) then
       read(unitwf,*,iostat=i_stat) ival, wval
       read(unitwf,*,iostat=i_stat) hgrids(1),hgrids(2),hgrids(3)
       read(unitwf,*,iostat=i_stat) n1, n2, n3
       read(unitwf, "(A)",iostat=i_stat) line
       read(line,*,iostat=i_stat) ns1,ns2,ns3
       legacy = (i_stat /= 0)
       if (.not. legacy) then
          read(unitwf,*,iostat=i_stat) (centre(i),i=1,3),iat,rad,ival,fac
          read(unitwf,*,iostat=i_stat) nat
       else
          ! Legacy format used by the cubic code
          ns1 = 0
          ns2 = 0
          ns3 = 0
          centre = UNINITIALIZED(1._gp)
          rad = UNINITIALIZED(rad)
          iat = UNINITIALIZED(iat)
          ival = UNINITIALIZED(ival)
          fac = UNINITIALIZED(fac)
          read(line,*,iostat=i_stat) nat
       end if
       do iat=1,nat
          read(unitwf,*,iostat=i_stat)
       enddo
       if (legacy) read(unitwf,*,iostat=i_stat) n1, n2
    else
       read(unitwf,iostat=i_stat) ival, wval
       read(unitwf,iostat=i_stat) hgrids(1),hgrids(2),hgrids(3)
       read(unitwf,iostat=i_stat) n1, n2, n3
       read(unitwf,iostat=i_stat) ns1,ns2,ns3
       read(unitwf,iostat=i_stat) (centre(i),i=1,3),iat,rad,ival,fac
       read(unitwf,iostat=i_stat) nat
       do iat=1,nat
          read(unitwf,iostat=i_stat) rxyz_
          if (i_stat /= 0) return
       enddo
    end if
  end subroutine skip_old_header

  function io_descr_locreg(descr, iorbp, gdom, filename, lbin) result(lr)
    use f_precisions, only: UNINITIALIZED
    use liborbs_precisions
    use dictionaries
    use locregs
    use at_domain
    use f_utils
    implicit none
    type(io_descriptor), intent(in) :: descr
    integer, intent(in) :: iorbp
    type(locreg_descriptors) :: lr
    type(dictionary), pointer :: orb
    type(domain), intent(in) :: gdom
    character(len = *), intent(in) :: filename
    logical, intent(in) :: lbin

    integer, dimension(2) :: nvctr
    integer :: unitwf

    lr = locreg_null()
    orb => io_descr_get_orb(descr, iorbp)
    if (.not. associated(orb)) return
    if (LOCREG .notin. orb) then
       call copy_locreg_descriptors(descr%glr, lr)
    else
       call lr_set_from_dict(lr, orb // LOCREG, gdom = gdom, path = trim(descr%path))

       if (WITHOUT_KEYS .in. orb) then
          nvctr = orb // WITHOUT_KEYS
          call init_sized(lr, nvctr(1), nvctr(2))
       else
          call f_open_file(unitwf, file = trim(descr%path)//trim(filename), &
               binary = lbin)
          if (WITH_HEADER .in. orb) then
             call skip_old_header(unitwf, .not. lbin)
          end if
          if (lbin) then
             call read_lr_bin(unitwf, lr)
          else
             call read_lr_txt(unitwf, lr)
          end if
          call f_close(unitwf)
       end if
    end if
  end function io_descr_locreg

  subroutine read_compressed(lbin, filename, lr, phi, has_lr_keys, has_old_header)
    use liborbs_precisions
    use locregs
    use f_utils
    implicit none
    logical, intent(in) :: lbin
    character(len = *), intent(in) :: filename
    logical, intent(in) :: has_lr_keys, has_old_header
    type(locreg_descriptors), intent(in) :: lr
    real(wp), dimension(array_dim(lr)), intent(out) :: phi

    integer :: unitwf

    unitwf = 99
    call f_open_file(unitwf, file = filename, binary = lbin)
    if (has_old_header) call skip_old_header(unitwf, .not. lbin)
    if (lbin) then
       if (has_lr_keys) call read_lr_bin(unitwf)
       call read_array_bin(lr, unitwf, phi)
    else
       if (has_lr_keys) call read_lr_txt(unitwf)
       call read_array_txt(lr, unitwf, phi)
    end if
    call f_close(unitwf)
  end subroutine read_compressed

  subroutine read_grid(lbin, filename, lr, phi, has_lr_keys, has_old_header)
    use liborbs_precisions
    use locregs
    use f_utils
    implicit none
    logical, intent(in) :: lbin
    character(len = *), intent(in) :: filename
    logical, intent(in) :: has_lr_keys, has_old_header
    type(locreg_descriptors), intent(in) :: lr
    real(wp), dimension(:,:,:,:,:,:), intent(out) :: phi

    integer :: unitwf

    unitwf = 99
    call f_open_file(unitwf, file = filename, binary = lbin)
    if (has_old_header) call skip_old_header(unitwf, .not. lbin)
    if (lbin) then
       if (has_lr_keys) call read_lr_bin(unitwf)
       call read_array_bin(lr, unitwf, phi)
    else
       if (has_lr_keys) call read_lr_txt(unitwf)
       call read_array_txt(lr, unitwf, phi)
    end if
    call f_close(unitwf)
  end subroutine read_grid

  function phi_iorbp(descr, iorbp, lr, ispinor, gdom) result(phi)
    use liborbs_precisions
    use liborbs_errors
    use dictionaries
    use locregs
    use f_utils
    use dynamic_memory
    use yaml_strings
    use at_domain
    implicit none
    type(io_descriptor), intent(in) :: descr
    integer, intent(in) :: iorbp
    real(wp), dimension(:), pointer :: phi
    integer, intent(in), optional :: ispinor
    type(locreg_descriptors), intent(out), optional :: lr
    type(domain), intent(in), optional :: gdom

    integer :: i, nspinor
    real(wp), dimension(:), pointer :: subphi
    character(len = max_field_length), dimension(4) :: fwave
    type(locreg_descriptors) :: lr_
    logical :: lbin, ok
    type(dictionary), pointer :: orb

    nullify(phi)
    orb => io_descr_get_filenames(descr, iorbp, fwave, nspinor, lbin)
    if (f_err_raise(.not. associated(orb), "orbital not found, iorb = " // yaml_toa(iorbp), &
         err_id = LIBORBS_IO_FORMAT_ERROR())) return

    if (present(ispinor)) then
       if (f_err_raise(ispinor < 1 .or. ispinor > nspinor, "inconsistent ispinor", &
            err_id = LIBORBS_IO_FORMAT_ERROR())) return
    end if

    if (present(ispinor)) then
       call f_file_exists(file = trim(descr%path)//trim(fwave(ispinor)), exists = ok)
       if (.not. ok) return
    else
       do i = 1, nspinor
          call f_file_exists(file = trim(descr%path)//trim(fwave(i)), exists = ok)
          if (.not. ok) return
       end do
    end if

    lr_ = io_descr_locreg(descr, iorbp, gdom, trim(fwave(1)), lbin)
    if (present(ispinor)) then
       phi = f_malloc_ptr(array_dim(lr_), id = "phi")
       call read_compressed(lbin, trim(descr%path)//trim(fwave(ispinor)), &
            lr_, phi, WITHOUT_KEYS .notin. orb, WITH_HEADER .in. orb)
    else
       phi = f_malloc_ptr(array_dim(lr_) * nspinor, id = "phi")
       do i = 1, nspinor
          subphi => f_subptr(phi((i-1) * array_dim(lr_) + 1), size = array_dim(lr_))
          call read_compressed(lbin, trim(descr%path)//trim(fwave(i)), &
               lr_, subphi, WITHOUT_KEYS .notin. orb, WITH_HEADER .in. orb)
       end do
    end if
    if (present(lr)) then
       lr = lr_
    else
       call deallocate_locreg_descriptors(lr_)
    end if
  end function phi_iorbp

  subroutine inplace_iorbp_glr(descr, phi, eigenvalue, iorbp, glr, lstat, displ, dxyz, psifscf)
    use liborbs_precisions
    use liborbs_errors
    use dictionaries
    use locregs
    use f_utils
    use dynamic_memory
    use yaml_strings
    use at_domain
    use f_precisions, only: UNINITIALIZED
    implicit none
    type(io_descriptor), intent(in) :: descr
    integer, intent(in) :: iorbp
    real(wp), dimension(:), intent(out) :: phi
    real(wp), intent(out) :: eigenvalue
    type(locreg_descriptors), intent(in) :: glr
    logical, intent(out) :: lstat
    real(gp), intent(in), optional :: displ
    real(gp), dimension(3), intent(in), optional :: dxyz
    real(wp), dimension(glr%mesh_fine%ndim), intent(out), optional :: psifscf

    integer :: i, nspinor
    real(wp), dimension(:), pointer :: subphi
    character(len = max_field_length), dimension(4) :: fwave
    type(locreg_descriptors) :: lr_
    logical :: lbin, ok
    type(dictionary), pointer :: orb
    real(wp), dimension(:,:,:,:,:,:), allocatable :: psigold

    lstat = .false.

    eigenvalue = UNINITIALIZED(eigenvalue)
    orb => io_descr_get_filenames(descr, iorbp, fwave, nspinor, lbin)
    if (f_err_raise(.not. associated(orb), "orbital not found, iorb = " // yaml_toa(iorbp), &
         err_id = LIBORBS_IO_FORMAT_ERROR())) return

    do i = 1, nspinor
       call f_file_exists(file = trim(descr%path)//trim(fwave(i)), exists = ok)
       if (.not. ok) return
    end do

    if (f_err_raise(array_dim(glr) * nspinor /= size(phi), &
         "wrong phi dimension, read nspinor = " // yaml_toa(nspinor), &
         err_id = LIBORBS_IO_FORMAT_ERROR())) return

    lr_ = io_descr_locreg(descr, iorbp, glr%mesh%dom, trim(fwave(1)), lbin)
    if (lr_do_reformat(lr_, glr, displ, iorbp == 1)) then
       psigold = f_malloc(grid_dim(lr_), id = 'psigold')
       do i = 1, nspinor
          call read_grid(lbin, trim(descr%path)//trim(fwave(i)), &
               lr_, psigold, WITHOUT_KEYS .notin. orb, WITH_HEADER .in. orb)
          subphi => f_subptr(phi, from = (i - 1) * array_dim(glr) + 1, size = array_dim(glr))
          call lr_reformat(glr, lr_, psigold, subphi, shift = dxyz, &
               force = .true., w_psifscf = psifscf)
       end do
       call f_free(psigold)
    else
       do i = 1, nspinor
          subphi => f_subptr(phi, from = (i - 1) * array_dim(glr) + 1, size = array_dim(glr))
          call read_compressed(lbin, trim(descr%path)//trim(fwave(i)), &
               lr_, subphi, WITHOUT_KEYS .notin. orb, WITH_HEADER .in. orb)
       end do
    end if
    call deallocate_locreg_descriptors(lr_)

    if (EIGEN .in. orb) eigenvalue = orb // EIGEN

    lstat = .true.
  end subroutine inplace_iorbp_glr

  function phi_indices(descr, lr, ikpt, iband, ispin, ispinor) result(phi)
    use liborbs_precisions
    use dictionaries
    use locregs
    implicit none
    type(io_descriptor), intent(in) :: descr
    real(wp), dimension(:), pointer :: phi
    integer, intent(in), optional :: ikpt, iband, ispin, ispinor
    type(locreg_descriptors), intent(out) :: lr

    type(dictionary), pointer :: onewave
    integer :: iorb, ikpt_, icur_kpt, icur_band, iband_, ispinor_
    character(4) :: ispin_
    character(max_field_length) :: icur_spin

    if (.not. associated(descr%funcs)) return

    ikpt_ = 1
    if (present(ikpt)) ikpt_ = ikpt
    ispin_ = "up"
    if (present(ispin)) then
       if (ispin == 2) ispin_ = "down"
    end if
    iband_ = 1
    if (present(iband)) iband_ = iband
    ispinor_ = 1
    if (present(ispinor)) ispinor_ = ispinor

    icur_band = 1
    iorb = 0
    onewave => dict_iter(descr%funcs)
    do while (associated(onewave))
       iorb = iorb + 1

       if (KPT .in. onewave) then
          icur_kpt = onewave // KPT // IND
       else
          icur_kpt = 1
       end if
       if (icur_kpt /= ikpt_) then
          onewave => dict_next(onewave)
          cycle
       end if

       if (SPIN .in. onewave) then
          icur_spin = onewave // SPIN
       else
          icur_spin = "up"
       end if
       if (trim(icur_spin) /= trim(ispin_)) then
          onewave => dict_next(onewave)
          cycle
       end if

       if (icur_band == iband_) then
          phi => phi_iorbp(descr, iorb, lr, ispinor_)
          return
       end if

       icur_band = icur_band + 1
       onewave => dict_next(onewave)
    end do
  end function phi_indices

  function phig_iorbp(descr, iorbp, ispinor, lr, gdom) result(phig)
    use liborbs_precisions
    use liborbs_errors
    use dictionaries
    use locregs
    use f_utils
    use dynamic_memory
    use yaml_strings
    use at_domain
    implicit none
    type(io_descriptor), intent(in) :: descr
    integer, intent(in) :: iorbp, ispinor
    real(wp), dimension(:,:,:,:,:,:), pointer :: phig
    type(locreg_descriptors), intent(out), optional :: lr
    type(domain), intent(in), optional :: gdom

    integer :: nspinor
    character(len = max_field_length), dimension(4) :: fwave
    type(locreg_descriptors) :: lr_
    logical :: lbin, ok
    type(dictionary), pointer :: orb

    nullify(phig)
    orb => io_descr_get_filenames(descr, iorbp, fwave, nspinor, lbin)
    if (f_err_raise(.not. associated(orb), "orbital not found, iorb = " // yaml_toa(iorbp), &
         err_id = LIBORBS_IO_FORMAT_ERROR())) return

    if (f_err_raise(ispinor < 1 .or. ispinor > nspinor, "inconsistent ispinor", &
         err_id = LIBORBS_IO_FORMAT_ERROR())) return

    call f_file_exists(file = trim(descr%path)//trim(fwave(ispinor)), exists = ok)
    if (.not. ok) return

    lr_ = io_descr_locreg(descr, iorbp, gdom, trim(fwave(ispinor)), lbin)

    phig = f_malloc_ptr(grid_dim(lr_), id = 'phig')
    call read_grid(lbin, trim(descr%path)//trim(fwave(ispinor)), &
         lr_, phig, WITHOUT_KEYS .notin. orb, WITH_HEADER .in. orb)

    if (present(lr)) then
       lr = lr_
    else
       call deallocate_locreg_descriptors(lr_)
    end if
  end function phig_iorbp

  function io_descr_funcs_to_dict(descr) result(dict)
    use dictionaries
    implicit none
    type(io_descriptor), intent(in) :: descr
    type(dictionary), pointer :: dict

    call dict_init(dict)
    call dict_copy(dict, descr%funcs)
  end function io_descr_funcs_to_dict

  subroutine io_descr_add_func(descr, filenames, lbin, &
       ikpt, ispin, ilr, iat, phi, eigenvalue, lr, withLegacyHeader)
    use liborbs_precisions
    use locregs
    use dictionaries
    use f_utils
    use yaml_strings
    use dynamic_memory
    use f_precisions, only: UNINITIALIZED
    implicit none
    type(io_descriptor), intent(inout) :: descr
    character(max_field_length), dimension(:), intent(in) :: filenames
    logical, intent(in) :: lbin
    type(locreg_descriptors), intent(in), optional :: lr
    integer, intent(in), optional :: ikpt, ilr, ispin, iat
    real(wp), dimension(:), pointer, optional :: phi
    logical, intent(in), optional :: withLegacyHeader
    real(wp), intent(in), optional :: eigenvalue

    type(dictionary), pointer :: onewave, onelr
    real(wp), dimension(:), pointer :: subphi
    integer :: i, unitwf

    call dict_init(onewave)
    if (size(filenames) == 1) then
       if (lbin) then
          call set(onewave // BINFUNC, filenames(1))
       else
          call set(onewave // FUNC, filenames(1))
       end if
    else
       if (lbin) then
          call set(onewave // BINFUNC, filenames)
       else
          call set(onewave // FUNC, filenames)
       end if
    end if
    if (present(withLegacyHeader)) call set(onewave // WITH_HEADER, withLegacyHeader)

    if (present(lr)) then
       call dict_init(onelr)
       call lr_merge_to_dict(onelr, lr)
       call set(onewave // LOCREG, onelr)
    end if

    if (present(ilr)) call set(onewave // LOCREG // IND, ilr)

    if (present(lr) .and. .not. present(phi)) then
       if (.not. associated(lr%wfd%keyglob)) then
          call set(onewave // WITHOUT_KEYS, (/ lr%wfd%nvctr_c, lr%wfd%nvctr_f /))
       end if
    end if

    if (present(iat)) call set(onewave // ON_WHICH_ATOM, iat)

    if (present(ikpt)) call set(onewave // KPT, "*KPT" // &
         adjustl(yaml_toa(ikpt, fmt = "(I3.3)")))

    if (present(ispin)) then
       if (ispin > 0) then
          call set(onewave // SPIN, "up")
       else if (ispin < 0) then
           call set(onewave // SPIN, "down")
       end if
    end if

    if (present(eigenvalue)) then
       if (eigenvalue /= UNINITIALIZED(eigenvalue)) call set(onewave // EIGEN, eigenvalue)
    end if

    if (.not. associated(descr%funcs)) call dict_init(descr%funcs)
    call add(descr%funcs, onewave)

    if (present(phi) .and. present(lr)) then
       if (associated(phi)) then
          unitwf = 99
          do i = 1, size(filenames)
             subphi => f_subptr(phi, from = (i - 1) * array_dim(lr) + 1, size = array_dim(lr))
             call f_open_file(unitwf, file = trim(descr%path) // filenames(i), binary = lbin)
             if (lbin) then
                call dump_array_bin(lr, unitwf, subphi)
             else
                call dump_array_txt(lr, unitwf, subphi)
             end if
             call f_close(unitwf)
          end do
       end if
    end if
  end subroutine io_descr_add_func

end module liborbs_io
