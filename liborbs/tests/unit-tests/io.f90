program test_io
  use yaml_output
  use f_unittests
  use wrapper_MPI
  use time_profiling

  implicit none

  type(mpi_environment) :: env
  
  call mpiinit()

  env = mpi_environment_comm()  

  call f_lib_initialize()
  call f_timing_category_group('Other', 'for various thingies')

  if (env%iproc == 0) then
     call yaml_sequence_open("Full grid reformatting")
     call run(periodicBC_nohybrid)
     call run(periodicBC_hybrid)
     call run(surfaceBC_hgrid)
     call run(surfaceBC_acell)
     call run(wireBC_hgrid)
     call run(wireBC_acell)
     call run(freeBC_hgrid)
     call run(freeBC_acell)
     call yaml_sequence_close()

     call yaml_sequence_open("Binary file read / write")
     call run(periodicBC_nohybrid_bin)
     call run(periodicBC_hybrid_bin)
     call run(surfaceBC_bin)
     call run(wireBC_bin)
     call run(freeBC_bin)
     call yaml_sequence_close()

     call yaml_sequence_open("Plain text file read / write")
     call run(periodicBC_nohybrid_plain)
     call run(periodicBC_hybrid_plain)
     call run(surfaceBC_plain)
     call run(wireBC_plain)
     call run(freeBC_plain)
     call yaml_sequence_close()

     call yaml_sequence_open("BigDFT wavefunction file")
     call run(read_bigdft_wave)
     call yaml_sequence_close()
  end if

  if (env%iproc == 0) call yaml_sequence_open("Rototranslation reformatting")
  call run(periodicBC_roto, env)
  call run(surfaceBC_roto, env)
  call run(freeBC_roto, env)
  if (env%iproc == 0) call yaml_sequence_close()

  if (env%iproc == 0) then
     call f_lib_finalize()
  else
     call f_lib_finalize_noreport()
  end if

  call release_mpi_environment(env)
  call mpifinalize()

contains

  subroutine check(lr, lr_old, psiold)
    use liborbs_precisions
    use locregs
    use dynamic_memory
    implicit none
    type(locreg_descriptors), intent(in) :: lr, lr_old
    real(wp), dimension(:), intent(in) :: psiold

    real(wp), dimension(:), allocatable :: psi
    real(wp), dimension(:), allocatable :: psifscfold, psifscf
    real(wp), dimension(:,:,:,:,:,:), allocatable :: psigold, psig

    ! No reformatting
    psi = f_malloc(array_dim(lr_old), id = "psi")
    call lr_reformat(lr_old, lr_old, psiold, psi)
    call compare(psi, psiold, "no reformatting", tol = 1.d-12)

    ! Force reformatting
    call lr_reformat(lr_old, lr_old, psiold, psi, force = .true.)
    call compare(psi, psiold, "force reformatting", tol = 9.d-9)
    call f_free(psi)

    ! Reformatting cases
    psi = f_malloc(array_dim(lr), id = "psi")
    call lr_reformat(lr, lr_old, psiold, psi)
    call compare(sum(psi**2) * lr%mesh%volume_element, &
         sum(psiold**2) * lr_old%mesh%volume_element, "no workarray", tol = 6.d-7)

    psifscfold = f_malloc(lr_old%mesh_fine%ndim, id = "psifscfold")
    psifscf = f_malloc(lr%mesh_fine%ndim, id = "psifscf")
    psigold = f_malloc(grid_dim(lr_old), id = "psigold")
    psig = f_malloc(grid_dim(lr), id = "psig")
    call lr_reformat(lr, lr_old, psiold, psi, &
         w_psifscf = psifscf, w_psifscfold = psifscfold, w_psig = psig, w_psigold = psigold)
    call compare(sum(psi**2) * lr%mesh%volume_element, &
         sum(psiold**2) * lr_old%mesh%volume_element, "with workarrays", tol = 6.d-7)
    call f_free(psig)
    call f_free(psigold)
    call f_free(psifscf)
    call f_free(psifscfold)

    call f_free(psi)
  end subroutine check

  subroutine periodicBC_nohybrid(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: dataold, data

    label = "Periodic BC, no hybrid"

    call setup_periodicBC(dataold, .false.)
    call setup_psi(dataold)
    call setup_periodicBC(data, .false., ndims = dataold%lr%mesh_coarse%ndims+2)
    call check(data%lr, dataold%lr, dataold%psi)
    call setup_clean(data)
    call setup_clean(dataold)
  end subroutine periodicBC_nohybrid

  subroutine periodicBC_hybrid(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: dataold, data

    label = "Periodic BC, hybrid"

    call setup_periodicBC(dataold, .true.)
    call setup_psi(dataold)
    call setup_periodicBC(data, .true., ndims = dataold%lr%mesh_coarse%ndims+2)
    call check(data%lr, dataold%lr, dataold%psi)
    call setup_clean(data)
    call setup_clean(dataold)
  end subroutine periodicBC_hybrid

  subroutine surfaceBC_hgrid(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    integer, dimension(3) :: ndims
    type(setup_data) :: dataold, data

    label = "Surface BC, hgrid change"

    call setup_surfaceBC(dataold)
    call setup_psi(dataold)
    ! hgrid change
    ndims = dataold%lr%mesh_coarse%ndims + 2
    ndims(2) = dataold%lr%mesh_coarse%ndims(2)
    call setup_surfaceBC(data, ndims = ndims)
    call check(data%lr, dataold%lr, dataold%psi)
    call setup_clean(data)
    call setup_clean(dataold)
  end subroutine surfaceBC_hgrid

  subroutine surfaceBC_acell(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    real(gp), dimension(3) :: acell
    integer, dimension(3) :: ndims
    type(setup_data) :: dataold, data

    label = "Surface BC, acell change"

    call setup_surfaceBC(dataold)
    call setup_psi(dataold)
    ! acell change
    ndims = dataold%lr%mesh_coarse%ndims
    ndims(2) = ndims(2) + 2
    acell = dataold%lr%mesh_coarse%dom%acell
    acell(2) = acell(2) + 2._gp * dataold%lr%mesh_coarse%hgrids(2)
    call setup_surfaceBC(data, acell = acell, ndims = ndims)
    call check(data%lr, dataold%lr, dataold%psi)
    call setup_clean(data)
    call setup_clean(dataold)
  end subroutine surfaceBC_acell

  subroutine wireBC_hgrid(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    integer, dimension(3) :: ndims
    type(setup_data) :: dataold, data

    label = "Wire BC, hgrid change"

    call setup_wireBC(dataold)
    call setup_psi(dataold)
    ! hgrid change
    ndims = dataold%lr%mesh_coarse%ndims
    ndims(3) = dataold%lr%mesh_coarse%ndims(3) + 2
    call setup_wireBC(data, ndims = ndims)
    call check(data%lr, dataold%lr, dataold%psi)
    call setup_clean(data)
    call setup_clean(dataold)
  end subroutine wireBC_hgrid

  subroutine wireBC_acell(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    real(gp), dimension(3) :: acell
    integer, dimension(3) :: ndims
    type(setup_data) :: dataold, data

    label = "Wire BC, acell change"

    call setup_wireBC(dataold)
    call setup_psi(dataold)
    ! acell change
    ndims = dataold%lr%mesh_coarse%ndims + 2
    ndims(3) = dataold%lr%mesh_coarse%ndims(3)
    acell = dataold%lr%mesh_coarse%dom%acell + 2._gp * dataold%lr%mesh_coarse%hgrids
    acell(3) = dataold%lr%mesh_coarse%dom%acell(3)
    call setup_wireBC(data, acell = acell, ndims = ndims)
    call check(data%lr, dataold%lr, dataold%psi)
    call setup_clean(data)
    call setup_clean(dataold)
  end subroutine wireBC_acell

  subroutine freeBC_hgrid(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: dataold, data

    label = "Free BC, hgrid change"

    call setup_freeBC(dataold)
    call setup_psi(dataold)
    call setup_freeBC(data, ndims = dataold%lr%mesh_coarse%ndims+2)
    call check(data%lr, dataold%lr, dataold%psi)
    call setup_clean(dataold)
    call setup_clean(data)
  end subroutine freeBC_hgrid

  subroutine freeBC_acell(label)
    use liborbs_precisions
    use setup
    implicit none
    character(len = *), intent(out) :: label

    real(gp), dimension(3) :: acell
    integer, dimension(3) :: ndims
    type(setup_data) :: dataold, data

    label = "Free BC, acell change"

    call setup_freeBC(dataold)
    call setup_psi(dataold)
    ! acell change
    ndims = dataold%lr%mesh_coarse%ndims + 2
    acell = dataold%lr%mesh_coarse%dom%acell + 2._gp * dataold%lr%mesh_coarse%hgrids
    call setup_freeBC(data, acell = acell, ndims = ndims)
    call check(data%lr, dataold%lr, dataold%psi)
    call setup_clean(dataold)
    call setup_clean(data)
  end subroutine freeBC_acell

  subroutine check_io_bin(lr, psiref)
    use liborbs_precisions
    use locregs
    use f_utils
    use compression, only: operator(==)
    use dynamic_memory
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    real(wp), dimension(:), intent(in) :: psiref

    type(locreg_descriptors) :: lr_rd
    integer :: wrt, rd
    real(wp), dimension(:), allocatable :: psi

    wrt = 456
    call f_open_file(wrt, file = "io.dat", binary = .true.)
    call dump_array_bin(lr, wrt, psiref)
    call f_close(wrt)

    rd = 456
    call f_open_file(rd, file = "io.dat", binary = .true.)
    lr_rd = locreg_null()
    call read_lr_bin(rd, lr_rd)
    call verify(lr_rd%wfd == lr%wfd, "wavefunction descriptors")
    psi = f_malloc(array_dim(lr_rd), id = "psi")
    call read_array_bin(lr_rd, rd, psi)
    call f_close(rd)
    call compare(psi, psiref, "compress array")
    call f_free(psi)
    call deallocate_locreg_descriptors(lr_rd)
  end subroutine check_io_bin

  subroutine periodicBC_nohybrid_bin(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, no hybrid"

    call setup_periodicBC(data, .false.)
    call setup_psi(data)
    call check_io_bin(data%lr, data%psi)
    call setup_clean(data)
  end subroutine periodicBC_nohybrid_bin

  subroutine periodicBC_hybrid_bin(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, hybrid"

    call setup_periodicBC(data, .true.)
    call setup_psi(data)
    call check_io_bin(data%lr, data%psi)
    call setup_clean(data)
  end subroutine periodicBC_hybrid_bin

  subroutine surfaceBC_bin(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Surface BC"

    call setup_surfaceBC(data)
    call setup_psi(data)
    call check_io_bin(data%lr, data%psi)
    call setup_clean(data)
  end subroutine surfaceBC_bin

  subroutine wireBC_bin(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Wire BC"

    call setup_wireBC(data)
    call setup_psi(data)
    call check_io_bin(data%lr, data%psi)
    call setup_clean(data)
  end subroutine wireBC_bin

  subroutine freeBC_bin(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Free BC"

    call setup_freeBC(data)
    call setup_psi(data)
    call check_io_bin(data%lr, data%psi)
    call setup_clean(data)
  end subroutine freeBC_bin

  subroutine check_io_plain(lr, psiref)
    use liborbs_precisions
    use locregs
    use f_utils
    use compression, only: operator(==)
    use dynamic_memory
    implicit none
    type(locreg_descriptors), intent(in) :: lr
    real(wp), dimension(:), intent(in) :: psiref

    type(locreg_descriptors) :: lr_rd
    integer :: wrt, rd
    real(wp), dimension(:), allocatable :: psi

    wrt = 456
    call f_open_file(wrt, file = "io.txt", binary = .false.)
    call dump_array_txt(lr, wrt, psiref)
    call f_close(wrt)

    rd = 456
    call f_open_file(rd, file = "io.txt", binary = .false.)
    lr_rd = locreg_null()
    call read_lr_txt(rd, lr_rd)
    call verify(lr_rd%wfd == lr%wfd, "wavefunction descriptors")
    psi = f_malloc(array_dim(lr_rd), id = "psi")
    call read_array_txt(lr_rd, rd, psi)
    call f_close(rd)
    call compare(psi, psiref, "compress array", tol = 1d-12)
    call f_free(psi)
    call deallocate_locreg_descriptors(lr_rd)
  end subroutine check_io_plain

  subroutine periodicBC_nohybrid_plain(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, no hybrid"

    call setup_periodicBC(data, .false.)
    call setup_psi(data)
    call check_io_plain(data%lr, data%psi)
    call setup_clean(data)
  end subroutine periodicBC_nohybrid_plain

  subroutine periodicBC_hybrid_plain(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Periodic BC, hybrid"

    call setup_periodicBC(data, .true.)
    call setup_psi(data)
    call check_io_plain(data%lr, data%psi)
    call setup_clean(data)
  end subroutine periodicBC_hybrid_plain

  subroutine surfaceBC_plain(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Surface BC"

    call setup_surfaceBC(data)
    call setup_psi(data)
    call check_io_plain(data%lr, data%psi)
    call setup_clean(data)
  end subroutine surfaceBC_plain

  subroutine wireBC_plain(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Wire BC"

    call setup_wireBC(data)
    call setup_psi(data)
    call check_io_plain(data%lr, data%psi)
    call setup_clean(data)
  end subroutine wireBC_plain

  subroutine freeBC_plain(label)
    use setup
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data

    label = "Free BC"

    call setup_freeBC(data)
    call setup_psi(data)
    call check_io_plain(data%lr, data%psi)
    call setup_clean(data)
  end subroutine freeBC_plain

  subroutine read_bigdft_wave(label)
    use setup
    use compression, only: operator(==)
    implicit none
    character(len = *), intent(out) :: label

    type(setup_data) :: data, dataref

    label = "Plain text format"

    call setup_fromFile(data, "io.wave", binary = .false.)

    ! io.wave was created from setup data.
    call setup_freeBC(dataref)
    call setup_psi(dataref)

    call verify(data%lr%wfd == dataref%lr%wfd, "wavefunction descriptors")
    call compare(data%psi, dataref%psi, "compress array", tol = 1d-12)

    call setup_clean(dataref)
    call setup_clean(data)

  end subroutine read_bigdft_wave

  subroutine check_roto(llr, llr_old, gmesh, gmesh_old, psiold, psirold)
    use liborbs_precisions
    use rototranslations
    use dynamic_memory
    use locregs
    use box
    use reformatting
    use dictionaries
    use f_enums
    implicit none
    type(locreg_descriptors), intent(in) :: llr, llr_old
    type(cell), intent(in) :: gmesh, gmesh_old
    real(wp), dimension(:), intent(in) :: psiold
    real(wp), dimension(:,:,:), intent(in) :: psirold

    type(rototranslation) :: roto
    real(wp), dimension(:), allocatable :: psi
    integer, parameter :: nat = 4
    real(gp), dimension(3, nat) :: src, dest
    type(dictionary), pointer :: info
    type(f_enumerator) :: en

    psi = f_malloc(array_dim(llr), id = "psi")

    roto = rototranslation_identity()
    call reformatting_init_info(info)
    en = inspect_rototranslation(roto, 1d-5, llr, llr_old, gmesh, gmesh_old, info)
    call verify(en == REFORMAT_FULL, "reformat needed")
    call dict_free(info)
    call reformat_one_supportfunction(llr, llr_old, &
         gmesh, gmesh_old, psiold, roto, psi)
    call compare(sum(psi**2) * llr%mesh%volume_element, &
         sum(psiold**2) * llr_old%mesh%volume_element, "no rototranslation", tol = 6.d-7)

    call reformat_one_supportfunction(llr, llr_old, &
         gmesh, gmesh_old, psiold, roto, psi, psirold, build_psirold = .true.)
    call compare(sum(psi**2) * llr%mesh%volume_element, &
         sum(psiold**2) * llr_old%mesh%volume_element, "with psirold", tol = 1.d-3)

    call reformat_one_supportfunction(llr, llr_old, &
         gmesh, gmesh_old, psiold, roto, psi, build_psirold = .true.)
    call compare(sum(psi**2) * llr%mesh%volume_element, &
         sum(psiold**2) * llr_old%mesh%volume_element, "build psirold", tol = 6.d-7)

    src(:, 1) = (/ 0._gp, 0._gp, 0._gp /)
    src(:, 2) = (/ 1._gp, 0._gp, 0._gp /)
    src(:, 3) = (/ 0._gp, 1._gp, 0._gp /)
    src(:, 4) = (/ 0._gp, 0._gp, 1._gp /)
    
    dest(:, 1) = (/ 0._gp, 0._gp, 0._gp /)
    dest(:, 2) = (/ 1._gp, -1._gp, 0._gp /) / sqrt(2._gp)
    dest(:, 3) = (/ 1._gp, 1._gp, 0._gp /) / sqrt(2._gp)
    dest(:, 4) = (/ 0._gp, 0._gp, 1._gp /)
    
    roto = rototranslation_new(nat, src, dest)
    call reformat_one_supportfunction(llr, llr_old, &
         gmesh, gmesh_old, psiold, roto, psi)
    call compare(sum(psi**2) * llr%mesh%volume_element, &
         sum(psiold**2) * llr_old%mesh%volume_element, "with rotation", tol = 3.d-5)
    
    call f_free(psi)
  end subroutine check_roto

  subroutine periodicBC_roto(label, env)
    use liborbs_precisions
    use setup
    use wrapper_MPI
    implicit none
    character(len = *), intent(out) :: label
    type(mpi_environment), intent(in) :: env

    type(setup_data) :: dataold, data
    real(gp), dimension(3) :: acell
    integer, dimension(3) :: ndims

    label = "Periodic BC"

    call setup_periodicBC(dataold, .false., ndims = (/ 30, 36, 33 /))
    call setup_llr(dataold, env)
    ndims = dataold%lr%mesh_coarse%ndims + 2
    acell = dataold%lr%mesh_coarse%dom%acell + 2._gp * dataold%lr%mesh_coarse%hgrids
    call setup_periodicBC(data, .false., acell = acell, ndims = ndims)
    call setup_llr(data, env)

    if (associated(dataold%llr(1)%bounds%sb%ibyyzz_c)) then
       call setup_tmb(dataold, 1)
       call check_roto(data%llr(1), dataold%llr(1), &
            data%lr%mesh_coarse, dataold%lr%mesh_coarse, dataold%tmb, dataold%psir)
    end if

    call setup_clean(data)
    call setup_clean(dataold)
  end subroutine periodicBC_roto

  subroutine surfaceBC_roto(label, env)
    use liborbs_precisions
    use setup
    use wrapper_MPI
    implicit none
    character(len = *), intent(out) :: label
    type(mpi_environment), intent(in) :: env

    type(setup_data) :: dataold, data
    real(gp), dimension(3) :: acell
    integer, dimension(3) :: ndims

    label = "Surface BC"

    call setup_surfaceBC(dataold, ndims = (/ 30, 36, 33 /))
    call setup_llr(dataold, env)

    ndims = dataold%lr%mesh_coarse%ndims + 2
    acell = dataold%lr%mesh_coarse%dom%acell + 2._gp * dataold%lr%mesh_coarse%hgrids
    call setup_surfaceBC(data, acell = acell, ndims = ndims)
    call setup_llr(data, env)

    if (associated(dataold%llr(1)%bounds%sb%ibyyzz_c)) then
       call setup_tmb(dataold, 1)
       call check_roto(data%llr(1), dataold%llr(1), &
            data%lr%mesh_coarse, dataold%lr%mesh_coarse, dataold%tmb, dataold%psir)
    end if

    call setup_clean(data)
    call setup_clean(dataold)
  end subroutine surfaceBC_roto

  subroutine freeBC_roto(label, env)
    use liborbs_precisions
    use setup
    use wrapper_MPI
    implicit none
    character(len = *), intent(out) :: label
    type(mpi_environment), intent(in) :: env

    type(setup_data) :: dataold, data
    real(gp), dimension(3) :: acell
    integer, dimension(3) :: ndims

    label = "Free BC"

    call setup_freeBC(dataold)
    call setup_llr(dataold, env)

    ndims = dataold%lr%mesh_coarse%ndims + 2
    acell = dataold%lr%mesh_coarse%dom%acell + 2._gp * dataold%lr%mesh_coarse%hgrids
    call setup_freeBC(data, acell = acell, ndims = ndims)
    call setup_llr(data, env)

    if (associated(dataold%llr(1)%bounds%sb%ibyyzz_c)) then
       call setup_tmb(dataold, 1)
       call check_roto(data%llr(1), dataold%llr(1), &
            data%lr%mesh_coarse, dataold%lr%mesh_coarse, dataold%tmb, dataold%psir)
    end if

    call setup_clean(data)
    call setup_clean(dataold)
  end subroutine freeBC_roto

end program test_io
