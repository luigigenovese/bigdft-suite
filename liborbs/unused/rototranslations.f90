  !> Express the coordinates of a vector into a rotated reference frame
  pure function rotate_vector(newz,theta,vec) result(vecn)
    !use module_base
    implicit none
    real(gp), intent(in) :: theta
    real(gp), dimension(3), intent(in) :: newz,vec
    real(gp), dimension(3) :: vecn
    !local variables
    real(gp) :: sint,cost,onemc,x,y,z

    !save recalculation
    sint=sin(theta)
    cost=cos(theta)
    onemc=1.0_gp-cost
    x=vec(1)
    y=vec(2)
    z=vec(3)

    vecn(1)=x*(cost + onemc*newz(1)**2) + y*(onemc*newz(1)*newz(2) - sint*newz(3)) &
         + z*(sint*newz(2) + onemc*newz(1)*newz(3))
    vecn(2)=y*(cost + onemc*newz(2)**2) + x*(onemc*newz(1)*newz(2) + sint*newz(3)) &
         + z*(-(sint*newz(1)) + onemc*newz(2)*newz(3))
    vecn(3)=z*(cost + onemc*newz(3)**2) + x*(onemc*newz(1)*newz(3) - sint*newz(2)) &
         + y*(sint*newz(1) + onemc*newz(2)*newz(3))

  end function rotate_vector


