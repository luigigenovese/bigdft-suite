module f_unittests
  use dictionaries, only: dictionary
  implicit none
  private

  interface compare
     module procedure compare_l, compare_i, compare_il, compare_f, compare_d, compare_s
     module procedure compare_dict, compare_dict_d
     module procedure compare_ai, compare_af, compare_ad
     module procedure compare_ai2, compare_ad2
     module procedure compare_ad3
     module procedure compare_scalar
  end interface compare

  interface run
     module procedure run_serial, run_parallel
  end interface run

  public :: run, compare, verify, skip, expectFailure

  integer :: ERR_SKIPPED_ID = 0
  integer :: ERR_EXPECTED_ID = 0

  type(dictionary), pointer :: logs_ => null()

contains
  function isError(errors, id, reason)
    use dictionaries
    implicit none
    type(dictionary), pointer :: errors
    integer, intent(in) :: id
    character(len = max_field_length), intent(out) :: reason
    logical :: isError

    type(dictionary), pointer :: iter
    integer :: ierr

    isError = .false.
    iter => dict_iter(errors)
    do while (associated(iter))
       ierr = iter // "Id"
       isError = ierr == id
       if (isError) then
          reason = iter // "Additional Info"
          call dict_remove(errors, dict_key(iter))
          return
       end if
       iter => dict_next(iter)
    end do
  end function isError
  
  subroutine output(test, logs, errors)
    use yaml_output
    use yaml_strings
    use dictionaries
    implicit none
    character(len = *), intent(in) :: test
    type(dictionary), pointer :: logs, errors

    type(dictionary), pointer :: iter
    character(len = max_field_length) :: reason

    call yaml_sequence(advance = "no")
    if (associated(errors)) then
       call yaml_mapping_open(test)
       if (isError(errors, ERR_SKIPPED_ID, reason)) then
          call yaml_map("status", "skipped")
          call yaml_map("reason", reason)
       else if (isError(errors, ERR_EXPECTED_ID, reason)) then
          call yaml_map("status", "failed")
          call yaml_map("expected", .true.)
          call yaml_map("reason", reason)
          if (associated(errors)) call yaml_map("error", errors)
       else
          call yaml_map("status", "failed")
          call yaml_map("error", errors)
       end if
       call yaml_mapping_close()
       call dict_free(errors)
       if (associated(logs)) call dict_free(logs)
    else if (associated(logs)) then
       call yaml_mapping_open(test)
       call yaml_map("status", "succeed")
       call yaml_mapping_open("log")
       iter => dict_iter(logs)
       do while (associated(iter))
          call yaml_map(dict_key(iter), iter, flow = .true.)
          iter => dict_next(iter)
       end do
       call yaml_mapping_close()
       call yaml_mapping_close()
       call dict_free(logs)
    else
       call yaml_map(test, "succeed")
    end if
  end subroutine output
  
  subroutine run_parallel(func, env)
    use dictionaries
    use wrapper_MPI
    implicit none
    interface
       subroutine func(test, env)
         use wrapper_MPI
         character(len = *), intent(out) :: test
         type(mpi_environment), intent(in) :: env
       end subroutine func
    end interface

    character(len = max_field_length) :: test
    type(dictionary), pointer :: errors
    type(mpi_environment), intent(in) :: env

    logical :: error

    nullify(errors)
    call f_err_open_try()
    call func(test, env)
    call f_err_close_try(errors)

    error = associated(errors)
    call fmpi_allreduce(error, 1, FMPI_LOR, env%mpi_comm)
    if (associated(errors) .and. env%iproc > 0) call dict_free(errors)

    if (env%iproc > 0) return

    if (.not. associated(errors) .and. error) then
       call dict_init(errors)
       call set(errors, "MPI issue")
    end if
    call output(test, logs_, errors)
    nullify(logs_)
  end subroutine run_parallel

  subroutine run_serial(func)
    use dictionaries
    use wrapper_MPI
    implicit none
    interface
       subroutine func(test)
         character(len = *), intent(out) :: test
       end subroutine func
    end interface

    character(len = max_field_length) :: test
    type(dictionary), pointer :: errors
    type(mpi_environment) :: env
    integer :: iproc

    env = mpi_environment_comm()
    iproc = env%iproc
    call release_mpi_environment(env)
    if (iproc > 0) return

    nullify(errors)
    call f_err_open_try()
    call func(test)
    call f_err_close_try(errors)
    call output(test, logs_, errors)
    nullify(logs_)
  end subroutine run_serial

  subroutine log(label, logs)
    use dictionaries
    implicit none
    character(len = *), intent(in) :: label
    type(dictionary), pointer :: logs

    if (.not. associated(logs_)) logs_ => dict_new()
    call set(logs_ // label, logs)
  end subroutine log

  subroutine skip(reason)
    use dictionaries
    implicit none
    character(len = *), intent(in), optional :: reason

    if (ERR_SKIPPED_ID == 0) then
       call f_err_define("REGTESTS_SKIP", "test skipped", ERR_SKIPPED_ID)
    end if
    call f_err_throw(reason, ERR_SKIPPED_ID)
  end subroutine skip

  subroutine expectFailure(reason)
    use dictionaries
    implicit none
    character(len = *), intent(in), optional :: reason

    if (ERR_EXPECTED_ID == 0) then
       call f_err_define("REGTESTS_EXPECT", "expected test failure", ERR_EXPECTED_ID)
    end if
    call f_err_throw(reason, ERR_EXPECTED_ID)
  end subroutine expectFailure

  subroutine verify(val, label)
    use dictionaries
    implicit none
    logical, intent(in) :: val
    character(len = *), intent(in) :: label

    if (.not. val) call f_err_throw(label // ": failed")
  end subroutine verify
  
  subroutine compare_l(val, expected, label)
    use dictionaries
    use yaml_strings
    implicit none
    logical, intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok, delta
    ok = val .eqv. expected
    delta = ok
    include 'compare-scalar-inc.f90'
  end subroutine compare_l

  subroutine compare_i(val, expected, label)
    use dictionaries
    use f_precisions, only: f_integer
    use yaml_strings
    implicit none
    integer(f_integer), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok
    integer(f_integer) :: delta
    ok = val == expected
    delta = abs(val-expected)
    include 'compare-scalar-inc.f90'
  end subroutine compare_i

  subroutine compare_il(val, expected, label)
    use dictionaries
    use f_precisions, only: f_long
    use yaml_strings
    implicit none
    integer(f_long), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok
    integer(f_long) :: delta
    ok = val == expected
    delta = abs(val-expected)
    include 'compare-scalar-inc.f90'
  end subroutine compare_il

  subroutine compare_f(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_simple
    use yaml_strings
    implicit none
    real(f_simple), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_simple), intent(in), optional :: tol
    logical :: ok
    real(f_simple) :: delta
    delta = abs(val - expected)
    if (present(tol)) then
       ok = delta < tol
    else
       ok = val == expected
    end if
    include 'compare-scalar-inc.f90'
  end subroutine compare_f

  subroutine compare_d(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_double
    use yaml_strings
    implicit none
    real(f_double), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    logical :: ok
    real(f_double) :: delta
    type(dictionary), pointer :: diff

    delta = abs(val - expected)
    if (present(tol)) then
       ok = delta < tol
       if (ok .and. present(label)) then
          diff => dict_new()
          call set(diff // "diff", delta, fmt = "(1G10.4)")
          call set(diff // "tol", tol, fmt = "(1G14.2)")
          call log(label, diff)
       end if
    else
       ok = val == expected
    end if
    include 'compare-scalar-inc.f90'
  end subroutine compare_d

  subroutine compare_s(val, expected, label)
    use dictionaries
    use yaml_strings
    implicit none
    character(len = *), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok, delta
    ok = val == expected
    delta = ok
    include 'compare-scalar-inc.f90'
  end subroutine compare_s

  function scalar_toa(s) result(a)
    use dictionaries
    use f_arrays
    use yaml_strings
    use f_precisions, only: f_double
    implicit none
    type(f_scalar), intent(in) :: s
    character(len = max_field_length) :: a

    if (imag(s) > 0._f_double) then
       a = trim(yaml_toa(real(s))) // "+" // trim(yaml_toa(imag(s))) // "*i"
    else if (imag(s) < 0._f_double) then
       a = trim(yaml_toa(real(s))) // "-" // trim(yaml_toa(-imag(s))) // "*i"
    else
       a = trim(yaml_toa(real(s)))
    end if
  end function scalar_toa

  subroutine compare_scalar(val, expected, label, tol)
    use dictionaries
    use f_arrays
    use f_precisions, only: f_double
    use yaml_strings
    implicit none
    type(f_scalar), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    logical :: ok
    type(dictionary), pointer :: diff
    if (present(tol)) then
       ok = abs(val - expected) < tol
       if (ok .and. present(label)) then
          diff => dict_new()
          call set(diff // "diff", abs(val - expected), fmt = "(1G10.4)")
          call set(diff // "tol", tol, fmt = "(1G14.2)")
          call log(label, diff)
       end if
    else
       ok = (real(val) == real(expected)) .and. (imag(val) == imag(expected))
    end if
    if (.not. ok) then
       if (present(label)) then
          call f_err_throw('"' // label // ": expected " // &
               trim(scalar_toa(expected)) // ", value " // trim(scalar_toa(val)) // '"')
       else
          call f_err_throw('"' // "expected " // &
               trim(scalar_toa(expected)) // ", value " // trim(scalar_toa(val)) // '"')
       end if
    end if
  end subroutine compare_scalar

  subroutine compare_dict(dict, expected, label)
    use dictionaries
    use yaml_strings
    implicit none
    type(dictionary), pointer :: dict
    character(len = *), intent(in) :: expected
    character(len = *), intent(in), optional :: label
    character(len = max_field_length) :: val

    val = dict
    call compare(val, expected, label)
  end subroutine compare_dict

  subroutine compare_dict_d(dict, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_double, f_long
    use yaml_strings
    implicit none
    type(dictionary), pointer :: dict
    real(f_double), intent(in) :: expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    real(f_double) :: val

    val = dict
    call compare(val, expected, label, tol)
  end subroutine compare_dict_d

  subroutine compare_ai(val, expected, label)
    use dictionaries
    use f_precisions, only: f_integer
    use yaml_strings
    implicit none
    integer(f_integer), dimension(:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok
    include 'compare-array-inc-size.f90'
    ok = all(val == expected)
    include 'compare-array-inc.f90'
  end subroutine compare_ai

  subroutine compare_af(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_simple
    use yaml_strings
    implicit none
    real(f_simple), dimension(:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_simple), intent(in), optional :: tol
    logical :: ok
    include 'compare-array-inc-size.f90'
    if (present(tol)) then
       ok = all(abs(val - expected) < tol)
    else
       ok = all(val == expected)
    end if
    include 'compare-array-inc.f90'
  end subroutine compare_af

  subroutine compare_ad(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_double
    use yaml_strings
    implicit none
    real(f_double), dimension(:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    logical :: ok
    type(dictionary), pointer :: diff
    include 'compare-array-inc-size.f90'
    if (present(tol)) then
       ok = all(abs(val - expected) < tol)
       if (ok .and. present(label)) then
          diff => dict_new()
          call set(diff // "diff", maxval(abs(val - expected)), fmt = "(1G10.4)")
          call set(diff // "tol", tol, fmt = "(1G14.2)")
          call log(label, diff)
       end if
    else
       ok = all(val == expected)
    end if
    include 'compare-array-inc.f90'
  end subroutine compare_ad

  subroutine compare_ad2(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_double
    use yaml_strings
    implicit none
    real(f_double), dimension(:,:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    logical :: ok
    type(dictionary), pointer :: diff
    include 'compare-array-inc-size.f90'
    if (present(tol)) then
       ok = all(abs(val - expected) < tol)
       if (ok .and. present(label)) then
          diff => dict_new()
          call set(diff // "diff", maxval(abs(val - expected)), fmt = "(1G10.4)")
          call set(diff // "tol", tol, fmt = "(1G14.2)")
          call log(label, diff)
       end if
    else
       ok = all(val == expected)
    end if
    include 'compare-array-inc.f90'
  end subroutine compare_ad2

  subroutine compare_ai2(val, expected, label)
    use dictionaries
    use f_precisions, only: f_integer
    use yaml_strings
    implicit none
    integer(f_integer), dimension(:,:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    logical :: ok
    include 'compare-array-inc-size.f90'
    ok = all(val == expected)
    include 'compare-array-inc.f90'
  end subroutine compare_ai2

  subroutine compare_ad3(val, expected, label, tol)
    use dictionaries
    use f_precisions, only: f_double
    use yaml_strings
    implicit none
    real(f_double), dimension(:,:,:), intent(in) :: val, expected
    character(len = *), intent(in), optional :: label
    real(f_double), intent(in), optional :: tol
    logical :: ok
    type(dictionary), pointer :: diff
    include 'compare-array-inc-size.f90'
    if (present(tol)) then
       ok = all(abs(val - expected) < tol)
       if (ok .and. present(label)) then
          diff => dict_new()
          call set(diff // "diff", maxval(abs(val - expected)), fmt = "(1G10.4)")
          call set(diff // "tol", tol, fmt = "(1G14.2)")
          call log(label, diff)
       end if
    else
       ok = all(val == expected)
    end if
    include 'compare-array-inc.f90'
  end subroutine compare_ad3
end module f_unittests
