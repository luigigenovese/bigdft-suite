    if (.not. ok) then
       if (present(label)) then
          call f_err_throw('"' // label // ": expected " // trim(yaml_toa(expected)) // &
               ", value " // trim(yaml_toa(val)) // ', delta ' // trim(yaml_toa(delta,fmt = "(1G14.2)"))// '"')
       else
          call f_err_throw('"' // "expected " // trim(yaml_toa(expected)) // &
               ", value " // trim(yaml_toa(val)) // ', delta ' // trim(yaml_toa(delta,fmt = "(1G14.2)"))// '"')
       end if
    end if
