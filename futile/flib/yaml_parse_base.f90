!> @file
!! Module to parse the yaml (flib library)
!! @author
!!    Copyright (C) 2013-2013 BigDFT group
!!    This file is distributed under the terms of the
!!    GNU General Public License, see ~/COPYING file
!!    or http://www.gnu.org/copyleft/gpl.txt .
!!    For the list of contributors, see ~/AUTHORS


!> Module containing routines to parse a yaml input
module yaml_parse_base
  use dictionaries, only: dictionary,max_field_length
  implicit none

  private

  integer :: STREAM_START, STREAM_END
  integer :: DOCUMENT_START, DOCUMENT_END
  integer :: SEQUENCE_START, SEQUENCE_END
  integer :: MAPPING_START, MAPPING_END
  integer :: ALIAS, SCALAR, ERROR

  !> Contains some keys to better explain the yaml parse errors
  type(dictionary), pointer :: dict_yaml_errs=>null()

  !> Two possible errors when parsing
  integer, public :: YAML_PARSE_ERROR       = 0
  integer, public :: YAML_PARSE_UNSUPPORTED = 0

  integer, parameter :: anchor_length = 32

  public :: yaml_parse_from_file
  public :: yaml_parse_from_char_array
  public :: yaml_parse_from_string
  public :: yaml_parse_database

  !for internal f_lib usage
  public :: yaml_parse_errors
  public :: yaml_parse_errors_finalize
  public :: yaml_load

contains

  !> Define the errors related to the parsing of the yaml files
  subroutine yaml_parse_errors()
    use dictionaries
    implicit none

    call f_err_define(err_name='YAML_PARSE_ERROR',&
         err_msg='YAML parse error.',&
         err_action='Modify your inputs.',&
         err_id=YAML_PARSE_ERROR)

    !Define a dictionary to have a more verbosity of yaml_parse_error
    dict_yaml_errs => dict_new("<document start>" .is. &
         & "The first indentation is different at this line in front of the given key.", &
         &                     "mapping values" .is. &
         & "The indentation is different at this line." )

    call f_err_define(err_name='YAML_PARSE_UNSUPPORTED',&
         err_msg='YAML standard not supported.',&
         err_action='kindly ask developers to finish implementation.',&
         err_id=YAML_PARSE_UNSUPPORTED)

!!$    call f_err_define(err_name='ERROR_YAML_COMMAND_LINE_PARSER',&
!!$         err_msg='Error in yaml parsing of the command line',&
!!$         err_action='Check the allowed options and their values (--help).',&
!!$         err_id=ERROR_YAML_COMMAND_LINE_PARSER)

  end subroutine yaml_parse_errors

  !> Create a dict from a file (fname is the buffer containing all the file)
  subroutine yaml_parse_from_file(dict, fname, tags)
    use dictionaries
    implicit none
    type(dictionary), pointer :: dict
    character(len = *), intent(in) :: fname
    type(dictionary), pointer, optional :: tags

    integer(kind = 8) :: parser

    call yaml_parser_c_init(parser, fname, len(fname))
    dict => yaml_parse_(parser, tags)
  end subroutine yaml_parse_from_file


  !> parse a database from the corresponding external routine
  !! such a routine is created from the Makefile macro
  !! yaml_db.mk which has to be included in the Makefile
  subroutine yaml_parse_database(dict,symbol)
    use f_precisions
    implicit none
    type(dictionary), pointer :: dict
    external :: symbol
    !local variables
    integer(f_integer) :: db_size
    integer(f_address) :: parser
    character, dimension(:), allocatable :: f_string !<not profiled in f_malloc

    !get reference to the database and size of the data
    !first get the size
    db_size=int(0,f_integer)
    call symbol(parser,db_size) !<parser here is ignored
    !allocate array
    allocate(f_string(db_size))
    call symbol(f_string,db_size) !<we fill the array
    call yaml_parse_from_char_array(dict,f_string)
    deallocate(f_string)
  end subroutine yaml_parse_database


  subroutine yaml_parse_from_char_array(dict, carr, tags)
    use dictionaries
    implicit none
    type(dictionary), pointer :: dict
    character, dimension(:), intent(in) :: carr
    type(dictionary), pointer, optional :: tags

    integer(kind = 8) :: parser

    call yaml_parser_c_init_from_buf(parser, carr(1), size(carr))
    dict => yaml_parse_(parser, tags)
  end subroutine yaml_parse_from_char_array


  subroutine yaml_parse_from_string(dict, str, tags)
    use dictionaries
    implicit none
    type(dictionary), pointer :: dict
    character(len = *), intent(in) :: str
    type(dictionary), pointer, optional :: tags

    integer(kind = 8) :: parser

    if (len_trim(str) > 0) then
       call yaml_parser_c_init_from_buf(parser, str, len_trim(str))
       dict => yaml_parse_(parser, tags)
    else
       nullify(dict)
    end if
  end subroutine yaml_parse_from_string

  function yaml_parse_(parser, tags) result(output)
    use dictionaries
    implicit none
    integer(kind = 8), intent(in) :: parser
    type(dictionary), pointer, optional :: tags

    type(dictionary), pointer :: dict, doc, output, anchors, it
    integer :: event, errid
    character(max_field_length) :: val
    character(anchor_length) :: anchor

    ! Get event values from C.
    call yaml_parser_c_get_stream_start(STREAM_START)
    call yaml_parser_c_get_stream_end(STREAM_END)
    call yaml_parser_c_get_document_start(DOCUMENT_START)
    call yaml_parser_c_get_document_end(DOCUMENT_END)
    call yaml_parser_c_get_sequence_start(SEQUENCE_START)
    call yaml_parser_c_get_sequence_end(SEQUENCE_END)
    call yaml_parser_c_get_mapping_start(MAPPING_START)
    call yaml_parser_c_get_mapping_end(MAPPING_END)
    call yaml_parser_c_get_alias(ALIAS)
    call yaml_parser_c_get_scalar(SCALAR)
    call yaml_parser_c_get_error(ERROR)

    ! Initialise error if required.
    if (YAML_PARSE_ERROR == 0) then
       call f_err_define(err_name='YAML_PARSE_ERROR',&
            err_msg='YAML parse error.',&
            err_action='correct your YAML stream.',&
            err_id=YAML_PARSE_ERROR)
    end if
    if (YAML_PARSE_UNSUPPORTED == 0) then
       call f_err_define(err_name='YAML_PARSE_UNSUPPORTED',&
            err_msg='YAML standard not supported.',&
            err_action='kindly ask developers to finish implementation.',&
            err_id=YAML_PARSE_UNSUPPORTED)
    end if

    call f_err_open_try()
    call dict_init(dict)
    call dict_init(anchors)
    event = 0
    nullify(doc)
    val=repeat(' ',len(val))
    do while (event /= STREAM_END)
       call yaml_parser_c_next(parser, event, val, max_field_length, anchor, anchor_length)
       !print *,'event',event_toa(event),event,trim(val),'end'
       if (event == ERROR) then
          !search
          call f_err_throw(err_id = YAML_PARSE_ERROR, err_msg = trim(val))
          exit
       end if

       if (event == DOCUMENT_END) then
          if (.not.associated(doc)) call dict_init(doc)   ! empty document case
          call add(dict, doc)
          nullify(doc)
       else if (event == MAPPING_START) then
          doc => build_map(parser, anchors, trim(anchor)) ! dictionary document case
       else if (event == SEQUENCE_START) then
          doc => build_seq(parser, anchors, trim(anchor)) ! list document case
       else if (event == SCALAR) then
          call dict_init(doc)                             ! scalar document case
          call set(doc, val)
       end if

       if (f_err_check(YAML_PARSE_ERROR)) exit
    end do
    if (present(tags)) then
       it => dict_iter(tags)
       do while (associated(it))
          if (dict_key(it) .in. anchors) &
               call dict_copy(it, anchors // dict_key(it))
          it => dict_next(it)
       end do
    end if
    call dict_free(anchors)
    errid = f_get_last_error(val)
    if (f_err_check(YAML_PARSE_ERROR)) then
       if (associated(doc)) call add(dict, doc)
    end if
    call f_err_close_try()

    if (event /= STREAM_END) call yaml_parser_c_finalize(parser)

    output => dict

    !if (errid == YAML_PARSE_ERROR) call f_err_throw(err_id = errid, err_msg = trim(val))
    if (errid == YAML_PARSE_ERROR) call yaml_parse_error_throw(val)

  end function yaml_parse_


  recursive function build_map(parser, anchors, anchor) result(map)
    use dictionaries
    !use yaml_output
    implicit none
    integer(kind = 8), intent(in) :: parser
    type(dictionary), pointer :: anchors
    character(len = *), intent(in) :: anchor

    type(dictionary), pointer :: m, sub, map
    integer :: event
    character(max_field_length) :: val, key
    character(anchor_length) :: anch

    call dict_init(m)
    map => m

    event = 0
    key(1:max_field_length) = " "
    do while (event /= STREAM_END)
       call yaml_parser_c_next(parser, event, val, max_field_length, anch, anchor_length)
       !write(*,*) "map", event

       if (event == ERROR) then
          call f_err_throw(err_id = YAML_PARSE_ERROR, err_msg = trim(val))
          !call yaml_parse_error_throw(val)
          return
       end if

       if (event == MAPPING_END) then
          exit
       else if (event == MAPPING_START) then
          sub => build_map(parser, anchors, trim(anch))
          if (len_trim(key) == 0) stop "no key"
          call set(m // key, sub)
          key(1:max_field_length) = " "
       else if (event == SEQUENCE_START) then
          sub => build_seq(parser, anchors, trim(anch))
          if (len_trim(key) == 0) stop "no key"
          call set(m // key, sub)
          key(1:max_field_length) = " "
       else if (event == SCALAR) then
          if (len_trim(key) > 0) then
             if (len_trim(anch) > 0) call set(anchors // anch, val)

             ! This is a simple key / val entry.
             call set(m // key, val)
             key(1:max_field_length) = " "
          else
             ! We store a key for later usage.
             key = val
             !write(*,*) "set ", key
          end if
       else if (event == ALIAS) then
          if (f_err_raise(val .notin. anchors, &
               err_msg = "unseen anchor " // trim(val), err_id = YAML_PARSE_ERROR)) return
          call dict_copy(m // key, anchors // val)
          key(1:max_field_length) = " "
       end if

       if (f_err_check(YAML_PARSE_ERROR)) return

    end do

    if (len(anchor) > 0) then
       call dict_copy(anchors // anchor, map)
    end if
  end function build_map


  recursive function build_seq(parser, anchors, anchor) result(seq)
    use dictionaries
    implicit none
    integer(kind = 8), intent(in) :: parser
    type(dictionary), pointer :: anchors
    character(len = *), intent(in) :: anchor

    type(dictionary), pointer :: s, sub, seq, dup
    integer :: event
    character(max_field_length) :: val
    character(anchor_length) :: anch

    call dict_init(s)
    seq => s
    nullify(s)

    event = 0
    do while (event /= STREAM_END)
       call yaml_parser_c_next(parser, event, val, max_field_length, anch, anchor_length)
       !write(*,*) "seq", event

       if (event == ERROR) then
          call f_err_throw(err_id = YAML_PARSE_ERROR, err_msg = trim(val))
          !call yaml_parse_error_throw(val)
          return
       end if

       if (event == SEQUENCE_END) then
          exit
       else if (event == MAPPING_START) then
          sub => build_map(parser, anchors, trim(anch))
          call add(seq, sub, s)
       else if (event == SEQUENCE_START) then
          sub => build_seq(parser, anchors, trim(anch))
          call add(seq, sub, s)
       else if (event == SCALAR) then
          if (len_trim(anch) > 0) call set(anchors // anch, val)
          call add(seq, val, s)
       else if (event == ALIAS) then
          if (f_err_raise(val .notin. anchors, &
               err_msg = "unseen anchor " // trim(val), err_id = YAML_PARSE_ERROR)) return
          call dict_init(dup)
          call dict_copy(dup, anchors // val)
          call add(seq, dup, s)
       end if

       if (f_err_check(YAML_PARSE_ERROR)) return

    end do

    if (len(anchor) > 0) then
       call dict_copy(anchors // anchor, seq)
    end if
  end function build_seq

  function yaml_load(string,key) result(dict)
    use dictionaries
    use yaml_strings, only: f_strcpy
    !use yaml_output !to be removed
    implicit none
    character(len=*), intent(in) :: string
    !> key of the parsed string. the dictionary will have this key
    !! if this variable is given
    character(len=*), intent(in), optional :: key
    type(dictionary), pointer :: dict
    !local variables
    type(dictionary), pointer :: loaded_string,test
    !parse from the given string
    call yaml_parse_from_string(loaded_string,string)

    if (associated(loaded_string)) then
       !extract the first document
       dict => loaded_string .pop. 0
       call dict_free(loaded_string)
    else
       nullify(dict)
    end if

    if (present(key) .and. associated(dict)) then !to be defined better
       select case(dict_value(dict))
       case(TYPE_DICT,TYPE_LIST)
          call dict_init(test)
          call dict_copy(test//trim(key),dict)
          call dict_free(dict)
          dict=>test
       case default
          test=>dict_new(trim(key) .is. dict_value(dict_iter(dict)))
          call dict_free(dict)
          dict=>test
       end select
    end if

  end function yaml_load

  !> Throw an error with YAML_PARSE_ERROR trying to give a better understandable message
  subroutine yaml_parse_error_throw(val)
    use dictionaries
    implicit none
    !Argument
    character(len=max_field_length) :: val
    !Local variables
    type(dictionary), pointer :: error
    !type(dictionary), pointer :: dict_error
    character(max_field_length) :: key,message
    !integer :: pp
    !dict_error=>dict_new()
    !pp = index(val,':')
    !key = val(1:pp-1)
    !message = val(pp+1:)
    !call set(dict_error//trim(key),trim(message))
    error=>dict_iter(dict_yaml_errs)
    message = trim(val)
    do while(associated(error))
      key = dict_key(error)
      if (index(val,trim(key)) /= 0) then
        !call set(dict_error//'Info',trim(dict_value(error)))
        message = trim(message) // '. ' // trim(dict_value(error))
        exit
      end if
      error=>dict_next(error)
    end do
    !call f_err_throw(err_id = YAML_PARSE_ERROR, err_dict = dict_error)
    !We add quite to have a yaml standard output.
    call f_err_throw(err_msg = '"'//trim(message)//'"',err_id = YAML_PARSE_ERROR)
  end subroutine yaml_parse_error_throw


  !> Nullify the dictionary dict_yaml_errs
  subroutine yaml_parse_errors_finalize()
     use dictionaries_base, only: dict_free
     implicit none
     call dict_free(dict_yaml_errs)

  end subroutine yaml_parse_errors_finalize


end module yaml_parse_base
