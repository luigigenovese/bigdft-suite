#include "yaml_output.h"
#include <config.h>
#include <string.h>

void FC_FUNC_(bind_yaml_swap_stream, BIND_YAML_SWAP_STREAM)(const int*,
  int*,
  int*);
void yaml_swap_stream(int new_unit,
  int* old_unit,
  int* ierr)
{
  FC_FUNC_(bind_yaml_swap_stream, BIND_YAML_SWAP_STREAM)
    (&new_unit, old_unit, ierr);
}

void FC_FUNC_(bind_yaml_output_errors, BIND_YAML_OUTPUT_ERRORS)(void);
void yaml_output_errors(void)
{
  FC_FUNC_(bind_yaml_output_errors, BIND_YAML_OUTPUT_ERRORS)
    ();
}

void FC_FUNC_(bind_yaml_set_default_stream, BIND_YAML_SET_DEFAULT_STREAM)(const int*,
  int*);
void yaml_set_default_stream(int unit,
  int* ierr)
{
  FC_FUNC_(bind_yaml_set_default_stream, BIND_YAML_SET_DEFAULT_STREAM)
    (&unit, ierr);
}

void FC_FUNC_(bind_yaml_get_default_stream, BIND_YAML_GET_DEFAULT_STREAM)(int*);
void yaml_get_default_stream(int* unit)
{
  FC_FUNC_(bind_yaml_get_default_stream, BIND_YAML_GET_DEFAULT_STREAM)
    (unit);
}

void FC_FUNC_(bind_yaml_stream_connected, BIND_YAML_STREAM_CONNECTED)(const char*,
  const size_t*,
  int*,
  int*,
  size_t);
void yaml_stream_connected(const char* filename,
  int* unit,
  int (*istat))
{
  size_t filename_chk_len, filename_len = filename_chk_len = filename ? strlen(filename) : 0;
  FC_FUNC_(bind_yaml_stream_connected, BIND_YAML_STREAM_CONNECTED)
    (filename, &filename_len, unit, istat, filename_chk_len);
}

void FC_FUNC_(bind_yaml_set_stream, BIND_YAML_SET_STREAM)(const int*,
  const char*,
  const size_t*,
  int*,
  const int*,
  const int*,
  const char*,
  const size_t*,
  const int*,
  size_t,
  size_t);
void yaml_set_stream(const int (*unit),
  const char (*filename),
  int (*istat),
  const int (*tabbing),
  const int (*record_length),
  const char (*position),
  const bool (*setdefault))
{
  size_t filename_chk_len, filename_len = filename_chk_len = filename ? strlen(filename) : 0;
  size_t position_chk_len, position_len = position_chk_len = position ? strlen(position) : 0;
  int setdefault_conv = setdefault ? *setdefault : 0;
  FC_FUNC_(bind_yaml_set_stream, BIND_YAML_SET_STREAM)
    (unit, filename, &filename_len, istat, tabbing, record_length, position, &position_len,  setdefault ? &setdefault_conv : NULL, filename_chk_len, position_chk_len);
}

void FC_FUNC_(bind_yaml_stream_attributes, BIND_YAML_STREAM_ATTRIBUTES)(const int*,
  const int*,
  int*,
  int*,
  int*,
  int*,
  int*,
  int*,
  int*,
  int*,
  int*);
void yaml_stream_attributes(const int (*unit),
  const int (*stream_unit),
  int (*icursor),
  bool (*flowrite),
  int (*itab_active),
  int (*iflowlevel),
  int (*ilevel),
  int (*ilast),
  int (*indent),
  int (*indent_previous),
  int (*record_length))
{
  int flowrite_conv;
  FC_FUNC_(bind_yaml_stream_attributes, BIND_YAML_STREAM_ATTRIBUTES)
    (unit, stream_unit, icursor,  flowrite ? &flowrite_conv : NULL, itab_active, iflowlevel, ilevel, ilast, indent, indent_previous, record_length);
  if (flowrite) *flowrite = flowrite_conv;
}

void FC_FUNC_(bind_yaml_new_document, BIND_YAML_NEW_DOCUMENT)(const int*);
void yaml_new_document(const int (*unit))
{
  FC_FUNC_(bind_yaml_new_document, BIND_YAML_NEW_DOCUMENT)
    (unit);
}

void FC_FUNC_(bind_yaml_flush_document, BIND_YAML_FLUSH_DOCUMENT)(const int*);
void yaml_flush_document(const int (*unit))
{
  FC_FUNC_(bind_yaml_flush_document, BIND_YAML_FLUSH_DOCUMENT)
    (unit);
}

void FC_FUNC_(bind_yaml_close_stream, BIND_YAML_CLOSE_STREAM)(const int*,
  int*);
void yaml_close_stream(const int (*unit),
  int (*istat))
{
  FC_FUNC_(bind_yaml_close_stream, BIND_YAML_CLOSE_STREAM)
    (unit, istat);
}

void FC_FUNC_(bind_yaml_close_all_streams, BIND_YAML_CLOSE_ALL_STREAMS)(void);
void yaml_close_all_streams(void)
{
  FC_FUNC_(bind_yaml_close_all_streams, BIND_YAML_CLOSE_ALL_STREAMS)
    ();
}

void FC_FUNC_(bind_yaml_dict_inspect, BIND_YAML_DICT_INSPECT)(f90_dictionary_pointer*);
void yaml_dict_inspect(f90_dictionary_pointer dict)
{
  FC_FUNC_(bind_yaml_dict_inspect, BIND_YAML_DICT_INSPECT)
    (&dict);
}

void FC_FUNC_(bind_dump_progress_bar, BIND_DUMP_PROGRESS_BAR)(f90_f_progress_bar*,
  const int*,
  const int*);
void dump_progress_bar(f90_f_progress_bar* bar,
  const int (*step),
  const int (*unit))
{
  FC_FUNC_(bind_dump_progress_bar, BIND_DUMP_PROGRESS_BAR)
    (bar, step, unit);
}

void FC_FUNC_(bind_yaml_cite, BIND_YAML_CITE)(const char*,
  const size_t*,
  const int*,
  size_t);
void yaml_cite(const char* paper,
  const int (*unit))
{
  size_t paper_chk_len, paper_len = paper_chk_len = paper ? strlen(paper) : 0;
  FC_FUNC_(bind_yaml_cite, BIND_YAML_CITE)
    (paper, &paper_len, unit, paper_chk_len);
}

void FC_FUNC_(bind_yaml_bib_dump, BIND_YAML_BIB_DUMP)(f90_dictionary_pointer*,
  const int*);
void yaml_bib_dump(f90_dictionary_pointer citations,
  const int (*unit))
{
  FC_FUNC_(bind_yaml_bib_dump, BIND_YAML_BIB_DUMP)
    (&citations, unit);
}

void FC_FUNC_(bind_yaml_scalar, BIND_YAML_SCALAR)(const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t);
void yaml_scalar(const char* message,
  const char (*advance),
  const int (*unit),
  const char (*hfill))
{
  size_t message_chk_len, message_len = message_chk_len = message ? strlen(message) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t hfill_chk_len, hfill_len = hfill_chk_len = hfill ? strlen(hfill) : 0;
  FC_FUNC_(bind_yaml_scalar, BIND_YAML_SCALAR)
    (message, &message_len, advance, &advance_len, unit, hfill, &hfill_len, message_chk_len, advance_chk_len, hfill_chk_len);
}

void FC_FUNC_(bind_yaml_mapping_open, BIND_YAML_MAPPING_OPEN)(const char*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const int*,
  const char*,
  const size_t*,
  const int*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_mapping_open(const char (*mapname),
  const char (*label),
  const char (*tag),
  const bool (*flow),
  const int (*tabbing),
  const char (*advance),
  const int (*unit))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t tag_chk_len, tag_len = tag_chk_len = tag ? strlen(tag) : 0;
  int flow_conv = flow ? *flow : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  FC_FUNC_(bind_yaml_mapping_open, BIND_YAML_MAPPING_OPEN)
    (mapname, &mapname_len, label, &label_len, tag, &tag_len,  flow ? &flow_conv : NULL, tabbing, advance, &advance_len, unit, mapname_chk_len, label_chk_len, tag_chk_len, advance_chk_len);
}

void FC_FUNC_(bind_yaml_mapping_close, BIND_YAML_MAPPING_CLOSE)(const char*,
  const size_t*,
  const int*,
  size_t);
void yaml_mapping_close(const char (*advance),
  const int (*unit))
{
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  FC_FUNC_(bind_yaml_mapping_close, BIND_YAML_MAPPING_CLOSE)
    (advance, &advance_len, unit, advance_chk_len);
}

void FC_FUNC_(bind_yaml_sequence_open, BIND_YAML_SEQUENCE_OPEN)(const char*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const int*,
  const char*,
  const size_t*,
  const int*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_sequence_open(const char (*mapname),
  const char (*label),
  const char (*tag),
  const bool (*flow),
  const int (*tabbing),
  const char (*advance),
  const int (*unit))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t tag_chk_len, tag_len = tag_chk_len = tag ? strlen(tag) : 0;
  int flow_conv = flow ? *flow : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  FC_FUNC_(bind_yaml_sequence_open, BIND_YAML_SEQUENCE_OPEN)
    (mapname, &mapname_len, label, &label_len, tag, &tag_len,  flow ? &flow_conv : NULL, tabbing, advance, &advance_len, unit, mapname_chk_len, label_chk_len, tag_chk_len, advance_chk_len);
}

void FC_FUNC_(bind_yaml_sequence_close, BIND_YAML_SEQUENCE_CLOSE)(const char*,
  const size_t*,
  const int*,
  size_t);
void yaml_sequence_close(const char (*advance),
  const int (*unit))
{
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  FC_FUNC_(bind_yaml_sequence_close, BIND_YAML_SEQUENCE_CLOSE)
    (advance, &advance_len, unit, advance_chk_len);
}

void FC_FUNC_(bind_yaml_newline, BIND_YAML_NEWLINE)(const int*);
void yaml_newline(const int (*unit))
{
  FC_FUNC_(bind_yaml_newline, BIND_YAML_NEWLINE)
    (unit);
}

void FC_FUNC_(bind_yaml_sequence, BIND_YAML_SEQUENCE)(const char*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const int*,
  size_t,
  size_t,
  size_t);
void yaml_sequence(const char (*seqvalue),
  const char (*label),
  const char (*advance),
  const int (*unit),
  const int (*padding))
{
  size_t seqvalue_chk_len, seqvalue_len = seqvalue_chk_len = seqvalue ? strlen(seqvalue) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  FC_FUNC_(bind_yaml_sequence, BIND_YAML_SEQUENCE)
    (seqvalue, &seqvalue_len, label, &label_len, advance, &advance_len, unit, padding, seqvalue_chk_len, label_chk_len, advance_chk_len);
}

void FC_FUNC_(bind_yaml_dict_dump, BIND_YAML_DICT_DUMP)(const f90_dictionary_pointer*,
  const int*,
  const int*,
  const int*);
void yaml_dict_dump(f90_dictionary_pointer dict,
  const int (*unit),
  const bool (*flow),
  const bool (*verbatim))
{
  int flow_conv = flow ? *flow : 0;
  int verbatim_conv = verbatim ? *verbatim : 0;
  FC_FUNC_(bind_yaml_dict_dump, BIND_YAML_DICT_DUMP)
    (&dict, unit,  flow ? &flow_conv : NULL,  verbatim ? &verbatim_conv : NULL);
}

void FC_FUNC_(bind_yaml_dict_dump_all, BIND_YAML_DICT_DUMP_ALL)(const f90_dictionary_pointer*,
  const int*,
  const int*,
  const int*);
void yaml_dict_dump_all(f90_dictionary_pointer dict,
  const int (*unit),
  const bool (*flow),
  const bool (*verbatim))
{
  int flow_conv = flow ? *flow : 0;
  int verbatim_conv = verbatim ? *verbatim : 0;
  FC_FUNC_(bind_yaml_dict_dump_all, BIND_YAML_DICT_DUMP_ALL)
    (&dict, unit,  flow ? &flow_conv : NULL,  verbatim ? &verbatim_conv : NULL);
}

void FC_FUNC_(bind_yaml_release_document, BIND_YAML_RELEASE_DOCUMENT)(const int*);
void yaml_release_document(const int (*unit))
{
  FC_FUNC_(bind_yaml_release_document, BIND_YAML_RELEASE_DOCUMENT)
    (unit);
}

void FC_FUNC_(bind_yaml_map, BIND_YAML_MAP)(const char*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  size_t,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map(const char* mapname,
  const char* mapvalue,
  const char (*label),
  const char (*tag),
  const char (*advance),
  const int (*unit))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t mapvalue_chk_len, mapvalue_len = mapvalue_chk_len = mapvalue ? strlen(mapvalue) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t tag_chk_len, tag_len = tag_chk_len = tag ? strlen(tag) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  FC_FUNC_(bind_yaml_map, BIND_YAML_MAP)
    (mapname, &mapname_len, mapvalue, &mapvalue_len, label, &label_len, tag, &tag_len, advance, &advance_len, unit, mapname_chk_len, mapvalue_chk_len, label_chk_len, tag_chk_len, advance_chk_len);
}

void FC_FUNC_(bind_yaml_map_dict, BIND_YAML_MAP_DICT)(const char*,
  const size_t*,
  const f90_dictionary_pointer*,
  const char*,
  const size_t*,
  const int*,
  const int*,
  size_t,
  size_t);
void yaml_map_dict(const char* mapname,
  f90_dictionary_pointer mapvalue,
  const char (*label),
  const int (*unit),
  const bool (*flow))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  int flow_conv = flow ? *flow : 0;
  FC_FUNC_(bind_yaml_map_dict, BIND_YAML_MAP_DICT)
    (mapname, &mapname_len, &mapvalue, label, &label_len, unit,  flow ? &flow_conv : NULL, mapname_chk_len, label_chk_len);
}

void FC_FUNC_(bind_yaml_map_enum, BIND_YAML_MAP_ENUM)(const char*,
  const size_t*,
  const f90_f_enumerator*,
  const char*,
  const size_t*,
  const int*,
  const int*,
  size_t,
  size_t);
void yaml_map_enum(const char* mapname,
  const f90_f_enumerator* mapvalue,
  const char (*label),
  const int (*unit),
  const bool (*flow))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  int flow_conv = flow ? *flow : 0;
  FC_FUNC_(bind_yaml_map_enum, BIND_YAML_MAP_ENUM)
    (mapname, &mapname_len, mapvalue, label, &label_len, unit,  flow ? &flow_conv : NULL, mapname_chk_len, label_chk_len);
}

void FC_FUNC_(bind_yaml_map_li, BIND_YAML_MAP_LI)(const char*,
  const size_t*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_li(const char* mapname,
  size_t mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_li, BIND_YAML_MAP_LI)
    (mapname, &mapname_len, &mapvalue, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_i, BIND_YAML_MAP_I)(const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_i(const char* mapname,
  int mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_i, BIND_YAML_MAP_I)
    (mapname, &mapname_len, &mapvalue, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_f, BIND_YAML_MAP_F)(const char*,
  const size_t*,
  const float*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_f(const char* mapname,
  float mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_f, BIND_YAML_MAP_F)
    (mapname, &mapname_len, &mapvalue, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_d, BIND_YAML_MAP_D)(const char*,
  const size_t*,
  const double*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_d(const char* mapname,
  double mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_d, BIND_YAML_MAP_D)
    (mapname, &mapname_len, &mapvalue, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_l, BIND_YAML_MAP_L)(const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_l(const char* mapname,
  bool mapvalue,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  int mapvalue_conv = mapvalue;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_l, BIND_YAML_MAP_L)
    (mapname, &mapname_len, &mapvalue_conv, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_dv, BIND_YAML_MAP_DV)(const char*,
  const size_t*,
  const double*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_dv(const char* mapname,
  const double* mapvalue,
  size_t mapvalue_dim_0,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_dv, BIND_YAML_MAP_DV)
    (mapname, &mapname_len, mapvalue, &mapvalue_dim_0, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_rv, BIND_YAML_MAP_RV)(const char*,
  const size_t*,
  const float*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_rv(const char* mapname,
  const float* mapvalue,
  size_t mapvalue_dim_0,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_rv, BIND_YAML_MAP_RV)
    (mapname, &mapname_len, mapvalue, &mapvalue_dim_0, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_liv, BIND_YAML_MAP_LIV)(const char*,
  const size_t*,
  const size_t*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_liv(const char* mapname,
  const size_t* mapvalue,
  size_t mapvalue_dim_0,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_liv, BIND_YAML_MAP_LIV)
    (mapname, &mapname_len, mapvalue, &mapvalue_dim_0, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_iv, BIND_YAML_MAP_IV)(const char*,
  const size_t*,
  const int*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_iv(const char* mapname,
  const int* mapvalue,
  size_t mapvalue_dim_0,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_iv, BIND_YAML_MAP_IV)
    (mapname, &mapname_len, mapvalue, &mapvalue_dim_0, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_dm, BIND_YAML_MAP_DM)(const char*,
  const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_dm(const char* mapname,
  const double* mapvalue,
  size_t mapvalue_dim_0,
  size_t mapvalue_dim_1,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_dm, BIND_YAML_MAP_DM)
    (mapname, &mapname_len, mapvalue, &mapvalue_dim_0, &mapvalue_dim_1, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_rm, BIND_YAML_MAP_RM)(const char*,
  const size_t*,
  const float*,
  const size_t*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_rm(const char* mapname,
  const float* mapvalue,
  size_t mapvalue_dim_0,
  size_t mapvalue_dim_1,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_rm, BIND_YAML_MAP_RM)
    (mapname, &mapname_len, mapvalue, &mapvalue_dim_0, &mapvalue_dim_1, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_im, BIND_YAML_MAP_IM)(const char*,
  const size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_im(const char* mapname,
  const int* mapvalue,
  size_t mapvalue_dim_0,
  size_t mapvalue_dim_1,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_im, BIND_YAML_MAP_IM)
    (mapname, &mapname_len, mapvalue, &mapvalue_dim_0, &mapvalue_dim_1, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_map_dt, BIND_YAML_MAP_DT)(const char*,
  const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t,
  size_t);
void yaml_map_dt(const char* mapname,
  const double* mapvalue,
  size_t mapvalue_dim_0,
  size_t mapvalue_dim_1,
  size_t mapvalue_dim_2,
  const char (*label),
  const char (*advance),
  const int (*unit),
  const char (*fmt))
{
  size_t mapname_chk_len, mapname_len = mapname_chk_len = mapname ? strlen(mapname) : 0;
  size_t label_chk_len, label_len = label_chk_len = label ? strlen(label) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_map_dt, BIND_YAML_MAP_DT)
    (mapname, &mapname_len, mapvalue, &mapvalue_dim_0, &mapvalue_dim_1, &mapvalue_dim_2, label, &label_len, advance, &advance_len, unit, fmt, &fmt_len, mapname_chk_len, label_chk_len, advance_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_warning_str, BIND_YAML_WARNING_STR)(const f90_f_string*,
  const int*,
  const int*);
void yaml_warning_str(const f90_f_string* message,
  const int (*level),
  const int (*unit))
{
  FC_FUNC_(bind_yaml_warning_str, BIND_YAML_WARNING_STR)
    (message, level, unit);
}

void FC_FUNC_(bind_yaml_warning_c, BIND_YAML_WARNING_C)(const char*,
  const size_t*,
  const int*,
  const int*,
  size_t);
void yaml_warning_c(const char* message,
  const int (*level),
  const int (*unit))
{
  size_t message_chk_len, message_len = message_chk_len = message ? strlen(message) : 0;
  FC_FUNC_(bind_yaml_warning_c, BIND_YAML_WARNING_C)
    (message, &message_len, level, unit, message_chk_len);
}

void FC_FUNC_(bind_yaml_comment_str, BIND_YAML_COMMENT_STR)(const f90_f_string*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  const int*,
  size_t,
  size_t);
void yaml_comment_str(const f90_f_string* message,
  const char (*advance),
  const int (*unit),
  const char (*hfill),
  const int (*tabbing))
{
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t hfill_chk_len, hfill_len = hfill_chk_len = hfill ? strlen(hfill) : 0;
  FC_FUNC_(bind_yaml_comment_str, BIND_YAML_COMMENT_STR)
    (message, advance, &advance_len, unit, hfill, &hfill_len, tabbing, advance_chk_len, hfill_chk_len);
}

void FC_FUNC_(bind_yaml_comment_c, BIND_YAML_COMMENT_C)(const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  const int*,
  size_t,
  size_t,
  size_t);
void yaml_comment_c(const char* message,
  const char (*advance),
  const int (*unit),
  const char (*hfill),
  const int (*tabbing))
{
  size_t message_chk_len, message_len = message_chk_len = message ? strlen(message) : 0;
  size_t advance_chk_len, advance_len = advance_chk_len = advance ? strlen(advance) : 0;
  size_t hfill_chk_len, hfill_len = hfill_chk_len = hfill ? strlen(hfill) : 0;
  FC_FUNC_(bind_yaml_comment_c, BIND_YAML_COMMENT_C)
    (message, &message_len, advance, &advance_len, unit, hfill, &hfill_len, tabbing, message_chk_len, advance_chk_len, hfill_chk_len);
}

