#include "yaml_strings.h"
#include <config.h>
#include <string.h>

void FC_FUNC_(bind_f90_f_string_copy_constructor, BIND_F90_F_STRING_COPY_CONSTRUCTOR)(f90_f_string_pointer*,
  const f90_f_string*);
f90_f_string_pointer f90_f_string_copy_constructor(const f90_f_string* other)
{
  f90_f_string_pointer out_self;
  FC_FUNC_(bind_f90_f_string_copy_constructor, BIND_F90_F_STRING_COPY_CONSTRUCTOR)
    (&out_self, other);
  return out_self;
}

void FC_FUNC_(bind_f90_f_string_type_new, BIND_F90_F_STRING_TYPE_NEW)(f90_f_string_pointer*,
  const char*,
  const size_t*,
  size_t);
f90_f_string_pointer f90_f_string_type_new(const char* msg)
{
  f90_f_string_pointer out_self;
  size_t msg_chk_len, msg_len = msg_chk_len = msg ? strlen(msg) : 0;
  FC_FUNC_(bind_f90_f_string_type_new, BIND_F90_F_STRING_TYPE_NEW)
    (&out_self, msg, &msg_len, msg_chk_len);
  return out_self;
}

void FC_FUNC_(bind_f90_f_string_free, BIND_F90_F_STRING_FREE)(f90_f_string_pointer*);
void f90_f_string_free(f90_f_string_pointer self)
{
  FC_FUNC_(bind_f90_f_string_free, BIND_F90_F_STRING_FREE)
    (&self);
}

void FC_FUNC_(bind_f90_f_string_empty, BIND_F90_F_STRING_EMPTY)(f90_f_string_pointer*);
f90_f_string_pointer f90_f_string_empty(void)
{
  f90_f_string_pointer out_self;
  FC_FUNC_(bind_f90_f_string_empty, BIND_F90_F_STRING_EMPTY)
    (&out_self);
  return out_self;
}

void FC_FUNC_(bind_yaml_bold, BIND_YAML_BOLD)(char*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_bold(char out_bstr[95],
  const char* str)
{
  size_t out_bstr_chk_len = 95;
  size_t str_chk_len, str_len = str_chk_len = str ? strlen(str) : 0;
  FC_FUNC_(bind_yaml_bold, BIND_YAML_BOLD)
    (out_bstr, str, &str_len, out_bstr_chk_len, str_chk_len);
}

void FC_FUNC_(bind_yaml_blink, BIND_YAML_BLINK)(char*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_blink(char out_bstr[95],
  const char* str)
{
  size_t out_bstr_chk_len = 95;
  size_t str_chk_len, str_len = str_chk_len = str ? strlen(str) : 0;
  FC_FUNC_(bind_yaml_blink, BIND_YAML_BLINK)
    (out_bstr, str, &str_len, out_bstr_chk_len, str_chk_len);
}

void FC_FUNC_(bind_yaml_date_and_time_toa, BIND_YAML_DATE_AND_TIME_TOA)(char*,
  const int*,
  const int*,
  size_t);
void yaml_date_and_time_toa(char out_yaml_date_and_time_toa[95],
  const int (*values)[8],
  const bool (*zone))
{
  size_t out_yaml_date_and_time_toa_chk_len = 95;
  int zone_conv = zone ? *zone : 0;
  FC_FUNC_(bind_yaml_date_and_time_toa, BIND_YAML_DATE_AND_TIME_TOA)
    (out_yaml_date_and_time_toa, values ? *values : NULL,  zone ? &zone_conv : NULL, out_yaml_date_and_time_toa_chk_len);
}

void FC_FUNC_(bind_yaml_date_toa, BIND_YAML_DATE_TOA)(char*,
  const int*,
  size_t);
void yaml_date_toa(char out_yaml_date_toa[95],
  const int (*values)[8])
{
  size_t out_yaml_date_toa_chk_len = 95;
  FC_FUNC_(bind_yaml_date_toa, BIND_YAML_DATE_TOA)
    (out_yaml_date_toa, values ? *values : NULL, out_yaml_date_toa_chk_len);
}

void FC_FUNC_(bind_yaml_time_toa, BIND_YAML_TIME_TOA)(char*,
  const int*,
  size_t);
void yaml_time_toa(char out_yaml_time_toa[95],
  const int (*values)[8])
{
  size_t out_yaml_time_toa_chk_len = 95;
  FC_FUNC_(bind_yaml_time_toa, BIND_YAML_TIME_TOA)
    (out_yaml_time_toa, values ? *values : NULL, out_yaml_time_toa_chk_len);
}

void FC_FUNC_(bind_is_atoi, BIND_IS_ATOI)(int*,
  const char*,
  const size_t*,
  size_t);
bool is_atoi(const char* str)
{
  int out_yes;
  size_t str_chk_len, str_len = str_chk_len = str ? strlen(str) : 0;
  FC_FUNC_(bind_is_atoi, BIND_IS_ATOI)
    (&out_yes, str, &str_len, str_chk_len);
  return out_yes;
}

void FC_FUNC_(bind_is_atoli, BIND_IS_ATOLI)(int*,
  const char*,
  const size_t*,
  size_t);
bool is_atoli(const char* str)
{
  int out_yes;
  size_t str_chk_len, str_len = str_chk_len = str ? strlen(str) : 0;
  FC_FUNC_(bind_is_atoli, BIND_IS_ATOLI)
    (&out_yes, str, &str_len, str_chk_len);
  return out_yes;
}

void FC_FUNC_(bind_is_atof, BIND_IS_ATOF)(int*,
  const char*,
  const size_t*,
  size_t);
bool is_atof(const char* str)
{
  int out_yes;
  size_t str_chk_len, str_len = str_chk_len = str ? strlen(str) : 0;
  FC_FUNC_(bind_is_atof, BIND_IS_ATOF)
    (&out_yes, str, &str_len, str_chk_len);
  return out_yes;
}

void FC_FUNC_(bind_is_atol, BIND_IS_ATOL)(int*,
  const char*,
  const size_t*,
  size_t);
bool is_atol(const char* str)
{
  int out_yes;
  size_t str_chk_len, str_len = str_chk_len = str ? strlen(str) : 0;
  FC_FUNC_(bind_is_atol, BIND_IS_ATOL)
    (&out_yes, str, &str_len, str_chk_len);
  return out_yes;
}

void FC_FUNC_(bind_buffer_string, BIND_BUFFER_STRING)(char*,
  const int*,
  const char*,
  const size_t*,
  int*,
  const int*,
  int*,
  size_t,
  size_t);
void buffer_string(char* string_bn,
  int string_lgt,
  const char* buffer,
  int* string_pos,
  const bool (*back),
  int (*istat))
{
  size_t string_bn_chk_len = string_bn ? strlen(string_bn) : 0;
  size_t buffer_chk_len, buffer_len = buffer_chk_len = buffer ? strlen(buffer) : 0;
  int back_conv = back ? *back : 0;
  FC_FUNC_(bind_buffer_string, BIND_BUFFER_STRING)
    (string_bn, &string_lgt, buffer, &buffer_len, string_pos,  back ? &back_conv : NULL, istat, string_bn_chk_len, buffer_chk_len);
}

void FC_FUNC_(bind_align_message, BIND_ALIGN_MESSAGE)(const int*,
  const int*,
  const int*,
  const char*,
  const size_t*,
  char*,
  size_t,
  size_t);
void align_message(bool rigid,
  int maxlen,
  int tabval,
  const char* anchor,
  char* message)
{
  int rigid_conv = rigid;
  size_t anchor_chk_len, anchor_len = anchor_chk_len = anchor ? strlen(anchor) : 0;
  size_t message_chk_len = message ? strlen(message) : 0;
  FC_FUNC_(bind_align_message, BIND_ALIGN_MESSAGE)
    (&rigid_conv, &maxlen, &tabval, anchor, &anchor_len, message, anchor_chk_len, message_chk_len);
}

void FC_FUNC_(bind_read_fraction_string, BIND_READ_FRACTION_STRING)(const char*,
  const size_t*,
  double*,
  int*,
  size_t);
void read_fraction_string(const char* string_bn,
  double* var,
  int* ierror)
{
  size_t string_bn_chk_len, string_bn_len = string_bn_chk_len = string_bn ? strlen(string_bn) : 0;
  FC_FUNC_(bind_read_fraction_string, BIND_READ_FRACTION_STRING)
    (string_bn, &string_bn_len, var, ierror, string_bn_chk_len);
}

void FC_FUNC(bind_rstrip, BIND_RSTRIP)(char*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void rstrip(char* string_bn,
  size_t string_bn_len,
  const char* substring)
{
  size_t string_bn_chk_len = string_bn_len;
  size_t substring_chk_len, substring_len = substring_chk_len = substring ? strlen(substring) : 0;
  FC_FUNC(bind_rstrip, BIND_RSTRIP)
    (string_bn, &string_bn_len, substring, &substring_len, string_bn_chk_len, substring_chk_len);
}

void FC_FUNC(bind_shiftstr, BIND_SHIFTSTR)(char*,
  const size_t*,
  const int*,
  size_t);
void shiftstr(char* str,
  size_t str_len,
  int n)
{
  size_t str_chk_len = str_len;
  FC_FUNC(bind_shiftstr, BIND_SHIFTSTR)
    (str, &str_len, &n, str_chk_len);
}

void FC_FUNC_(bind_convert_f_char_ptr, BIND_CONVERT_F_CHAR_PTR)(const char*,
  const size_t*,
  char*,
  const size_t*,
  size_t,
  size_t);
void convert_f_char_ptr(const char* src,
  char* dest,
  size_t dest_len)
{
  size_t src_chk_len, src_len = src_chk_len = src ? strlen(src) : 0;
  size_t dest_chk_len = dest_len;
  FC_FUNC_(bind_convert_f_char_ptr, BIND_CONVERT_F_CHAR_PTR)
    (src, &src_len, dest, &dest_len, src_chk_len, dest_chk_len);
}

void FC_FUNC_(bind_yaml_itoa, BIND_YAML_ITOA)(char*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_itoa(char out_str[95],
  int data,
  const char (*fmt))
{
  size_t out_str_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_itoa, BIND_YAML_ITOA)
    (out_str, &data, fmt, &fmt_len, out_str_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_litoa, BIND_YAML_LITOA)(char*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_litoa(char out_str[95],
  size_t data,
  const char (*fmt))
{
  size_t out_str_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_litoa, BIND_YAML_LITOA)
    (out_str, &data, fmt, &fmt_len, out_str_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_ftoa, BIND_YAML_FTOA)(char*,
  const float*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_ftoa(char out_str[95],
  float data,
  const char (*fmt))
{
  size_t out_str_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_ftoa, BIND_YAML_FTOA)
    (out_str, &data, fmt, &fmt_len, out_str_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_dtoa, BIND_YAML_DTOA)(char*,
  const double*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_dtoa(char out_str[95],
  double data,
  const char (*fmt))
{
  size_t out_str_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_dtoa, BIND_YAML_DTOA)
    (out_str, &data, fmt, &fmt_len, out_str_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_ltoa, BIND_YAML_LTOA)(char*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_ltoa(char out_yaml_ltoa[95],
  bool l,
  const char (*fmt))
{
  size_t out_yaml_ltoa_chk_len = 95;
  int l_conv = l;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_ltoa, BIND_YAML_LTOA)
    (out_yaml_ltoa, &l_conv, fmt, &fmt_len, out_yaml_ltoa_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_dvtoa, BIND_YAML_DVTOA)(char*,
  const double*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_dvtoa(char out_vec_toa[95],
  const double* vec,
  size_t vec_dim_0,
  const char (*fmt))
{
  size_t out_vec_toa_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_dvtoa, BIND_YAML_DVTOA)
    (out_vec_toa, vec, &vec_dim_0, fmt, &fmt_len, out_vec_toa_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_ivtoa, BIND_YAML_IVTOA)(char*,
  const int*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_ivtoa(char out_vec_toa[95],
  const int* vec,
  size_t vec_dim_0,
  const char (*fmt))
{
  size_t out_vec_toa_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_ivtoa, BIND_YAML_IVTOA)
    (out_vec_toa, vec, &vec_dim_0, fmt, &fmt_len, out_vec_toa_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_rvtoa, BIND_YAML_RVTOA)(char*,
  const float*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_rvtoa(char out_vec_toa[95],
  const float* vec,
  size_t vec_dim_0,
  const char (*fmt))
{
  size_t out_vec_toa_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_rvtoa, BIND_YAML_RVTOA)
    (out_vec_toa, vec, &vec_dim_0, fmt, &fmt_len, out_vec_toa_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_livtoa, BIND_YAML_LIVTOA)(char*,
  const long*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_livtoa(char out_vec_toa[95],
  const long* vec,
  size_t vec_dim_0,
  const char (*fmt))
{
  size_t out_vec_toa_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_livtoa, BIND_YAML_LIVTOA)
    (out_vec_toa, vec, &vec_dim_0, fmt, &fmt_len, out_vec_toa_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_f_strcpy, BIND_F_STRCPY)(char*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void f_strcpy(char* dest,
  size_t dest_len,
  const char* src)
{
  size_t dest_chk_len = dest_len;
  size_t src_chk_len, src_len = src_chk_len = src ? strlen(src) : 0;
  FC_FUNC_(bind_f_strcpy, BIND_F_STRCPY)
    (dest, &dest_len, src, &src_len, dest_chk_len, src_chk_len);
}

void FC_FUNC_(bind_f_strcpy_str, BIND_F_STRCPY_STR)(char*,
  const size_t*,
  const f90_f_string*,
  size_t);
void f_strcpy_str(char* dest,
  size_t dest_len,
  const f90_f_string* src)
{
  size_t dest_chk_len = dest_len;
  FC_FUNC_(bind_f_strcpy_str, BIND_F_STRCPY_STR)
    (dest, &dest_len, src, dest_chk_len);
}

void FC_FUNC_(bind_string_equivalence, BIND_STRING_EQUIVALENCE)(int*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
bool string_equivalence(const char* a,
  const char* b)
{
  int out_ok;
  size_t a_chk_len, a_len = a_chk_len = a ? strlen(a) : 0;
  size_t b_chk_len, b_len = b_chk_len = b ? strlen(b) : 0;
  FC_FUNC_(bind_string_equivalence, BIND_STRING_EQUIVALENCE)
    (&out_ok, a, &a_len, b, &b_len, a_chk_len, b_chk_len);
  return out_ok;
}

void FC_FUNC_(bind_string_inequivalence, BIND_STRING_INEQUIVALENCE)(int*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
bool string_inequivalence(const char* a,
  const char* b)
{
  int out_notok;
  size_t a_chk_len, a_len = a_chk_len = a ? strlen(a) : 0;
  size_t b_chk_len, b_len = b_chk_len = b ? strlen(b) : 0;
  FC_FUNC_(bind_string_inequivalence, BIND_STRING_INEQUIVALENCE)
    (&out_notok, a, &a_len, b, &b_len, a_chk_len, b_chk_len);
  return out_notok;
}

void FC_FUNC_(bind_string_and_integer, BIND_STRING_AND_INTEGER)(f90_f_string_pointer*,
  const char*,
  const size_t*,
  const int*,
  size_t);
f90_f_string_pointer string_and_integer(const char* a,
  int num)
{
  f90_f_string_pointer out_c;
  size_t a_chk_len, a_len = a_chk_len = a ? strlen(a) : 0;
  FC_FUNC_(bind_string_and_integer, BIND_STRING_AND_INTEGER)
    (&out_c, a, &a_len, &num, a_chk_len);
  return out_c;
}

void FC_FUNC_(bind_integer_and_string, BIND_INTEGER_AND_STRING)(f90_f_string_pointer*,
  const int*,
  const char*,
  const size_t*,
  size_t);
f90_f_string_pointer integer_and_string(int a,
  const char* num)
{
  f90_f_string_pointer out_c;
  size_t num_chk_len, num_len = num_chk_len = num ? strlen(num) : 0;
  FC_FUNC_(bind_integer_and_string, BIND_INTEGER_AND_STRING)
    (&out_c, &a, num, &num_len, num_chk_len);
  return out_c;
}

void FC_FUNC_(bind_integer_and_msg, BIND_INTEGER_AND_MSG)(f90_f_string_pointer*,
  const int*,
  const f90_f_string*);
f90_f_string_pointer integer_and_msg(int a,
  const f90_f_string* num)
{
  f90_f_string_pointer out_c;
  FC_FUNC_(bind_integer_and_msg, BIND_INTEGER_AND_MSG)
    (&out_c, &a, num);
  return out_c;
}

void FC_FUNC_(bind_string_and_long, BIND_STRING_AND_LONG)(f90_f_string_pointer*,
  const char*,
  const size_t*,
  const size_t*,
  size_t);
f90_f_string_pointer string_and_long(const char* a,
  size_t num)
{
  f90_f_string_pointer out_c;
  size_t a_chk_len, a_len = a_chk_len = a ? strlen(a) : 0;
  FC_FUNC_(bind_string_and_long, BIND_STRING_AND_LONG)
    (&out_c, a, &a_len, &num, a_chk_len);
  return out_c;
}

void FC_FUNC_(bind_string_and_double, BIND_STRING_AND_DOUBLE)(f90_f_string_pointer*,
  const char*,
  const size_t*,
  const double*,
  size_t);
f90_f_string_pointer string_and_double(const char* a,
  double num)
{
  f90_f_string_pointer out_c;
  size_t a_chk_len, a_len = a_chk_len = a ? strlen(a) : 0;
  FC_FUNC_(bind_string_and_double, BIND_STRING_AND_DOUBLE)
    (&out_c, a, &a_len, &num, a_chk_len);
  return out_c;
}

void FC_FUNC_(bind_string_and_simple, BIND_STRING_AND_SIMPLE)(f90_f_string_pointer*,
  const char*,
  const size_t*,
  const float*,
  size_t);
f90_f_string_pointer string_and_simple(const char* a,
  float num)
{
  f90_f_string_pointer out_c;
  size_t a_chk_len, a_len = a_chk_len = a ? strlen(a) : 0;
  FC_FUNC_(bind_string_and_simple, BIND_STRING_AND_SIMPLE)
    (&out_c, a, &a_len, &num, a_chk_len);
  return out_c;
}

void FC_FUNC_(bind_attach_ci, BIND_ATTACH_CI)(f90_f_string_pointer*,
  const char*,
  const size_t*,
  const int*,
  size_t);
f90_f_string_pointer attach_ci(const char* s,
  int num)
{
  f90_f_string_pointer out_c;
  size_t s_chk_len, s_len = s_chk_len = s ? strlen(s) : 0;
  FC_FUNC_(bind_attach_ci, BIND_ATTACH_CI)
    (&out_c, s, &s_len, &num, s_chk_len);
  return out_c;
}

void FC_FUNC_(bind_attach_cli, BIND_ATTACH_CLI)(f90_f_string_pointer*,
  const char*,
  const size_t*,
  const size_t*,
  size_t);
f90_f_string_pointer attach_cli(const char* s,
  size_t num)
{
  f90_f_string_pointer out_c;
  size_t s_chk_len, s_len = s_chk_len = s ? strlen(s) : 0;
  FC_FUNC_(bind_attach_cli, BIND_ATTACH_CLI)
    (&out_c, s, &s_len, &num, s_chk_len);
  return out_c;
}

void FC_FUNC_(bind_attach_lic, BIND_ATTACH_LIC)(f90_f_string_pointer*,
  const size_t*,
  const char*,
  const size_t*,
  size_t);
f90_f_string_pointer attach_lic(size_t num,
  const char* s)
{
  f90_f_string_pointer out_c;
  size_t s_chk_len, s_len = s_chk_len = s ? strlen(s) : 0;
  FC_FUNC_(bind_attach_lic, BIND_ATTACH_LIC)
    (&out_c, &num, s, &s_len, s_chk_len);
  return out_c;
}

void FC_FUNC_(bind_attach_cd, BIND_ATTACH_CD)(f90_f_string_pointer*,
  const char*,
  const size_t*,
  const double*,
  size_t);
f90_f_string_pointer attach_cd(const char* s,
  double num)
{
  f90_f_string_pointer out_c;
  size_t s_chk_len, s_len = s_chk_len = s ? strlen(s) : 0;
  FC_FUNC_(bind_attach_cd, BIND_ATTACH_CD)
    (&out_c, s, &s_len, &num, s_chk_len);
  return out_c;
}

void FC_FUNC_(bind_msg_to_string, BIND_MSG_TO_STRING)(char*,
  const size_t*,
  const f90_f_string*,
  size_t);
void msg_to_string(char* string_bn,
  size_t string_bn_len,
  const f90_f_string* msg)
{
  size_t string_bn_chk_len = string_bn_len;
  FC_FUNC_(bind_msg_to_string, BIND_MSG_TO_STRING)
    (string_bn, &string_bn_len, msg, string_bn_chk_len);
}

void FC_FUNC_(bind_string_to_msg, BIND_STRING_TO_MSG)(f90_f_string*,
  const char*,
  const size_t*,
  size_t);
void string_to_msg(f90_f_string* msg,
  const char* string_bn)
{
  size_t string_bn_chk_len, string_bn_len = string_bn_chk_len = string_bn ? strlen(string_bn) : 0;
  FC_FUNC_(bind_string_to_msg, BIND_STRING_TO_MSG)
    (msg, string_bn, &string_bn_len, string_bn_chk_len);
}

void FC_FUNC_(bind_yaml_itoa_fmt, BIND_YAML_ITOA_FMT)(char*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_itoa_fmt(char out_c[95],
  int num,
  const char* fmt)
{
  size_t out_c_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_itoa_fmt, BIND_YAML_ITOA_FMT)
    (out_c, &num, fmt, &fmt_len, out_c_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_litoa_fmt, BIND_YAML_LITOA_FMT)(char*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_litoa_fmt(char out_c[95],
  size_t num,
  const char* fmt)
{
  size_t out_c_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_litoa_fmt, BIND_YAML_LITOA_FMT)
    (out_c, &num, fmt, &fmt_len, out_c_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_dtoa_fmt, BIND_YAML_DTOA_FMT)(char*,
  const double*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void yaml_dtoa_fmt(char out_c[95],
  double num,
  const char* fmt)
{
  size_t out_c_chk_len = 95;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_dtoa_fmt, BIND_YAML_DTOA_FMT)
    (out_c, &num, fmt, &fmt_len, out_c_chk_len, fmt_chk_len);
}

void FC_FUNC_(bind_yaml_ctoa_fmt, BIND_YAML_CTOA_FMT)(char*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t,
  size_t);
void yaml_ctoa_fmt(char out_c[95],
  const char* num,
  const char* fmt)
{
  size_t out_c_chk_len = 95;
  size_t num_chk_len, num_len = num_chk_len = num ? strlen(num) : 0;
  size_t fmt_chk_len, fmt_len = fmt_chk_len = fmt ? strlen(fmt) : 0;
  FC_FUNC_(bind_yaml_ctoa_fmt, BIND_YAML_CTOA_FMT)
    (out_c, num, &num_len, fmt, &fmt_len, out_c_chk_len, num_chk_len, fmt_chk_len);
}

